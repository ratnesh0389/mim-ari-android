package arinet.com.mim.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;

import arinet.com.mim.R;
import arinet.com.mim.model.Attribute;
import arinet.com.mim.model.Unit;
import arinet.com.mim.model.UnitAttribute;
import arinet.com.mim.service.attribute.AttributeService;
import arinet.com.mim.service.toolkit.ToolkitService;
import arinet.com.mim.service.unit.UnitService;
import arinet.com.mim.service.unitAttribute.UnitAttributeService;
import arinet.com.mim.view.AddDetailEditText;
import arinet.com.mim.view.AddDetailSpinner;

public class AdditionalDetails extends Activity {

    public final static String UNIT_ID = "unit_id";

    private LinearLayout additionalDetailContainer;
    private ArrayList<Attribute>attributes;
    private Unit unit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional_details);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setLogo(this.getResources().getDrawable(R.drawable.action_bar_logo));

        int unitId = getIntent().getExtras().getInt(UNIT_ID, 0);

        ArrayList<Attribute>attributes = AttributeService.getInstance().getAllAttributes(getApplicationContext());

        additionalDetailContainer = (LinearLayout)findViewById(R.id.additional_details_container);

        if (unitId > 0){
            unit = UnitService.getInstance().getUnitWithUnitId(getApplicationContext(), unitId);

            if (attributes.size() > unit.getUnitAttributes().size()){
                for (Attribute attribute : attributes){
                    boolean found = false;
                    for (UnitAttribute ua : unit.getUnitAttributes()){
                        if (ua.getAttributeId() == attribute.getAttributeId()){
                            found = true;
                            break;
                        }
                    }
                    if (!found){
                        UnitAttribute nua = new UnitAttribute();
                        nua.setAttributeId(attribute.getAttributeId());
                        nua.setName(attribute.getName());
                        nua.setType(attribute.getType());
                        nua.setValue("");
                        nua.setUnitId(unit.getUnitId());

                        unit.getUnitAttributes().add(nua);
                    }
                }
                UnitAttributeService.getInstance().replaceUnitAttributesForUnit(getApplicationContext(), unit);
            }

            LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            for (Attribute attribute : attributes){
                if (attribute.getIndustryId() == unit.getIndustryId()){

                    if (attribute.getType().equalsIgnoreCase("String") || attribute.getType().equalsIgnoreCase("Decimal") || attribute.getType().equalsIgnoreCase("Int")){
                        LinearLayout detailTextLayout = (LinearLayout)inflater.inflate(R.layout.add_details_text_layout,null);
                        TextView label = (TextView)detailTextLayout.findViewById(R.id.unit_detail_layout_label);
                        label.setMaxLines(1);
                        label.setEllipsize(TextUtils.TruncateAt.END);
                        label.setText(attribute.getName());

                        AddDetailEditText et = (AddDetailEditText)detailTextLayout.findViewById(R.id.unit_detail_layout_edit_text);
                        et.setAttribute(attribute);
                        et.addTextChangedListener(new SharedTextWatcher(attribute, detailTextLayout));
                        et.setOnEditorActionListener(new SharedOnEditorActionListener(attribute, detailTextLayout));

                        if (!attribute.getType().equalsIgnoreCase("String")){
                            et.setInputType(InputType.TYPE_CLASS_NUMBER);
                        }

                        for (UnitAttribute ua : unit.getUnitAttributes()){
                            if (ua.getAttributeId() == attribute.getAttributeId()){
                                et.setText(ua.getValue());
                                break;
                            }
                        }
                        additionalDetailContainer.addView(detailTextLayout);
                    }
                    else if (attribute.getType().equalsIgnoreCase("Feet/Inches")){
                        LinearLayout detailTwinLayout = (LinearLayout)inflater.inflate(R.layout.add_details_twin_text_layout,null);
                        TextView label = (TextView)detailTwinLayout.findViewById(R.id.unit_detail_layout_label);
                        label.setText(attribute.getName());

                        AddDetailEditText fet = (AddDetailEditText)detailTwinLayout.findViewById(R.id.unit_detail_layout_edit_text_feet);
                        fet.setAttribute(attribute);
                        fet.addTextChangedListener(new SharedTextWatcher(attribute, detailTwinLayout));
                        fet.setOnEditorActionListener(new SharedOnEditorActionListener(attribute, detailTwinLayout));

                        AddDetailEditText iet = (AddDetailEditText)detailTwinLayout.findViewById(R.id.unit_detail_layout_edit_text_inches);
                        iet.setAttribute(attribute);
                        iet.addTextChangedListener(new SharedTextWatcher(attribute, detailTwinLayout));
                        iet.setOnEditorActionListener(new SharedOnEditorActionListener(attribute, detailTwinLayout));

                        for (UnitAttribute ua : unit.getUnitAttributes()){
                            if (ua.getAttributeId() == attribute.getAttributeId() && ua.getValue().length() > 0){
                                try {
                                    double fiVal = Double.parseDouble(ua.getValue());

                                    int feetVal = (int) Math.floor(fiVal);
                                    fet.setText(feetVal + "");

                                    int inches = (int) Math.round((fiVal * 12) - (feetVal * 12));
                                    iet.setText(inches + "");
                                }
                                catch (NumberFormatException e){

                                }
                                break;
                            }
                        }

                        additionalDetailContainer.addView(detailTwinLayout);
                    }
                    else if (attribute.getType().equalsIgnoreCase("Drop Down Menu")){
                        LinearLayout detailSpinnerLayout = (LinearLayout)inflater.inflate(R.layout.add_details_spinner_layout,null);
                        TextView label = (TextView)detailSpinnerLayout.findViewById(R.id.unit_detail_layout_label);
                        label.setText(attribute.getName());

                        AddDetailSpinner spinner = (AddDetailSpinner)detailSpinnerLayout.findViewById(R.id.unit_detail_spinner);
                        spinner.setAttribute(attribute);

                        ArrayList<String>strings = new ArrayList<String>();
                        strings.add("");
                        strings.addAll(Arrays.asList(attribute.getValue().split(",")));
                        String[] items = strings.toArray(new String[strings.size()]);

                        ArrayAdapter<String> itemAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_row_item, items);
                        itemAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);

                        spinner.setAdapter(itemAdapter);
                        spinner.setOnItemSelectedListener(new SharedItemSelectedListener());

                        int index = 0;
                        for (UnitAttribute ua : unit.getUnitAttributes()){
                            if (ua.getAttributeId() == attribute.getAttributeId()){
                                for (int i = 0; i < items.length; ++i){
                                    if (items[i].equalsIgnoreCase(ua.getValue())){
                                        index = i;
                                        break;
                                    }
                                }
                                break;
                            }
                        }
                        spinner.setSelection(index);

                        additionalDetailContainer.addView(detailSpinnerLayout);
                    }
                }
            }

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.additional_details, menu);
        menu.findItem(R.id.action_save).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                handleBack();
                return true;
            case R.id.action_save:
                save();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void save(){
        UnitAttributeService.getInstance().replaceUnitAttributesForUnit(getApplicationContext(), unit);
        Intent returnIntent = new Intent();
        setResult(RESULT_OK,returnIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        handleBack();
    }

    public void handleBack(){
        Intent returnIntent = new Intent();
        setResult(RESULT_CANCELED,returnIntent);
        finish();
    }

    private class SharedTextWatcher implements TextWatcher{

        private Attribute attribute;
        private LinearLayout layout;

        private SharedTextWatcher(Attribute attribute, LinearLayout layout) {
            this.attribute = attribute;
            this.layout = layout;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        public void afterTextChanged(Editable editable) {
            handleEditorChange(attribute, layout, editable.toString());
        }
    }

    private class SharedOnEditorActionListener implements TextView.OnEditorActionListener{

        private Attribute attribute;
        private LinearLayout layout;

        public SharedOnEditorActionListener(Attribute attribute, LinearLayout layout){
            this.attribute = attribute;
            this.layout = layout;
        }

        @Override
        public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
                handled = true;

                handleEditorChange(attribute, layout, textView.getText().toString());
            }
            return handled;
        }
    }

    private class SharedItemSelectedListener implements AdapterView.OnItemSelectedListener {

        private SharedItemSelectedListener() {}

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            Attribute attribute = ((AddDetailSpinner)adapterView).getAttribute();
            String[] values = attribute.getValue().split(",");
            if (i == 0){
                updateUnitAttribute(attribute, "");
            }
            else if (i-1 < values.length){
                updateUnitAttribute(attribute, values[i-1]);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private void handleEditorChange(Attribute attribute, LinearLayout layout, String value){
        if (attribute.getType().equalsIgnoreCase("String") || attribute.getType().equalsIgnoreCase("Decimal") || attribute.getType().equalsIgnoreCase("Int")){
            updateUnitAttribute(attribute, value);
        }
        else if (attribute.getType().equalsIgnoreCase("Feet/Inches")){
            AddDetailEditText fet = (AddDetailEditText)layout.findViewById(R.id.unit_detail_layout_edit_text_feet);
            AddDetailEditText iet = (AddDetailEditText)layout.findViewById(R.id.unit_detail_layout_edit_text_inches);

            try{
                int feet = Integer.parseInt(fet.getText().toString());
                int inches = Integer.parseInt(iet.getText().toString());

                if (inches > 11){
                    feet += inches/12;
                    inches = inches%12;
                }

                double total = feet + ((double)inches/12);

                updateUnitAttribute(attribute, total+"");
            }
            catch(NumberFormatException e){

            }
        }
    }

    private void updateUnitAttribute(Attribute attribute, String value){
        for (UnitAttribute ua : unit.getUnitAttributes()){
            if (ua.getAttributeId() == attribute.getAttributeId()){
                ua.setValue(value);
            }
        }
    }
}
