package arinet.com.mim.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ortiz.touchview.TouchImageView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import arinet.com.mim.R;
import arinet.com.mim.model.Image;
import arinet.com.mim.service.toolkit.ToolkitService;
import arinet.com.mim.view.CustomViewPager;

/**
 * Created by Dev Android on 19,May,2019
 */
public class ImageSlider extends Activity {

    private CustomViewPager customViewPager;

    public static String IMAGES_LIST = "image_list";
    public static String CURRENT_INDEX = "current_index";

    private int currentIndex;
    public ArrayList<Image> images;

    private TextView sliderTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState!=null){
            images = savedInstanceState.getParcelableArrayList(IMAGES_LIST);
            currentIndex = savedInstanceState.getInt(CURRENT_INDEX);
        }else{
            images = getIntent().getParcelableArrayListExtra(IMAGES_LIST);
            currentIndex = getIntent().getIntExtra(CURRENT_INDEX,0);
        }
        setContentView(R.layout.activity_image_slider);
        sliderTv = findViewById(R.id.sliderTv);
        sliderTv.setText((currentIndex+1)+"/"+images.size());
        customViewPager = findViewById(R.id.viewPager);
        customViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                sliderTv.setText((position+1)+"/"+images.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        customViewPager.setAdapter(new TouchImageAdapter(this,images));

        customViewPager.setCurrentItem(currentIndex);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(IMAGES_LIST,images);
        outState.putInt(CURRENT_INDEX,currentIndex);
    }


    static class TouchImageAdapter extends PagerAdapter {
        private ArrayList<Image> images;
        private Context context;
        public TouchImageAdapter(Context context,ArrayList<Image> images){
            this.images = images;
            this.context = context;
        }

        //private static int[] images = { R.drawable.nature_1, R.drawable.nature_2, R.drawable.nature_3, R.drawable.nature_4, R.drawable.nature_5 };

        @Override
        public int getCount() {
            return images.size();
        }

        @NonNull
        @Override
        public View instantiateItem(@NonNull ViewGroup container, int position) {
            TouchImageView imageView = new TouchImageView(container.getContext());
            imageView.setBackgroundColor(context.getResources().getColor(R.color.black_color));
            Image image = images.get(position);

            /*if (image.getLocation() != null){
                Picasso.with(context).load(image.getLocation());
            }
            else if (image.getLocalPath() != null){
                Picasso.with(context).load(Uri.parse(image.getLocalPath()));
            }*/
            if (image.getLocation() != null) {
                Picasso.with(context)
                        .load("http:"+image.getLocation())
                        .placeholder(context.getResources().getDrawable(R.color.black_color)).into(imageView);
            } else if (image.getLocalPath() != null) {

                File imgFile = new File(image.getLocalPath());
                if (imgFile.exists()) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
                    options.inSampleSize = ToolkitService.getInstance().calculateInSampleSize(options, 400, 400);
                    options.inPurgeable = true;
                    options.inInputShareable = true;
                    options.inJustDecodeBounds = false;
                    options.inPreferredConfig = Bitmap.Config.ARGB_8888;

                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
                    imageView.setImageBitmap(myBitmap);
                }
            }

            //img.setImageResource(images[position]);
            container.addView(imageView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            return imageView;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view == object;
        }

    }
}
