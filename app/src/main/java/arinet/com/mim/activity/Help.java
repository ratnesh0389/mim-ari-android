package arinet.com.mim.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;

import arinet.com.mim.R;
import arinet.com.mim.utils.Utils;

public class Help extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setLogo(this.getResources().getDrawable(R.drawable.action_bar_logo));

	    PackageManager manager = this.getPackageManager();
	    try {
		    PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);

		    TextView version = (TextView)findViewById(R.id.version);
		    version.setText("ARI Mobile Version " + info.versionName);
	    }
	    catch(PackageManager.NameNotFoundException e){
		    e.printStackTrace();
	    }

	    // Open Phone dialer pad
	    findViewById(R.id.help_call_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDialer();
            }
        });

        // Open Email App
        findViewById(R.id.help_email_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openEmailApp();
            }
        });

        findViewById(R.id.privacyPolicyTv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.openPrivacyBrowser(Help.this);
            }
        });
    }

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.help, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     *  Open Phone Dialer pad to call
     */
    private void openDialer(){
        try{
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + getString(R.string.content_help_phone)));
            startActivity(intent);
        }catch (ActivityNotFoundException e){
            displayAlert(getString(R.string.dialer_activity_not_found_error));
        }
    }


    private void openEmailApp(){
        try{
            Intent intent = new Intent (Intent.ACTION_VIEW , Uri.parse("mailto:" + getString(R.string.content_help_email)));
            intent.putExtra(Intent.EXTRA_SUBJECT, "Request from ARI Mobile Inventory Manager");
            intent.putExtra(Intent.EXTRA_TEXT, "");
            startActivity(intent);
        }catch(ActivityNotFoundException e){
            displayAlert(getString(R.string.email_activity_not_found_error));
        }
    }


    private void displayAlert(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message).setPositiveButton("OK", null).show();
    }
}
