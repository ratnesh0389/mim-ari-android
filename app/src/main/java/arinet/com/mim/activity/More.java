package arinet.com.mim.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.google.analytics.tracking.android.EasyTracker;

import arinet.com.mim.R;
import arinet.com.mim.service.site.SiteRequestService;

public class More extends Activity {

    private More more;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more);

        more = this;

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setLogo(this.getResources().getDrawable(R.drawable.action_bar_logo));

        registerReceiver(receiver, new IntentFilter(SiteRequestService.NOTIFICATION));

        LinearLayout helpButtonView = (LinearLayout)findViewById(R.id.help_button_view);
        helpButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToHelp();
            }
        });

        LinearLayout deauthButton = (LinearLayout)findViewById(R.id.deauth_button_view);
        deauthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ui = new Intent(more, SiteRequestService.class);
                ui.putExtra(SiteRequestService.REMOVE, true);
                startService(ui);
            }
        });
    }

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    private void goToHelp(){
        Intent intent = new Intent(this.getApplicationContext(), Help.class);
        startActivity(intent);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();

            if (bundle != null) {
                String sender = bundle.getString("sender");

                if (sender.equals("site_request_service")) {
                    int resultCode = bundle.getInt("result");
                    boolean remove = bundle.getBoolean(SiteRequestService.REMOVE);

                    if (resultCode == Activity.RESULT_OK && remove){
                        handleSiteDeauth();
                    }
                }
            }
        }
    };

    private void handleSiteDeauth(){
        Intent returnIntent = new Intent();
        setResult(RESULT_OK, returnIntent);
        finish();
    }

}
