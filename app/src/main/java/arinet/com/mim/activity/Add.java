package arinet.com.mim.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;

import java.util.ArrayList;

import arinet.com.mim.R;
import arinet.com.mim.adapter.ProductResultAdapter;
import arinet.com.mim.model.Unit;
import arinet.com.mim.service.unit.UnitRequestService;
import arinet.com.mim.service.unit.UnitService;

public class Add extends Activity {

    private GridView grid;
    private ProductResultAdapter adapter;
    private ArrayList<Unit> units;
    private ProgressBar progressbar;
    private TextView noResults;

    static final int ADD_REQUEST = 6;
    static final int FINALIZE_REQUEST = 7;

    static final String FINALIZE = "finalize";
    static final String QUERY = "query";

    enum State {
        NEW,
        UNFINALIZED,
    }

    State state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setLogo(this.getResources().getDrawable(R.drawable.action_bar_logo));

        progressbar = (ProgressBar)findViewById(R.id.progress_bar);
        noResults = (TextView)findViewById(R.id.noResults);

        SearchView searchView = (SearchView)findViewById(R.id.add_search);
        searchView.setOnQueryTextListener(queryTextListener);

        this.state = State.NEW;

        if (getIntent().getExtras() != null) {
            String query = getIntent().getExtras().getString(QUERY);
            if (query.length() > 0) {
                searchView.setQuery(query, true);
            }
            if (getIntent().getExtras().getBoolean(FINALIZE)) {
                this.state = State.UNFINALIZED;
                getActionBar().setTitle("Search");
            }
        }

        units = new ArrayList<Unit>();

        grid = (GridView)findViewById(R.id.add_search_grid);
        grid.setOnItemClickListener(onItemClickListener);
        adapter = new ProductResultAdapter(this, android.R.id.text1, units);
        grid.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                handleBack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}

    @Override
    protected void onResume(){
        super.onResume();
        registerReceiver(receiver, new IntentFilter(UnitRequestService.NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        handleBack();
    }

    public void handleBack(){
        Intent returnIntent = new Intent();
        setResult(RESULT_OK,returnIntent);
        finish();
    }

    public void goToDetail(long localUnitId){
        if (this.state == State.NEW){
            Intent intent = new Intent(this.getApplicationContext(), Detail.class);
            intent.putExtra(Detail.LOCAL_UNIT_ID, localUnitId);
            startActivityForResult(intent, Stream.DETAIL_REQUEST);
        }
        else{
            Intent returnIntent = new Intent();
            returnIntent.putExtra(Detail.LOCAL_UNIT_ID, localUnitId);
            setResult(RESULT_OK,returnIntent);
            finish();
        }
    }

    OnQueryTextListener queryTextListener = new OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {
            if (s.length() > 3){
                noResults.setVisibility(View.GONE);
                progressbar.setVisibility(View.VISIBLE);
                UnitService.getInstance().getProductDataForQuery(getApplicationContext(), s);
            }
            return false;
        }
    };

    OnItemClickListener onItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Unit unit = units.get(i);
            goToDetail(unit.getLocalId());
        }
    };

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();

            if (bundle != null) {
                String sender = bundle.getString("sender");

                if (sender.equals("unit_request_service")) {
                    progressbar.setVisibility(View.GONE);
                    noResults.setVisibility(View.GONE);

                    new GetProductResultsTask().execute();
                }
            }
        }
    };

    /*
     * ASYNC TASKS
     */

    private class GetProductResultsTask extends AsyncTask<String, Void, ArrayList<Unit>> {

        protected ArrayList<Unit> doInBackground(String... urls) {
            return UnitService.getInstance().getProductDataResults(getApplicationContext());
        }

        protected void onPostExecute(ArrayList<Unit> results) {
            units.clear();

            units.addAll(results);

            adapter.notifyDataSetChanged();

            if (results.size() == 0){
                noResults.setVisibility(View.VISIBLE);
            }
            else {
                noResults.setVisibility(View.GONE);
            }
        }
    }
}
