package arinet.com.mim.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.newrelic.agent.android.NewRelic;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.drawerlayout.widget.DrawerLayout;
import arinet.com.mim.R;
import arinet.com.mim.adapter.UnitGridAdapter;
import arinet.com.mim.fragment.VisibilityDialogFragment;
import arinet.com.mim.model.Filter;
import arinet.com.mim.model.Image;
import arinet.com.mim.model.Site;
import arinet.com.mim.model.Unit;
import arinet.com.mim.service.attribute.AttributeRequestService;
import arinet.com.mim.service.connection.ConnectionService;
import arinet.com.mim.service.filter.FilterRequestService;
import arinet.com.mim.service.filter.FilterService;
import arinet.com.mim.service.image.ImageService;
import arinet.com.mim.service.location.LocationRequestService;
import arinet.com.mim.service.site.SiteRequestService;
import arinet.com.mim.service.site.SiteService;
import arinet.com.mim.service.toolkit.ToolkitService;
import arinet.com.mim.service.unit.UnitRequestService;
import arinet.com.mim.service.unit.UnitService;
import arinet.com.mim.service.visibility.VisibilityRequestService;
import arinet.com.mim.view.FilterDrawerButtonView;
import arinet.com.mim.view.FragmentDialogVisibilityItem;

/**
 * Created by kevinstueber on 8/4/14.
 */
public class Stream extends Activity{

    private Stream streamActivity;

    private Filter selectedFilter;
    private Menu menu;
    private DrawerLayout drawerLayout;
    //private ActionBarDrawerToggle drawerToggleListener;

    private GridView grid;
    private UnitGridAdapter adapter;

    private ProgressDialog dialog;
    private ProgressBar searchLoader;

    private TextView noResults;

    private ImageButton popularButton;
    private ImageButton recentButton;
    private ImageButton visibilityButton;

    AlertDialog offlineConfirmation;

    private ArrayList<Unit> units;
    private ArrayList<Unit> allUnits;
    private ArrayList<Filter> filters;

    static final int WELCOME_REQUEST = 1;
    static final int DETAIL_REQUEST = 2;
    static final int MORE_REQUEST = 3;
    static final int IMAGE_REQUEST = 4;
    static final int IMAGE_CAPTURE = 5;

    static final String LOADED = "loaded";
    static final String DIALOG = "dialog";
    static final String DIALOG_MESSAGE = "dialog_message";

    private String dialogMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stream);

	    NewRelic.withApplicationToken("AAce11642f90a6784338f628f0ddb18f1733f43da3").start(this.getApplication());

	    streamActivity = this;

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setTitle(getResources().getString(R.string.label_action_bar_popular));
        getActionBar().setLogo(this.getResources().getDrawable(R.drawable.action_bar_logo));

        dialogMessage = "Processing";
        dialog = ToolkitService.getInstance().makeIndeterminateDialog(this, "", dialogMessage);

        searchLoader = (ProgressBar)findViewById(R.id.searchLoader);
        noResults = (TextView)findViewById(R.id.noResults);

        popularButton = (ImageButton)findViewById(R.id.popular_button);
        popularButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedFilter = FilterService.getInstance().createPopularFilter();
                initiateVerifySites();
            }
        });

        recentButton = (ImageButton)findViewById(R.id.recent_button);
        recentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedFilter = FilterService.getInstance().createRecentFilter();
                initiateVerifySites();
            }
        });

        visibilityButton = (ImageButton)findViewById(R.id.visibility_button);
        visibilityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayVisibilities();
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        /*drawerToggleListener = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer, R.string.label_drawer_open, R.string.label_drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            public void onDrawerOpened(View view) {
                super.onDrawerOpened(view);
            }
        };
        drawerLayout.setDrawerListener(drawerToggleListener);*/

        units = new ArrayList<Unit>();
        allUnits = new ArrayList<Unit>();
        filters = new ArrayList<Filter>();

        grid = (GridView)findViewById(R.id.grid);
        adapter = new UnitGridAdapter(this, android.R.id.text1, units);
        adapter.setStreamActivity(this);
        grid.setAdapter(adapter);

        if (savedInstanceState != null) {
            new GetUnitsTask().execute();
            filters.clear();
            filters.addAll(FilterService.getInstance().getFilters(this));
            populateFilterDrawer();

            if (savedInstanceState.getBoolean(DIALOG)){
                dialogMessage = savedInstanceState.getString(DIALOG_MESSAGE);
                dialog.setMessage(dialogMessage);
                dialog.show();
            }
        }
        else{
            initiateVerifySites();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        savedInstanceState.putBoolean(LOADED, true);
        savedInstanceState.putString(DIALOG_MESSAGE, dialogMessage);
        savedInstanceState.putBoolean(DIALOG, dialog.isShowing());

        super.onSaveInstanceState(savedInstanceState);
    }

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	}

    @Override
    protected void onResume(){
        super.onResume();
        registerReceiver(receiver, new IntentFilter(SiteRequestService.NOTIFICATION));
        registerReceiver(receiver, new IntentFilter(UnitRequestService.NOTIFICATION));
        registerReceiver(receiver, new IntentFilter(FilterRequestService.NOTIFICATION));
        registerReceiver(connectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
        unregisterReceiver(connectivityReceiver);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        //drawerToggleListener.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.stream, menu);
        this.menu = menu;

        final SearchView searchView = new SearchView(getActionBar().getThemedContext());
        searchView.setQueryHint("Search");

        menu.add(Menu.NONE,Menu.NONE,1,"Search")
                .setIcon(R.drawable.search)
                .setActionView(searchView)
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);

        searchView.setOnQueryTextListener(queryTextListener);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == WELCOME_REQUEST) {
            if (resultCode == RESULT_OK) {
                initiateVerifySites();
            }
        }
        else if (requestCode == DETAIL_REQUEST || requestCode == IMAGE_REQUEST || requestCode == Add.ADD_REQUEST){
            if (resultCode == RESULT_OK){
                new GetUnitsTask().execute();
            }
        }
        else if (requestCode == MORE_REQUEST){
            if (resultCode == RESULT_OK){
                initiateVerifySites();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*if (drawerToggleListener.onOptionsItemSelected(item)) {
            return true;
        }*/

        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            if (selectedFilter != null){
                initiateVerifySites();
            }
            return true;
        }
        else if (id == R.id.action_more){
            goToMore();
            return true;
        }
        else if (id == R.id.action_add){
            goToAdd();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //drawerToggleListener.onConfigurationChanged(newConfig);
    }

    /*
    * UI
    */

    private void updateActionbarTitleForFilter(Filter filter){
        if (filter != null){
            String title = filter.getName();
            if (filter.getLocation().length() > 0){
                title = filter.getLocation();
            }
            getActionBar().setTitle(title);
        }
    }

    private void populateFilterDrawer(){
        if(filters==null || filters.size()==0) return;
        LinearLayout drawer = (LinearLayout)this.findViewById(R.id.drawer_content);
        drawer.removeAllViews();

        TextView filterHeader = new TextView(this);
        filterHeader.setHeight((int)ToolkitService.getInstance().convertDpToPixel(40, this));
        int padding = (int)ToolkitService.getInstance().convertDpToPixel(10, this);
        filterHeader.setPadding(padding, padding, 0, 0);
        filterHeader.setText("Filter By Status");
        filterHeader.setTextColor(this.getResources().getColor(R.color.ari_green));
        drawer.addView(filterHeader);

        ArrayList<Filter>locationFilters = new ArrayList<Filter>();
        Filter[]arr = new Filter[4];

        int counter = 1;
        for (Filter filter : filters) {
            if (filter.getLocation().length() > 0) {
                locationFilters.add(filter);
            } else {
                if (filter.getName().equals("unfinalized")){
                    filter.setName("New");
                    arr[0] = filter;
                }
                else if (filter.getName().equals("details")){
                    filter.setName("Add Details");
                    arr[1] = filter;
                }
                else if (filter.getName().equals("images")){
                    filter.setName("Add Images");
                    arr[2] = filter;
                }
                else if (filter.getName().equals("visibility")){
                    filter.setName("Add Visibility");
                    arr[3] = filter;
                }
            }
            ++counter;
        }

        for (Filter filter : Arrays.asList(arr)){
            drawer.addView(makeFilterDrawerButton(filter, counter != filters.size()));
        }

        counter = 1;
        if (locationFilters.size() > 0){
            TextView filterHeader2 = new TextView(this);
            filterHeader2.setHeight((int)ToolkitService.getInstance().convertDpToPixel(40, this));
            filterHeader2.setPadding(padding, padding, 0, 0);
            filterHeader2.setText("Filter By Location");
            filterHeader2.setTextColor(this.getResources().getColor(R.color.ari_green));
            drawer.addView(filterHeader2);

            for (Filter filter : locationFilters){
                drawer.addView(makeFilterDrawerButton(filter, counter != locationFilters.size()));
                ++counter;
            }
        }
    }

    private FilterDrawerButtonView makeFilterDrawerButton(Filter filter, boolean hasLiner){
        LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        FilterDrawerButtonView btn = (FilterDrawerButtonView)inflater.inflate(R.layout.filter_drawer_button_view,null);
        btn.setFilter(filter);

        if (hasLiner){
            View liner = (View)btn.findViewById(R.id.liner);
            liner.setVisibility(View.VISIBLE);
        }

        btn.setOnClickListener(filterDrawerButtonClickListener);

        String nameString = filter.getName();
        if (filter.getLocation().length() > 0){
            nameString = filter.getLocation();
        }

        TextView name = (TextView)btn.findViewById(R.id.filter_drawer_button_view_text);
        name.setText(nameString);

        TextView countLabel = (TextView)btn.findViewById(R.id.filter_drawer_button_view_count_text);
        countLabel.setText(""+filter.getCount());

        return btn;
    }

    /*
    * ACTIONS
    */

    private void goToWelcome(){
        Intent intent = new Intent(this.getApplicationContext(), Welcome.class);
        startActivityForResult(intent, WELCOME_REQUEST);
    }

    private void goToAdd(){
        Intent intent = new Intent(this.getApplicationContext(), Add.class);
        startActivityForResult(intent, Add.ADD_REQUEST);
    }

    public void goToDetail(Unit unit, boolean shouldScroll){
        Intent intent = new Intent(this.getApplicationContext(), Detail.class);
        intent.putExtra(Detail.UNIT_ID, unit.getUnitId());
        intent.putExtra(Detail.LOCAL_UNIT_ID, unit.getLocalId());
        intent.putExtra(Detail.SHOULD_SCROLL, shouldScroll);
        startActivityForResult(intent, DETAIL_REQUEST);
    }

    public void goToImages(Unit unit){
        Intent intent = new Intent(this.getApplicationContext(), ImageManager.class);
        intent.putExtra(ImageManager.UNIT_ID, unit.getUnitId());
        intent.putExtra(ImageManager.LOCAL_UNIT_ID, unit.getLocalId());
        startActivityForResult(intent, IMAGE_REQUEST);
    }

    public void goToMore(){
        Intent intent = new Intent(this.getApplicationContext(), More.class);
        startActivityForResult(intent, MORE_REQUEST);
    }

    private void updateUIForConnection(){
        boolean isOnline = ConnectionService.getInstance().isOnline(getApplicationContext());
        popularButton.setEnabled(isOnline);
        popularButton.setAlpha((float)(isOnline ? 1.0 : 0.25));
        recentButton.setEnabled(isOnline);
        recentButton.setAlpha((float)(isOnline ? 1.0 : 0.25));
        visibilityButton.setEnabled(isOnline);
        visibilityButton.setAlpha((float)(isOnline ? 1.0 : 0.25));

        if (menu != null){
            for (int i = 0; i < menu.size(); ++i){
                menu.getItem(i).setVisible(isOnline);
            }
        }

        drawerLayout.setDrawerLockMode(isOnline ? DrawerLayout.LOCK_MODE_UNLOCKED : DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        getActionBar().setDisplayHomeAsUpEnabled(isOnline);
        getActionBar().setHomeButtonEnabled(isOnline);
    }

	private void displayAlertWithMessage(String title, String message){
		new AlertDialog.Builder(this)
				.setTitle(title)
				.setMessage(message)
				.setNegativeButton("Dismiss", null)
				.show();
	}

    private void initiateVerifySites(){
        dialogMessage = "Verifying Activation";
        dialog.setMessage(dialogMessage);
        dialog.show();

        Intent ui = new Intent(this, SiteRequestService.class);
        ui.putExtra(SiteRequestService.VERIFY, true);
        startService(ui);
    }

    private int offlineCount = 0;

    private void initiateOfflineSync(){
        dialogMessage = "Syncing Offline Data";
        dialog.setMessage(dialogMessage);

        ArrayList<Unit>offlineUnits = UnitService.getInstance().getOfflineUnits(getApplicationContext());
        offlineCount = offlineUnits.size();

        if (offlineCount > 0){
            UnitService.getInstance().syncOfflineData(getApplicationContext(), offlineUnits);
        }
        else{
            initiateAdditionalData();
        }
    }

    private void initiateAdditionalData(){
        if (ConnectionService.getInstance().isOnline(getApplicationContext())) {
            Intent fi = new Intent(this, FilterRequestService.class);
            fi.putExtra(FilterRequestService.GET, true);
            startService(fi);

            Intent ui = new Intent(this, LocationRequestService.class);
            ui.putExtra(LocationRequestService.GET, true);
            startService(ui);

            Intent vi = new Intent(this, VisibilityRequestService.class);
            vi.putExtra(VisibilityRequestService.GET, true);
            startService(vi);

            Intent ai = new Intent(this, AttributeRequestService.class);
            ai.putExtra(AttributeRequestService.GET, true);
            startService(ai);
        }
    }

    private void initiateUpdateUnits(){
        Site site = SiteService.getInstance().getSite(this.getApplicationContext());
        if (site != null){
            if (selectedFilter == null){
                selectedFilter = FilterService.getInstance().createPopularFilter();
            }
            updateUnitsForFilter();
        }
        else{
            goToWelcome();
        }
    }

    private void initiateOfflineImageSync(){
        ArrayList<Image>offlineImages = ImageService.getInstance().getOfflineImages(getApplicationContext());

        if (offlineImages.size() > 0){
            ImageService.getInstance().syncOfflineImages(getApplicationContext(), offlineImages);
        }
    }

    private void updateUnitsForFilter(){
        if (selectedFilter != null){
            dialogMessage = "Retrieving Units";
            dialog.setMessage(dialogMessage);
            dialog.show();

            UnitService.getInstance().getUnitsForQuery(this, selectedFilter.getQuery());
        }
    }

    private View.OnClickListener filterDrawerButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View btn) {
            final Filter filter = ((FilterDrawerButtonView)btn).getFilter();
            selectedFilter = filter;
            new Thread() {
                @Override
                public void run() {
                    SystemClock.sleep(500);
                    streamActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            initiateVerifySites();
                        }
                    });
                }
            }.start();
            drawerLayout.closeDrawers();
        }
    };

    private SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String s) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String s) {
            if (s.length() >= 3) {
                noResults.setVisibility(View.GONE);
                searchLoader.setVisibility(View.VISIBLE);
                UnitService.getInstance().getUnitsForSearch(getApplicationContext(), s);
            }
            return false;
        }
    };

	VisibilityDialogFragment vdf;
    private void displayVisibilities(){
	    vdf = new VisibilityDialogFragment();
	    vdf.show(getFragmentManager(), "VisibilityDialogFragment");
    }

	public AdapterView.OnItemClickListener dialogOnClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            vdf = (VisibilityDialogFragment)getFragmentManager().findFragmentByTag("VisibilityDialogFragment");
            if(vdf!=null)
			   vdf.dismiss();
			FragmentDialogVisibilityItem fdv = (FragmentDialogVisibilityItem)view;
			selectedFilter = FilterService.getInstance().createVisibilityFilterForName(fdv.getItemVisibility().getName());
			initiateVerifySites();
		}
	};

     /*
     * BROADCAST RECEIVER AND HANDLERS
     */

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();

            if (bundle != null) {
                String sender = bundle.getString("sender");

                if (sender.equals("unit_request_service")) {
                    handleUnitRequestServiceResult(bundle);
                }
                else if (sender.equals("filter_request_service")){
                    handleFilterRequestServiceResult(bundle);
                }
                else if (sender.equals("site_request_service")){
                    handleSiteRequestServiceResult(bundle);
                }
            }
        }
    };

    private void handleSiteRequestServiceResult(Bundle bundle){
        int resultCode = bundle.getInt("result");
        boolean verify = bundle.getBoolean(SiteRequestService.VERIFY);

        if (resultCode == Activity.RESULT_OK) {
            if (verify){
                initiateOfflineSync();
            }
        }
        else{
            dialog.dismiss();
            if (verify){
                int statusCode = bundle.getInt(SiteRequestService.CODE);

                if (statusCode == SiteRequestService.OFFLINE || statusCode == SiteRequestService.TIMEOUT){

                    if (statusCode == SiteRequestService.TIMEOUT){
                        ToolkitService.getInstance().makeToast(this.streamActivity, this.getResources().getString(R.string.toast_network_timeout));
                    }

                    if (SiteService.getInstance().getSite(getApplicationContext()) != null){
                        updateUIForConnection();
                        new GetUnitsTask().execute();
                    }
                    else {
                        goToWelcome();
                    }
                }
                else {
                    goToWelcome();
                }
            }
        }
    }

    private void handleFilterRequestServiceResult(Bundle bundle){

        int resultCode = bundle.getInt("result");
        boolean get = bundle.getBoolean(UnitRequestService.GET);

        if (resultCode == Activity.RESULT_OK) {
            if (get){
                filters.clear();
                filters.addAll(FilterService.getInstance().getFilters(this));
                populateFilterDrawer();
                initiateUpdateUnits();
            }
        }
        else{
            if (get){
                //COULDN'T GET RESULTS
            }
        }
    }

    private void handleUnitRequestServiceResult(Bundle bundle){
        int resultCode = bundle.getInt("result");
        boolean get = bundle.getBoolean(UnitRequestService.GET);
        boolean put = bundle.getBoolean(UnitRequestService.PUT);
        boolean offlineRequest = bundle.getBoolean(UnitRequestService.OFFLINE_REQUEST);

        searchLoader.setVisibility(View.GONE);
        noResults.setVisibility(View.GONE);

        if (resultCode == Activity.RESULT_OK) {
            if (get){
                new GetUnitsTask().execute();
                initiateOfflineImageSync();
            }
        }
        else{
            if (get){
                handleNetworkError(bundle.getInt(UnitRequestService.CODE));
            }
        }

        if (offlineRequest){
            --offlineCount;
            if (offlineCount == 0){
                initiateAdditionalData();
            }
        }
    }

    private void handleNetworkError(int statusCode){
        dialog.dismiss();
        if (statusCode == 401){
            goToWelcome();
        }
    }

    private BroadcastReceiver connectivityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)){
                updateUIForConnection();
            }
        }
    };

    /*
     * ASYNC TASKS
     */

    private class GetUnitsTask extends AsyncTask<String, Void, ArrayList<Unit>> {

        protected ArrayList<Unit> doInBackground(String... urls) {
            return UnitService.getInstance().getAllUnits(getApplicationContext());
        }

        protected void onPostExecute(ArrayList<Unit> results) {
            if (results != null){

                //protects from rotation change crashes
                if (dialog != null) {
                    dialog.dismiss();
                }
                else{
                    dialog = ToolkitService.getInstance().makeIndeterminateDialog(getApplicationContext(), "", dialogMessage);
                    dialog.dismiss();
                }

                units.clear();
                allUnits.clear();

                units.addAll(results);

                UnitService.getInstance().sortUnits(units);

                allUnits.addAll(results);

                adapter.notifyDataSetChanged();

                updateActionbarTitleForFilter(selectedFilter);

            }
            if (results.size() > 0){
                noResults.setVisibility(View.GONE);
            }
            else{
                noResults.setVisibility(View.VISIBLE);
            }
        }
    }

}
