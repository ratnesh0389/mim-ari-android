package arinet.com.mim.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;

import arinet.com.mim.R;
import arinet.com.mim.service.filter.FilterService;
import arinet.com.mim.service.site.SiteRequestService;
import arinet.com.mim.service.toolkit.ToolkitService;
import arinet.com.mim.utils.Utils;

public class Welcome extends Activity {

    private EditText et;
    private Button btn;
	private Button helpButton;
    private ProgressDialog dialog;

    static final String DIALOG = "dialog";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getActionBar().hide();

        setContentView(R.layout.activity_welcome);

        et = (EditText)findViewById(R.id.authorize_textfield);
        et.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    submitCode(et.getText().toString().trim());
                    handled = true;
                }
                return handled;
            }
        });
        btn = (Button)findViewById(R.id.authorize_button);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitCode(et.getText().toString().trim());
            }
        });

	    helpButton = (Button)findViewById(R.id.help_button);
	    helpButton.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View view) {
			    Intent intent = new Intent(Welcome.this, Help.class);
			    startActivity(intent);
		    }
	    });

        dialog = ToolkitService.getInstance().makeIndeterminateDialog(this, "", this.getResources().getString(R.string.notification_authorization_request));

        if (savedInstanceState != null
                && savedInstanceState.getBoolean(DIALOG)) {
            dialog.show();
        }

        findViewById(R.id.privacyPolicyTv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.openPrivacyBrowser(Welcome.this);
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(DIALOG, dialog.isShowing());
        super.onSaveInstanceState(savedInstanceState);
    }

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}

    @Override
    protected void onResume(){
        super.onResume();

        registerReceiver(receiver, new IntentFilter(SiteRequestService.NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(receiver);
    }

    @Override
    public void onBackPressed() {
        //
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void submitCode(String code){
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(et.getWindowToken(), 0);

        if (code.length() > 0){

            lockScreenOrientation();

            dialog.show();
            Intent intent = new Intent(this, SiteRequestService.class);
            intent.putExtra(SiteRequestService.ADD, true);
            intent.putExtra(SiteRequestService.CODE, code);
            startService(intent);
        }
        else{
            displayAlert();
        }
    }

    private void displayAlert(){

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setMessage(R.string.notification_authorization_failed)
                .setPositiveButton("Dismiss",null)
                .show();
        doKeepDialog(dialog);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();

            if (bundle != null) {
                String sender = bundle.getString("sender");

                if (sender.equals("site_request_service")) {
                    handleSiteRequestServiceResult(bundle);
                }
            }

            unlockScreenOrientation();
        }
    };

    private void handleSiteRequestServiceResult(Bundle bundle){
        int resultCode = bundle.getInt("result");
        int statusCode = bundle.getInt(SiteRequestService.CODE);
        boolean add = bundle.getBoolean(SiteRequestService.ADD);

        dialog.dismiss();

        if (resultCode == Activity.RESULT_OK) {
            if (add){
                Intent returnIntent = new Intent();
                setResult(RESULT_OK,returnIntent);
                finish();
            }
        }
        else{
            if (add){
                if (statusCode == SiteRequestService.TIMEOUT){
                    ToolkitService.getInstance().makeToast(this, this.getResources().getString(R.string.toast_network_timeout));
                }
                displayAlert();

            }
        }
    }

    private static void doKeepDialog(Dialog dialog){
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
    }


    private void lockScreenOrientation() {
        int currentOrientation = getResources().getConfiguration().orientation;
        if (currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    private void unlockScreenOrientation() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
    }
}
