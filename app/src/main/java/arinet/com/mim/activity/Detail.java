package arinet.com.mim.activity;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.google.analytics.tracking.android.EasyTracker;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import arinet.com.mim.R;
import arinet.com.mim.fragment.DescriptionDialogFragment;
import arinet.com.mim.fragment.HeroImageFragment;
import arinet.com.mim.model.Image;
import arinet.com.mim.model.Location;
import arinet.com.mim.model.Unit;
import arinet.com.mim.model.Visibility;
import arinet.com.mim.service.database.DatabaseService;
import arinet.com.mim.service.image.ImageService;
import arinet.com.mim.service.location.LocationService;
import arinet.com.mim.service.toolkit.ToolkitService;
import arinet.com.mim.service.unit.UnitRequestService;
import arinet.com.mim.service.unit.UnitService;
import arinet.com.mim.service.visibility.VisibilityService;
import arinet.com.mim.utils.StringUtil;
import arinet.com.mim.view.DetailVisibilitySwitchView;

public class Detail extends FragmentActivity {

    static final String UNIT_ID = "unit_id";
    static final String LOCAL_UNIT_ID = "local_unit_id";
    static final String SHOULD_SCROLL = "should_scroll";

    private final int ADDITIONAL_DETAIL_REQUEST_CODE = 1;

    private Unit unit;
    private Button saleStartButton;

    public Unit getUnit() { return unit;}

    private Spinner locationSpinner;
    private Spinner priceLabelSpinner;
    private Spinner titleSpinner;
    private Spinner conditionSpinner;
    private Spinner usageUnitSpinner;

    private Switch oemActiveSwitch;
    private EditText priceText;
    private LinearLayout saleGroup;
    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;
    private ProgressDialog dialog;
    private DatePickerDialog datePickerDialog;
    private Button saleEndButton;
    private EditText usageValueText;
    private ImageView imageContainer;
    private Button descriptionButton;
    private AlertDialog diag = null;

    private Menu menu;
    private LinearLayout usageValueLayout;
    private LinearLayout usageUnitLayout;
    private LinearLayout buttonGroup;
    private LinearLayout buttonGroup2;
    private LinearLayout showRoomContainer;
    private LinearLayout showRoomGroup;
    private LinearLayout salesChannelGroup;
    private LinearLayout ariLayout;
    private LinearLayout additionalFieldsLayout;

    private List<Fragment> fragmentsList;

    private ArrayList<Location>locations;
    private ArrayList<String> priceLabelStrings;
    private ArrayList<String> titleStrings;
    private ArrayList<String> conditionStrings;
    private ArrayList<String> metricStrings;
    private ArrayList<String> saleTypeStrings;

    private boolean isConfirmationDisplayed;

    static final String DIALOG = "dialog";
    static final String DIALOG_MESSAGE = "dialog_message";
    private String dialogMessage;
    int unitId;
    long localUnitId;

    //Workaround because object returned by add unit does not include an industryId
    private int industryId = 0;

    enum State {
        SAVED,
        UNSAVED,
        UNFINALIZED,
    }

    State state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setLogo(this.getResources().getDrawable(R.drawable.action_bar_logo));

        isConfirmationDisplayed = false;
        dialogMessage = "Saving";
        dialog = ToolkitService.getInstance().makeIndeterminateDialog(this, "", dialogMessage);
        if(savedInstanceState==null){
            unitId = getIntent().getExtras().getInt(UNIT_ID,0);
            localUnitId = getIntent().getExtras().getLong(LOCAL_UNIT_ID);
        }else{
            unitId = savedInstanceState.getInt(UNIT_ID,0);
            localUnitId = savedInstanceState.getLong(LOCAL_UNIT_ID);
        }


        boolean shouldScroll = getIntent().getExtras().getBoolean(SHOULD_SCROLL);

        if (unitId > 0){
            this.state = State.SAVED;
            unit = UnitService.getInstance().getUnitWithUnitId(this.getApplicationContext(), unitId);
        }
        else if (localUnitId > 0){
            this.state = State.UNSAVED;
            unit = UnitService.getInstance().getUnitWithLocalUnitId(this.getApplicationContext(), localUnitId);
            if (unit!=null && !unit.getInventoryStatusValue().toLowerCase().equals("finalized")){
                this.state = State.UNFINALIZED;
            }
            else if (unit!=null && unit.getVisibilities().size() > 0){
                this.state = State.SAVED;
            }
        }

        if (unit != null) {

            industryId = unit.getIndustryId();

            ariLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_fifty_below);
            TextView ariLabel = (TextView) ariLayout.findViewById(R.id.unit_detail_layout_label);
            ariLabel.setText("ARI Website");
            Switch ariSwitch = (Switch) ariLayout.findViewById(R.id.unit_detail_switch);
            ariSwitch.setChecked(unit.isPublishedOnWeb());
            ariSwitch.setOnCheckedChangeListener(new SharedOnCheckChangedListener(ariLayout));

            populateView();

            imageContainer = (ImageView)findViewById(R.id.image_container);
            if (unit.getImages().size() > 0){
                Image image = unit.getImages().get(0);
                if (image.getLocation() != null){
                    Picasso.with(getApplicationContext()).load(unit.getImages().get(0).getLocation());
                }
                else if (image.getLocalPath() != null){
                    Picasso.with(getApplicationContext()).load(Uri.parse(image.getLocalPath()));
                }
            }
        }

        if (shouldScroll){
            sendScroll();
        }

        if (savedInstanceState != null) {

            if (savedInstanceState.getBoolean(DIALOG)){
                dialogMessage = savedInstanceState.getString(DIALOG_MESSAGE);
                dialog.setMessage(dialogMessage);
                dialog.show();
            }else{
                dialog.dismiss();
            }
        }
    }

    private void populateView(){
        viewPager = (ViewPager) findViewById(R.id.pager);

        initializePaging();

        Button finalizeButton = (Button) findViewById(R.id.finalize_button);
        finalizeButton.setOnClickListener(new FinalizeOnClickListener());
        finalizeButton.setVisibility(View.GONE);
        if (this.state == State.UNFINALIZED){
            finalizeButton.setVisibility(View.VISIBLE);
        }

        TextView heroTitle = (TextView) findViewById(R.id.hero_unit_title);
        heroTitle.setText(unit.getYear() + " " + unit.getMake() + " " + unit.getModel());

        getActionBar().setTitle(unit.getYear() + " " + unit.getMake() + " " + unit.getModel());

        TextView heroImagesCount = (TextView) findViewById(R.id.hero_unit_images);
        heroImagesCount.setText(unit.getImages().size() + " images");

        TextView heroViewCount = (TextView) findViewById(R.id.hero_unit_views);
        heroViewCount.setText(unit.getHitsThirty() + " views this month");

        ImageView viewsIcon = (ImageView) findViewById(R.id.imageview_views_icon);

        if (unit.getHitsThirty() > 0){
            heroViewCount.setVisibility(View.VISIBLE);
            viewsIcon.setVisibility(View.VISIBLE);
        }
        else{
            heroViewCount.setVisibility(View.GONE);
            viewsIcon.setVisibility(View.GONE);
        }

        View shareView = (View)findViewById(R.id.detail_share_button_view);
        shareView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (state != State.UNFINALIZED){
                    shareUnit(unit);
                }
            }
        });

        locations = LocationService.getInstance().getLocations(this.getApplicationContext());
        ArrayList<String>locationStrings = new ArrayList<String>();

        int locationIndex = 0;
        int counter = 0;
        for (Location location : locations){
            if (location.getLocationId() == unit.getLocationId()){
                locationIndex = counter;
            }
            else{
                ++counter;
            }
            locationStrings.add(location.getName());
        }
        ArrayAdapter<String> locationsAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_row_item, locationStrings);
        locationsAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);

        buttonGroup = (LinearLayout) findViewById(R.id.unit_detail_button_group_1);
        LinearLayout imageButtonView = (LinearLayout)findViewById(R.id.detail_photos_button_view);
        imageButtonView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                goToImage();
            }
        });

        LinearLayout locationLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_location);
        TextView locationLabel = (TextView) locationLayout.findViewById(R.id.unit_detail_layout_label);
        locationLabel.setText("Location");

        locationSpinner = (Spinner) locationLayout.findViewById(R.id.unit_detail_spinner);
        locationSpinner.setAdapter(locationsAdapter);
        locationSpinner.setSelection(locationIndex, false);
        locationSpinner.setOnItemSelectedListener(new SharedItemSelectedListener(locationLayout));

        LinearLayout vinLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_vin);
        TextView vinLabel = (TextView) vinLayout.findViewById(R.id.unit_detail_layout_label);
        EditText vinText = (EditText) vinLayout.findViewById(R.id.unit_detail_layout_edit_text);
        vinText.setText(unit.getVin());
        vinText.addTextChangedListener(new SharedTextWatcher(vinText));
        vinText.setOnEditorActionListener(new SharedOnEditorActionListener());
        vinLabel.setText("VIN");

        LinearLayout stockLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_stock);
        TextView stockLabel = (TextView) stockLayout.findViewById(R.id.unit_detail_layout_label);
        EditText stockText = (EditText) stockLayout.findViewById(R.id.unit_detail_layout_edit_text);
        stockText.setText(unit.getStockNumber());
        stockText.addTextChangedListener(new SharedTextWatcher(stockText));
        stockText.setOnEditorActionListener(new SharedOnEditorActionListener());
        stockLabel.setText("Stock #");

        priceLabelStrings = new ArrayList<String>(Arrays.asList(UnitService.getInstance().getPriceLabels()));
        ArrayAdapter<String> priceLabelAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_row_item, priceLabelStrings);
        priceLabelAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);

        LinearLayout priceLabelLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_price_label);
        TextView priceLabelLabel = (TextView) priceLabelLayout.findViewById(R.id.unit_detail_layout_label);
        priceLabelLabel.setText("Price Label");

        priceLabelSpinner = (Spinner) priceLabelLayout.findViewById(R.id.unit_detail_spinner);
        priceLabelSpinner.setAdapter(priceLabelAdapter);

        int selectedPriceIndex = this.unit.getPriceLabel();
        if (selectedPriceIndex > 2){
            selectedPriceIndex = selectedPriceIndex - 1;
        }

        priceLabelSpinner.setSelection(selectedPriceIndex, false);
        priceLabelSpinner.setOnItemSelectedListener(new SharedItemSelectedListener(priceLabelLayout));

        LinearLayout priceLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_price);
        TextView priceLabel = (TextView) priceLayout.findViewById(R.id.unit_detail_layout_label);
        priceText = (EditText) priceLayout.findViewById(R.id.unit_detail_layout_edit_text);
        updatePriceFieldForLabel();
        priceText.addTextChangedListener(new SharedTextWatcher(priceText));
        priceText.setOnEditorActionListener(new SharedOnEditorActionListener());
        priceLabel.setText("Price");

        LinearLayout bestPriceLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_best_price);
        TextView bestPriceLabel = (TextView) bestPriceLayout.findViewById(R.id.unit_detail_layout_label);
        EditText bestPriceText = (EditText) bestPriceLayout.findViewById(R.id.unit_detail_layout_edit_text);
        if (unit.getBestPrice() > 0){
            bestPriceText.setText(String.format( "%.2f", unit.getBestPrice() ));
        }
        else{
            bestPriceText.setText("");
        }
        bestPriceText.addTextChangedListener(new SharedTextWatcher(bestPriceText));
        bestPriceText.setOnEditorActionListener(new SharedOnEditorActionListener());
        bestPriceLabel.setText("Best Price");

        LinearLayout descriptionLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_description);
        descriptionButton = (Button) descriptionLayout.findViewById(R.id.unit_detail_description_button);
        updateDescriptionButtonText(unit.getDescription());
        descriptionButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                DialogFragment newFragment = new DescriptionDialogFragment();
                newFragment.show(ft, "dialog");
            }
        });

        LinearLayout colorLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_color);
        TextView colorLabel = (TextView) colorLayout.findViewById(R.id.unit_detail_layout_label);
        EditText colorText = (EditText) colorLayout.findViewById(R.id.unit_detail_layout_edit_text);
        colorText.setText(unit.getPrimaryColor());
        colorText.addTextChangedListener(new SharedTextWatcher(colorText));
        colorText.setOnEditorActionListener(new SharedOnEditorActionListener());
        colorLabel.setText("Color");

        LinearLayout trimColorLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_trim_color);
        TextView trimColorLabel = (TextView) trimColorLayout.findViewById(R.id.unit_detail_layout_label);
        EditText trimColorText = (EditText) trimColorLayout.findViewById(R.id.unit_detail_layout_edit_text);
        trimColorText.setText(unit.getTrimColor());
        trimColorText.addTextChangedListener(new SharedTextWatcher(trimColorText));
        trimColorText.setOnEditorActionListener(new SharedOnEditorActionListener());
        trimColorLabel.setText("Trim Color");

        titleStrings = new ArrayList<String>(Arrays.asList(UnitService.getInstance().getTitleStatus()));
        ArrayAdapter<String> titleAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_row_item, titleStrings);
        titleAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);

        LinearLayout titleLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_title);
        TextView titleLabel = (TextView) titleLayout.findViewById(R.id.unit_detail_layout_label);
        titleLabel.setText("Title");

        titleSpinner = (Spinner) titleLayout.findViewById(R.id.unit_detail_spinner);
        titleSpinner.setAdapter(titleAdapter);
        titleSpinner.setSelection(this.unit.getTitleStatus(), false);
        titleSpinner.setOnItemSelectedListener(new SharedItemSelectedListener(titleLayout));

        conditionStrings = new ArrayList<String>(Arrays.asList(UnitService.getInstance().getConditions()));
        ArrayAdapter<String> conditionAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_row_item, conditionStrings);
        conditionAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);

        LinearLayout conditionLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_condition);
        TextView conditionLabel = (TextView) conditionLayout.findViewById(R.id.unit_detail_layout_label);
        conditionLabel.setText("Condition");
        conditionSpinner = (Spinner) conditionLayout.findViewById(R.id.unit_detail_spinner);
        conditionSpinner.setAdapter(conditionAdapter);
        conditionSpinner.setSelection(this.unit.getCondition(), false);
        conditionSpinner.setOnItemSelectedListener(new SharedItemSelectedListener(conditionLayout));

        DetailVisibilitySwitchView sellAsNewLayout = (DetailVisibilitySwitchView) findViewById(R.id.unit_detail_layout_sell_as_new);
        TextView sellAsNewLabel = (TextView) sellAsNewLayout.findViewById(R.id.unit_detail_layout_label);
        Switch sellAsNewSwitch = (Switch) sellAsNewLayout.findViewById(R.id.unit_detail_switch);
        sellAsNewSwitch.setOnCheckedChangeListener(new SharedOnCheckChangedListener(sellAsNewLayout));
        sellAsNewSwitch.setChecked(unit.getUsageStatus() == 2);
        sellAsNewLabel.setText("Sell As New");

        metricStrings = new ArrayList<String>();

        usageUnitLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_usage_unit);
        TextView usageUnitLabel = (TextView) usageUnitLayout.findViewById(R.id.unit_detail_layout_label);
        usageUnitLabel.setText("Usage Unit");
        usageUnitSpinner = (Spinner) usageUnitLayout.findViewById(R.id.unit_detail_spinner);

        usageValueLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_usage_value);
        TextView usageValueLabel = (TextView) usageValueLayout.findViewById(R.id.unit_detail_layout_label);
        usageValueText = (EditText) usageValueLayout.findViewById(R.id.unit_detail_layout_edit_text);
        usageValueText.setText("");
        usageValueText.addTextChangedListener(new SharedTextWatcher(usageValueText));
        usageValueText.setOnEditorActionListener(new SharedOnEditorActionListener());
        usageValueLabel.setText("Usage");

        updateMetrics();

        additionalFieldsLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_additional_details);

        TextView additionalFieldsLabel = (TextView) additionalFieldsLayout.findViewById(R.id.unit_detail_layout_label);
        additionalFieldsLabel.setText("Additional Details");

        Button additionalFieldsButton = (Button) findViewById(R.id.unit_detail_button);
        additionalFieldsButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AdditionalDetails.class);
                intent.putExtra(AdditionalDetails.UNIT_ID, unit.getUnitId());
                startActivityForResult(intent,ADDITIONAL_DETAIL_REQUEST_CODE);
            }
        });

        additionalFieldsLayout.setVisibility(View.GONE);

        if (unit.getUnitId() > 0 && (industryId == 7 || industryId == 8 || industryId == 11)){
            additionalFieldsLayout.setVisibility(View.VISIBLE);
        }

        saleGroup = (LinearLayout) findViewById(R.id.unit_detail_sale_group);

        DetailVisibilitySwitchView saleActiveLayout = (DetailVisibilitySwitchView) findViewById(R.id.unit_detail_layout_sale_active);
        TextView saleActiveLabel = (TextView) saleActiveLayout.findViewById(R.id.unit_detail_layout_label);
        saleActiveLabel.setText("Sale Active");
        Switch saleActiveSwitch = (Switch) saleActiveLayout.findViewById(R.id.unit_detail_switch);

        if (unit.isSaleActive()) {
            saleGroup.setVisibility(View.VISIBLE);
            saleActiveSwitch.setChecked(true);
        }
        saleActiveSwitch.setOnCheckedChangeListener(new SharedOnCheckChangedListener(saleActiveLayout));


        saleTypeStrings = new ArrayList<String>(Arrays.asList(UnitService.getInstance().getSaleTypes()));
        ArrayAdapter<String> saleTypeAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_row_item, saleTypeStrings);
        saleTypeAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);


        LinearLayout saleTypeLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_sale_type);
        TextView saleTypeLabel = (TextView) saleTypeLayout.findViewById(R.id.unit_detail_layout_label);
        saleTypeLabel.setText("Sale Type");

        Spinner saleTypeSpinner = (Spinner) saleTypeLayout.findViewById(R.id.unit_detail_spinner);
        saleTypeSpinner.setAdapter(saleTypeAdapter);
        saleTypeSpinner.setSelection(UnitService.getInstance().getSaleTypeIntFromString(unit.getSaleType()), false);
        saleTypeSpinner.setOnItemSelectedListener(new SharedItemSelectedListener(saleTypeLayout));


        LinearLayout salePriceLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_sale_price);
        TextView salePriceLabel = (TextView) salePriceLayout.findViewById(R.id.unit_detail_layout_label);
        salePriceLabel.setText("Sale Price");
        EditText salePriceText = (EditText) salePriceLayout.findViewById(R.id.unit_detail_layout_edit_text);
        if (unit.getSalePrice() > 0){
            salePriceText.setText(String.format( "%.2f", unit.getSalePrice() ));
        }
        else{
            salePriceText.setText("");
        }
        salePriceText.addTextChangedListener(new SharedTextWatcher(salePriceText));
        salePriceText.setOnEditorActionListener(new SharedOnEditorActionListener());

        LinearLayout saleStartLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_sale_start);
        TextView saleStartLabel = (TextView) saleStartLayout.findViewById(R.id.unit_detail_layout_label);
        saleStartLabel.setText("Sale Start");

        saleStartButton = (Button) saleStartLayout.findViewById(R.id.unit_detail_button);
        saleStartButton.setOnClickListener(new SharedOnClickListener(saleStartLayout));
        saleStartButton.setText(ToolkitService.getInstance().formatDateStringForDisplay(unit.getSaleStartDate()));

        LinearLayout saleEndLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_sale_end);
        TextView saleEndLabel = (TextView) saleEndLayout.findViewById(R.id.unit_detail_layout_label);
        saleEndLabel.setText("Sale End");

        saleEndButton = (Button) saleEndLayout.findViewById(R.id.unit_detail_button);
        saleEndButton.setOnClickListener(new SharedOnClickListener(saleEndLayout));
        saleEndButton.setText(ToolkitService.getInstance().formatDateStringForDisplay(unit.getSaleEndDate()));

        LinearLayout oemLayout = (LinearLayout) findViewById(R.id.unit_detail_layout_oem_details);
        TextView oemLabel = (TextView) oemLayout.findViewById(R.id.unit_detail_layout_label);
        oemLabel.setText("Use OEM Imagery/Info");

        oemActiveSwitch = (Switch) oemLayout.findViewById(R.id.unit_detail_switch);
        oemActiveSwitch.setChecked(unit.getLinkedProductId() > 0);
        oemActiveSwitch.setOnCheckedChangeListener(new SharedOnCheckChangedListener(oemLayout));

        showRoomContainer = (LinearLayout) findViewById(R.id.showroom_container);
        showRoomGroup = (LinearLayout) findViewById(R.id.unit_detail_showroom_group);
        salesChannelGroup = (LinearLayout) findViewById(R.id.unit_detail_sale_channel_group);
        int showRoomCount = 0;

        if (unit.getVisibilities().size() > 0) {
            updateVisibilities();
        }

        buttonGroup2 = (LinearLayout) findViewById(R.id.unit_detail_button_group_2);
        LinearLayout deleteButtonView = (LinearLayout) findViewById(R.id.detail_remove_button_view);
        deleteButtonView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                displayDeleteConfirmation();
            }
        });

        LinearLayout duplicateButtonView = (LinearLayout) findViewById(R.id.detail_duplicate_button_view);
        duplicateButtonView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                duplicateUnit();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        savedInstanceState.putString(DIALOG_MESSAGE, dialogMessage);
        savedInstanceState.putBoolean(DIALOG, dialog.isShowing());
        savedInstanceState.putInt(UNIT_ID,unitId);
        savedInstanceState.putLong(LOCAL_UNIT_ID,localUnitId);
        super.onSaveInstanceState(savedInstanceState);
    }


    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();

        if(diag != null)
            diag.dismiss();

        EasyTracker.getInstance(this).activityStop(this);
    }

    @Override
    protected void onResume(){
        super.onResume();
        registerReceiver(receiver, new IntentFilter(UnitRequestService.NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (dialog != null){
            dialog.dismiss();
            dialog = null;
        }

        if (datePickerDialog != null){
            datePickerDialog.dismiss();
            datePickerDialog = null;
        }
    }

    private void sendScroll(){
        final ScrollView scrollView = (ScrollView)findViewById(R.id.detail_scroller);
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {Thread.sleep(100);} catch (InterruptedException e) {}
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.fullScroll(View.FOCUS_DOWN);
                    }
                });
            }
        }).start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail, menu);
        this.setMenu(menu);
        updateView(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        dialogMessage = "Saving";
        dialog.setMessage(dialogMessage);
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent returnIntent = new Intent();
                setResult(RESULT_OK,returnIntent);
                finish();
                return true;
            case R.id.action_save:
                saveData();
                return true;
            case R.id.action_add:
                setRequestedOrientation(getResources().getConfiguration().orientation);
                dialog.show();
                UnitService.getInstance().addUnit(getApplicationContext(), unit, false);
                return true;
            case R.id.action_finalize:
                setRequestedOrientation(getResources().getConfiguration().orientation);
                if (unit.getInventoryStatusValue().toLowerCase().equals("finalized")) {
                    dialog.show();
                    UnitService.getInstance().addUnit(getApplicationContext(), unit, false);
                }
                else{
                    goToAdd();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateView(boolean updateAdditionalFields){

        try{
            this.getMenu().findItem(R.id.action_add).setVisible(this.state == State.UNSAVED);
            this.getMenu().findItem(R.id.action_save).setVisible(this.state == State.SAVED);
            this.getMenu().findItem(R.id.action_finalize).setVisible(this.state == State.UNFINALIZED);

            usageUnitLayout.setVisibility(this.state == State.SAVED ? View.VISIBLE : View.GONE);
            usageValueLayout.setVisibility(this.state == State.SAVED ? View.VISIBLE : View.GONE);

            buttonGroup.setVisibility(this.state == State.SAVED ? View.VISIBLE : View.GONE);
            buttonGroup2.setVisibility(this.state == State.SAVED ? View.VISIBLE : View.GONE);

            if (unit.getUnitId() > 0 && (industryId == 7 || industryId == 8 || industryId == 11)){
                additionalFieldsLayout.setVisibility(View.VISIBLE);
            }

            if (updateAdditionalFields){
                updateVisibilities();
                updateMetrics();
            }
        }catch(Exception e){

        }
    }

    private void updateVisibilities(){
        showRoomGroup.removeAllViews();
        salesChannelGroup.removeAllViews();

        int showRoomCount = 0;

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        salesChannelGroup.addView(ariLayout);

        if (unit.getVisibilities() != null){
            for (Visibility visibility : unit.getVisibilities()) {

                DetailVisibilitySwitchView switchView = (DetailVisibilitySwitchView) inflater.inflate(R.layout.detail_toggle_layout, null);
                switchView.setSwitchVisibility(visibility);
                TextView switchLabel = (TextView) switchView.findViewById(R.id.unit_detail_layout_label);
                switchLabel.setText(visibility.getName());
                Switch visibilitySwitch = (Switch) switchView.findViewById(R.id.unit_detail_switch);
                visibilitySwitch.setChecked(visibility.isActive());
                visibilitySwitch.setOnCheckedChangeListener(new SharedVisibilityOnCheckChangedListener(switchView));

                if (visibility.getType().equals("Showroom")) {
                    ++showRoomCount;
                    showRoomGroup.addView(switchView);
                } else if (visibility.getType().equals("SalesChannel")) {
                    salesChannelGroup.addView(switchView);
                }
            }
        }
        showRoomContainer.setVisibility(showRoomCount > 0 ? View.VISIBLE : View.GONE);
    }

    private void updateMetrics(){
        metricStrings = new ArrayList<String>();

        int selectedMetricIndex = 0;
        int usageValue = -1;

        if (unit.getMetrics() != null) {
            for (int i = 0; i < unit.getMetrics().size(); ++i) {
                metricStrings.add(unit.getMetrics().get(i).getName() + " (" + unit.getMetrics().get(i).getAbbreviation() + ")");

                if (unit.getMetrics().get(i).isSelected()) {
                    selectedMetricIndex = i;
                    usageValue = unit.getMetrics().get(i).getValue();
                }
            }
        }

        ArrayAdapter<String> usageUnitAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_row_item, metricStrings);
        usageUnitAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        usageUnitSpinner.setAdapter(usageUnitAdapter);
        usageUnitSpinner.setOnItemSelectedListener(new SharedItemSelectedListener(usageUnitLayout));
        usageUnitSpinner.setSelection(selectedMetricIndex, false);

        if (usageValue > -1){
            usageValueText.setText("" + usageValue);
        }

    }

    private void updatePriceFieldForLabel(){
        if (unit.getPriceLabel() == 5){
            priceText.setEnabled(true);
            priceText.setBackgroundColor(getResources().getColor(R.color.white_color));
            if (unit.getPrice() > 0){
                priceText.setText(String.format( "%.2f", unit.getPrice() ));
            }
        }
        else{
            priceText.setEnabled(false);
            priceText.setBackgroundColor(getResources().getColor(R.color.light_light_gray_color));
            if (unit.getPriceLabel() == 1 && unit.getDmsPrice() > 0){
                priceText.setText(String.format( "%.2f", (float)unit.getDmsPrice() ));
            }
            else if (unit.getPrice() > 0){
                priceText.setText(String.format( "%.2f", unit.getPrice() ));
            }
        }
    }

    public void updateDescriptionButtonText(String text){
        descriptionButton.setText(text);
    }

    @Override
    public void onBackPressed() {
        handleBack();
    }

    public void handleBack(){
        Intent returnIntent = new Intent();
        setResult(RESULT_OK,returnIntent);
        finish();
    }

    private void goToImage(){
        Intent intent = new Intent(this.getApplicationContext(), ImageManager.class);
        intent.putExtra(ImageManager.UNIT_ID, this.unit.getUnitId());
        startActivityForResult(intent, Stream.IMAGE_REQUEST);
    }

    private void goToAdd(){
        Intent intent = new Intent(getApplicationContext(), Add.class);
        intent.putExtra(Add.FINALIZE, true);
        intent.putExtra(Add.QUERY, (unit.getYear() + " " + unit.getMake() + " " + unit.getModel()));
        startActivityForResult(intent, Add.FINALIZE_REQUEST);
    }

    private void shareUnit(Unit unit){
        Intent share = new Intent(Intent.ACTION_SEND);
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        share.setType("text/plain");

        String messageText = unit.getYear() + " " + unit.getMake() + " " + unit.getModel() + "\n\n";
        String subject =  unit.getYear() + " " + unit.getMake() + " " + unit.getModel();
        if (unit.getPrice() > 0){
            messageText = messageText + "Price: $" + unit.getPrice() + "\n\n";
        }

        if (unit.getBestPrice() > 0){
            messageText = messageText + "Best Price: $" + unit.getBestPrice() + "\n\n";
        }

        if (unit.isSaleActive() && unit.getSalePrice() > 0){
            messageText = messageText + "Sale Price: $" + unit.getSalePrice() + "\n\n";
        }

        messageText = messageText + unit.getUrl().replace(" ", "%20").replace("®", "");

        share.putExtra(Intent.EXTRA_TEXT, messageText);
        share.putExtra(Intent.EXTRA_SUBJECT,subject);

        startActivity(Intent.createChooser(share, "Share Unit"));
    }

    private void displayUnlinkConfirmation(){
        new AlertDialog.Builder(this)
                .setTitle("Are you sure?")
                .setMessage("This action cannot be undone.")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        unlinkUnit();
                        isConfirmationDisplayed = false;
                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        oemActiveSwitch.setChecked(true);
                    }
                })
                .show();
        isConfirmationDisplayed = true;
    }

    private void displayDeleteConfirmation(){
        new AlertDialog.Builder(this)
                .setTitle("Are you sure?")
                .setMessage("This action cannot be undone.")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteUnit();
                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                })
                .show();
    }

    private void displayDeleteAlert(){
        AlertDialog ad = new AlertDialog.Builder(this)
                .setMessage("Unit Deleted")
                .setNegativeButton("Dismiss", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handleBack();
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                    }
                })
                .show();

        doKeepDialog(ad);
    }

    private void displayAlertWithMessage(String message){
        diag = new AlertDialog.Builder(this)
                .setMessage(message)
                .setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                    }
                })
                .show();
        doKeepDialog(diag);

    }

    private static void doKeepDialog(Dialog dialog){
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.getWindow().setAttributes(lp);
    }

    private void unlinkUnit(){
        unit.setLinkedProductId(0);
        oemActiveSwitch.setEnabled(false);
    }

    private void deleteUnit(){
        dialogMessage = "Deleting Unit";
        dialog.setMessage(dialogMessage);
        UnitService.getInstance().deleteUnit(getApplicationContext(), unit, false);
    }

    private void duplicateUnit(){
        UnitService.getInstance().duplicateUnit(getApplicationContext(), unit);
    }

    private void displayDatePicker(final LinearLayout containingLayout,Button button){

        String text = button.getText().toString();

        int year;
        int month;
        int day;
        final Calendar c;

        if(!StringUtil.isNullOrEmpty(text)){
            String[] cd = StringUtil.splitString(text, "-");
            c = Calendar.getInstance();
            year = Integer.parseInt(cd[2]);
            month = Integer.parseInt(cd[0]);
            day = Integer.parseInt(cd[1]);
        }else{
            c = Calendar.getInstance();
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
        }

        Calendar cal = Calendar.getInstance(TimeZone.getDefault());

        DatePickerDialog datePicker = new DatePickerDialog(this,
                new SharedOnDateSetListener(containingLayout,button),
                year,
                month-1,
                day);
        datePicker.setCancelable(false);
        datePicker.setTitle("Select the date");
        datePicker.show();
    }

    protected void initializePaging() {
        fragmentsList = new ArrayList<Fragment>();

        int size = unit.getImages().size();

        if (size > 0){
            for (int i=0; i<size; i++){
                Image image = unit.getImages().get(i);
                Fragment fragment= Fragment.instantiate(this, HeroImageFragment.class.getName());

                Bundle args = new Bundle();
                args.putString("image_location", image.getLocation());
                args.putString("image_path", image.getLocalPath());
                args.putInt(ImageSlider.CURRENT_INDEX,i);
                fragment.setArguments(args);
                fragmentsList.add(fragment);
            }

            pagerAdapter = new HeroImagePagerAdapter(getSupportFragmentManager(), fragmentsList);
            viewPager.setAdapter(pagerAdapter);
        }
    }

    public void openFullView(int index){
        if(unit==null || unit.getImages()==null || unit.getImages().size()==0) return;
        Intent intent = new Intent(this, ImageSlider.class);
        intent.putExtra(ImageSlider.CURRENT_INDEX,index);
        intent.putParcelableArrayListExtra(ImageSlider.IMAGES_LIST,(ArrayList<Image>) unit.getImages());
        startActivity(intent);
    }

    private class SharedTextWatcher implements TextWatcher{

        private View view;
        private SharedTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

        public void afterTextChanged(Editable editable) {
            String text = editable.toString();

            switch(((LinearLayout)view.getParent()).getId()){
                case R.id.unit_detail_layout_vin:
                    unit.setVin(text);
                    break;
                case R.id.unit_detail_layout_stock:
                    unit.setStockNumber(text);
                    break;
                case R.id.unit_detail_layout_price:
                    if (text.length() > 0 && unit.getPriceLabel() == 5 && ToolkitService.getInstance().isDouble(text)){
                        unit.setPrice(Double.parseDouble(text));
                    }
                    break;
                case R.id.unit_detail_layout_color:
                    unit.setPrimaryColor(text);
                    break;
                case R.id.unit_detail_layout_trim_color:
                    unit.setTrimColor(text);
                    break;
                case R.id.unit_detail_layout_usage_value:
                    if (text.length() > 0 && ToolkitService.getInstance().isInteger(text) && unit != null) {
                        UnitService.getInstance().updateSelectedMetricValueForUnitWithName(getApplicationContext(), unit, usageUnitSpinner.getSelectedItem().toString(), Integer.parseInt(text));
                    }
                    break;
                case R.id.unit_detail_layout_sale_price:
                    if (text.length() > 0 && ToolkitService.getInstance().isDouble(text)) {
                        unit.setSalePrice(Double.parseDouble(text));
                    }
                    break;
                case R.id.unit_detail_layout_best_price:
                    if (text.length() > 0 && ToolkitService.getInstance().isDouble(text)) {
                        unit.setBestPrice(Double.parseDouble(text));
                    }
                    break;
            }
        }
    }

    private class SharedOnEditorActionListener implements TextView.OnEditorActionListener{
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                handled = true;
            }
            return handled;
        }
    }

    private class FinalizeOnClickListener implements OnClickListener{

        @Override
        public void onClick(View view) {
            goToAdd();
        }
    }

    private class SharedItemSelectedListener implements OnItemSelectedListener{

        private LinearLayout containingLayout;

        private SharedItemSelectedListener(LinearLayout containingLayout) {
            this.containingLayout = containingLayout;
        }

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            switch (containingLayout.getId()) {
                case R.id.unit_detail_layout_location:
                    unit.setLocationId(locations.get(i).getLocationId());
                    break;
                case R.id.unit_detail_layout_price_label:
                    unit.setPriceLabel(UnitService.getInstance().getPriceLabelIntFromString(priceLabelStrings.get(i)));
                    updatePriceFieldForLabel();
                    break;
                case R.id.unit_detail_layout_title:
                    unit.setTitleStatus(UnitService.getInstance().getTitleIntFromString(titleStrings.get(i)));
                    break;
                case R.id.unit_detail_layout_condition:
                    unit.setCondition(UnitService.getInstance().getConditionIntFromString(conditionStrings.get(i)));
                    break;
                case R.id.unit_detail_layout_usage_unit:
                    unit.setMetrics(UnitService.getInstance().updateSelectedMetricForUnitWithName(getApplicationContext(), unit, metricStrings.get(i)));
                    break;
                case R.id.unit_detail_layout_sale_type:
                    unit.setSaleType(saleTypeStrings.get(i));
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    private class SharedOnCheckChangedListener implements CompoundButton.OnCheckedChangeListener{

        private LinearLayout containingLayout;

        private SharedOnCheckChangedListener(LinearLayout containingLayout) {
            this.containingLayout = containingLayout;
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            switch(containingLayout.getId()) {
                case R.id.unit_detail_layout_sale_active:
                    unit.setSaleActive(b);
                    if (b){
                        saleGroup.setVisibility(View.VISIBLE);
                    }
                    else{
                        saleGroup.setVisibility(View.GONE);
                    }
                    break;
                case R.id.unit_detail_layout_oem_details:
                    if (!isConfirmationDisplayed && !b) {
                        displayUnlinkConfirmation();
                    }
                    else{
                        isConfirmationDisplayed = false;
                    }
                    break;
                case R.id.unit_detail_layout_fifty_below:
                    unit.setPublishedOnWeb(b);
                    break;
                case R.id.unit_detail_layout_sell_as_new:
                    unit.setUsageStatus(b ? 2 : 1);
                    break;
            }
        }
    }

    private class SharedVisibilityOnCheckChangedListener implements CompoundButton.OnCheckedChangeListener{

        private DetailVisibilitySwitchView containingLayout;

        private SharedVisibilityOnCheckChangedListener(DetailVisibilitySwitchView containingLayout) {
            this.containingLayout = containingLayout;
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            Visibility visibility = containingLayout.getSwitchVisibility();
            if (visibility != null){
                visibility.setActive(b);
                unit.setVisibilities(VisibilityService.getInstance().updateVisibility(getApplicationContext(), visibility));
            }
        }
    }

    private class SharedOnClickListener implements OnClickListener{

        private LinearLayout containingLayout;

        private SharedOnClickListener(LinearLayout containingLayout) {
            this.containingLayout = containingLayout;
        }

        @Override
        public void onClick(View view) {
            displayDatePicker(containingLayout,(Button)view);
        }
    }

    private class SharedOnDateSetListener implements DatePickerDialog.OnDateSetListener{

        private LinearLayout containingLayout;
        private Button button;

        private SharedOnDateSetListener(LinearLayout containingLayout,Button button) {
            this.containingLayout = containingLayout;
            this.button = button;
        }

        @Override
        public void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

            int day = datePicker.getDayOfMonth();
            int month = datePicker.getMonth()+1;
            int year =  datePicker.getYear();

            try {
                Date date = sdf.parse(year + "-" + month + "-" + day);

                //Button button = (Button)containingLayout.findViewById(R.id.unit_detail_button);
                String d = day <=9 ? "0"+day+"" : day+"";
                String m = (""+month).length()==1?"0"+(month)+"":(month)+"";
                button.setText(m + "-" + d + "-" + year);

                switch(containingLayout.getId()) {
                    case R.id.unit_detail_layout_sale_start:
                        unit.setSaleStartDate(sdf2.format(date));

                        if (unit.getSaleEndDate() != null && !isSaleStartBeforeSaleEnd(unit.getSaleStartDate(), unit.getSaleEndDate())){
                            unit.setSaleEndDate(unit.getSaleStartDate());
                            saleEndButton.setText(ToolkitService.getInstance().formatDateStringForDisplay(unit.getSaleStartDate()));
                        }

                        break;
                    default:
                        if (isSaleStartBeforeSaleEnd(unit.getSaleStartDate(), sdf2.format(date))){
                            unit.setSaleEndDate(sdf2.format(date));
                        }
                        else {
                            unit.setSaleEndDate(unit.getSaleStartDate());
                            saleEndButton.setText(ToolkitService.getInstance().formatDateStringForDisplay(unit.getSaleStartDate()));
                        }

                        break;
                }
            }
            catch (ParseException e){
                e.printStackTrace();
            }
        }
    }

    private boolean isSaleStartBeforeSaleEnd(String startString, String endString){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

        if (startString != null) {
            try {
                Date startDate = sdf.parse(startString);
                Date endDate = sdf.parse(endString);

                return startDate.before(endDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String sender = bundle.getString("sender");
                int statusCode = bundle.getInt(UnitRequestService.CODE);

                if (sender.equals("unit_request_service")) {
                    int result = bundle.getInt(UnitRequestService.RESULT);
                    boolean put = bundle.getBoolean(UnitRequestService.PUT);
                    boolean delete = bundle.getBoolean(UnitRequestService.DELETE);
                    boolean add = bundle.getBoolean(UnitRequestService.ADD);
                    boolean duplicate = bundle.getBoolean(UnitRequestService.DUPLICATE);

                    if (put){
                        dialog.dismiss();
                        if (result == Activity.RESULT_OK){
                            displayAlertWithMessage("Save Complete.");
                        }
                        else{
                            displayAlertWithMessage("Save Failed.");
                        }
                    }
                    else if (delete){
                        dialog.dismiss();
                        displayDeleteAlert();
                    }
                    else if (add){
                        dialog.dismiss();

                        if (result == Activity.RESULT_OK){

                            displayAlertWithMessage("Unit added.");

                            unitId = bundle.getInt(UnitRequestService.UNIT_ID);
                            if (unitId > 0){
                                localUnitId = 0;
                                state = State.SAVED;
                                unit = UnitService.getInstance().getUnitWithUnitId(getApplicationContext(), unitId);

                                //Next two lines are a workaround because object returned by add unit does not include an industryId
                                unit.setIndustryId(industryId);
                                DatabaseService.getInstance(context.getApplicationContext()).updateUnit(unit);

                                initializePaging();
                                updateView(true);
                                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                            }
                        }
                        else{
                            displayAlertWithMessage("Unable to add unit.");
                        }
                    }
                    else if (duplicate){
                        dialog.dismiss();
                        int resultCode = bundle.getInt(UnitRequestService.RESULT);
                        if (resultCode == Activity.RESULT_OK){
                            displayAlertWithMessage("Unit duplicated.");
                            long localUnitId = bundle.getLong(UnitRequestService.LOCAL_UNIT_ID);
                            unit = UnitService.getInstance().getUnitWithLocalUnitId(getApplicationContext(), localUnitId);
                            populateView();
                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                        }
                        else{
                            displayAlertWithMessage("Unable to duplicate unit.");
                        }
                    }
                }

                if (statusCode == UnitRequestService.TIMEOUT){
                    dialog.dismiss();
                    displayToast();
                }
            }
        }


    };

    public void displayToast(){
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        ToolkitService.getInstance().makeToast(this, getResources().getString(R.string.toast_network_timeout));
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    private class HeroImagePagerAdapter extends FragmentStatePagerAdapter {
        private List<Fragment> fragments = null;

        public HeroImagePagerAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return this.fragments.get(position);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void saveData(){
        if(!isValidDate()) return;
        setRequestedOrientation(getResources().getConfiguration().orientation);
        dialog.show();
        UnitService.getInstance().updateUnit(getApplicationContext(), unit, false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Add.FINALIZE_REQUEST) {
            if (resultCode == RESULT_OK) {
                if (data.getExtras() != null) {
                    long localId = data.getExtras().getLong(LOCAL_UNIT_ID);
                    Unit selectedUnit = UnitService.getInstance().getUnitWithLocalUnitId(getApplicationContext(), localId);
                    if (selectedUnit != null) {
                        unit = UnitService.getInstance().updateUnitWithUnitDataForFinalization(getApplicationContext(), unit, selectedUnit);
                        oemActiveSwitch.setChecked(selectedUnit.getLinkedProductId() > 0);
                        this.state = State.UNSAVED;
                        populateView();
                    }
                }
            }
        } else if (requestCode == Stream.IMAGE_REQUEST) {
            unit.setImages(ImageService.getInstance().getImagesForUnit(getApplicationContext(), unit));
            populateView();
        } else if (requestCode == ADDITIONAL_DETAIL_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                saveData();
            } else {

            }
        }
    }

    private boolean isValidDate(){

        if(saleGroup.getVisibility()!=View.VISIBLE) return true;

        String endDate = saleEndButton.getText().toString();
        String startDate = saleStartButton.getText().toString();
        if(StringUtil.isNullOrEmpty(startDate)){
            displayAlertWithMessage("Please select start date");
            return false;
        }
        if(StringUtil.isNullOrEmpty(endDate)){
            displayAlertWithMessage("Please select end date");
            return false;
        }
        if(convertDateIntoMills(endDate)>=convertDateIntoMills(startDate)){
            return true;
        }else{
            displayAlertWithMessage("Sale End Date should always be greater than Sale Start Date.");
            return false;
        }
    }

    private long convertDateIntoMills(String date){
        String[] d =  date.split("-");
        int[] a = new int[3];
        a[0] = Integer.parseInt(d[0]);
        a[1] = Integer.parseInt(d[1]);
        a[2] = Integer.parseInt(d[2]);

        Calendar calendar = Calendar.getInstance();
        calendar.set(a[2],a[1]-1,a[0]);
        return calendar.getTimeInMillis();
    }

}
