package arinet.com.mim.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import arinet.com.mim.R;
import arinet.com.mim.service.toolkit.ToolkitService;
import arinet.com.mim.service.unit.UnitRequestService;
import arinet.com.mim.service.yearMakeModel.YearMakeModelService;

public class Custom extends Activity {

    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setLogo(this.getResources().getDrawable(R.drawable.action_bar_logo));

        dialog = ToolkitService.getInstance().makeIndeterminateDialog(this, "Updating Equipment", "This may take a moment.");
        dialog.show();

        Intent ui = new Intent(this, YearMakeModelService.class);
        startService(ui);
    }

    @Override
    protected void onResume(){
        super.onResume();
        registerReceiver(receiver, new IntentFilter(UnitRequestService.NOTIFICATION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);

        if (dialog != null){
            dialog.dismiss();
            dialog = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.custom, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();

            if (bundle != null) {
                String sender = bundle.getString("sender");
                if (sender.equals("year_make_model_request_service")) {
                    dialog.dismiss();
                }
            }
        }
    };

}
