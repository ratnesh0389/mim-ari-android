package arinet.com.mim.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.google.analytics.tracking.android.EasyTracker;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import arinet.com.mim.R;
import arinet.com.mim.adapter.ImageGridAdapter;
import arinet.com.mim.model.Image;
import arinet.com.mim.model.Unit;
import arinet.com.mim.permission.Permission;
import arinet.com.mim.permission.PermissionListener;
import arinet.com.mim.provider.ARIProvider;
import arinet.com.mim.service.image.ImageRequestService;
import arinet.com.mim.service.image.ImageService;
import arinet.com.mim.service.site.SiteRequestService;
import arinet.com.mim.service.toolkit.ToolkitService;
import arinet.com.mim.service.unit.UnitService;
import arinet.com.mim.vendor.dynamicgrid.DynamicGridView;
import arinet.com.mim.view.ImageGridRelativeLayout;

public class ImageManager extends Activity {

    public static final String UNIT_ID = "unit_id";
    public static final String LOCAL_UNIT_ID = "local_unit_id";
    public static final String OUTPUT_URI = "output_uri";
    public static final String CAMERA_IMAGE_PATH = "cameraImageFilePath";
    public static final String PREFS = "prefs";
    public static final String TIP_DISPLAYED = "tip_displayed";
    public static final String TOTAL_IMAGES = "total_images";
    public static final String CURRENT_IMAGE_COUNT = "current_image_count";

    private static final int SELECT_LIBRARY = 1;
    private static final int SELECT_CAMERA= 2;

    private int unitId;
    private int imageCount;
    private int totalCount;
    private boolean isSelecting;
    private boolean tipDisplayed;
    private ArrayList<Image>images;
    private Uri outputFileUri;

    private SharedPreferences settings;

    private Unit unit;
    private DynamicGridView grid;
    private ImageGridAdapter adapter;
    private ProgressDialog dialog;
    private Menu menu;

    private File root;

    static final String DIALOG = "dialog";
    private String mCameraImageFilePath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        if (savedInstanceState != null) {
            outputFileUri= savedInstanceState.getParcelable(OUTPUT_URI);
            mCameraImageFilePath = savedInstanceState.getString(CAMERA_IMAGE_PATH);
        }

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setLogo(this.getResources().getDrawable(R.drawable.action_bar_logo));

        root = new File(Environment.getExternalStorageDirectory() + File.separator + "MIM" + File.separator);
        root.mkdirs();

        settings = getSharedPreferences(PREFS, 0);
        tipDisplayed = settings.getBoolean(TIP_DISPLAYED, false);

        isSelecting = false;

        dialog = ToolkitService.getInstance().makeDeterminateDialog(this, "Image Upload", "Uploading Image");

        unitId = getIntent().getExtras().getInt(UNIT_ID);
        unit = UnitService.getInstance().getUnitWithUnitId(this.getApplicationContext(), unitId);

        images = unit.getImages();

        grid = (DynamicGridView)findViewById(R.id.image_grid);
        grid.setMultiChoiceModeListener(new MultiChoiceModeListener());



        adapter = new ImageGridAdapter(this, images, 4);
        grid.setAdapter(adapter);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ImageManager.this, ImageSlider.class);
                intent.putExtra(ImageSlider.CURRENT_INDEX,position);
                intent.putParcelableArrayListExtra(ImageSlider.IMAGES_LIST,(ArrayList<Image>) images);
                ImageManager.this.startActivity(intent);
            }
        });

        grid.setOnDropListener(new DynamicGridView.OnDropListener(){
            @Override
            public void onActionDrop(){
                refreshImageSequence();
            }
        });

        if (savedInstanceState != null) {
            if (savedInstanceState.getBoolean(DIALOG)){
                dialog.show();
            }
            else{
                dialog.dismiss();
            }
            totalCount = savedInstanceState.getInt(TOTAL_IMAGES);
            imageCount = savedInstanceState.getInt(CURRENT_IMAGE_COUNT);

            if (totalCount > 0){
                dialog.setMessage("Uploading " + totalCount + " images.");
                dialog.setIndeterminate(true);
            }
        }
    }

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}

    @Override
    protected void onResume(){
        super.onResume();
        registerReceiver(receiver, new IntentFilter(ImageRequestService.NOTIFICATION));
    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        unregisterReceiver(receiver);

        if (dialog != null){
            dialog.dismiss();
            dialog = null;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putParcelable(OUTPUT_URI, outputFileUri);
        savedInstanceState.putInt(TOTAL_IMAGES, totalCount);
        savedInstanceState.putInt(CURRENT_IMAGE_COUNT, imageCount);
        savedInstanceState.putString(CAMERA_IMAGE_PATH,mCameraImageFilePath);
        savedInstanceState.putBoolean(DIALOG, dialog.isShowing());
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent returnIntent = new Intent();
                setResult(RESULT_OK,returnIntent);
                finish();
                return true;
            case R.id.action_image_camera:
                /*if (!tipDisplayed){
                    new AlertDialog.Builder(this)
                            .setTitle("Pro Tip")
                            .setMessage("Images in landscape work best.")
                            .setPositiveButton("Okay", new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    tipDisplayed = true;
                                    SharedPreferences settings = getSharedPreferences(PREFS, 0);
                                    SharedPreferences.Editor editor = settings.edit();
                                    editor.putBoolean(TIP_DISPLAYED, true);
                                    editor.commit();
                                    openCameraPermission();
                                }

                            })
                            .show();
                }
                else{

                }*/
                openCameraPermission();
                return true;
            case R.id.action_image_library:
                storageFileAccessPermission();
                return true;
            case R.id.action_select:
                editGrid();
                updateGridForState();
                return true;
            case R.id.action_cancel:
                endEditGrid();
                updateGridForState();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        handleBack();
    }

    public void handleBack(){
        Intent returnIntent = new Intent();
        setResult(RESULT_OK,returnIntent);
        finish();
    }

    private void refreshImageSequence(){
        images.clear();
        for (int i = 0; i < grid.getChildCount(); ++i){
            Image image = ((ImageGridRelativeLayout)grid.getChildAt(i)).getImage();
            image.setSequence(i);
            image.setPrimary((i == 0));
            images.add(image);
            ImageService.getInstance().putImage(getApplicationContext(), image, false);
        }
    }

    private void confirmDelete(final ArrayList<Image>imagesToDelete, final ActionMode mode){
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle("Are You Sure?");
        b.setMessage("This action cannot be undone.");
        b.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface d, int arg1) {
                for (Image image : imagesToDelete){
                    adapter.remove(image);
                    ImageService.getInstance().deleteImage(getApplicationContext(), image);
                }
                adapter.notifyDataSetChanged();
                mode.finish();
                d.cancel();
            };
        });
        b.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int arg1) {
                d.cancel();
            }

            ;
        });
        b.show();
    }

    private void editGrid(){
        grid.startEditMode();
    }

    private void endEditGrid(){
        grid.stopEditMode();
    }

    private void updateGridForState(){
        isSelecting = isSelecting ? false : true;
        toggleVisibility(this.menu.findItem(R.id.action_select));
        toggleVisibility(this.menu.findItem(R.id.action_image_camera));
        toggleVisibility(this.menu.findItem(R.id.action_image_library));
        toggleVisibility(this.menu.findItem(R.id.action_cancel));
    }

    private void toggleVisibility(MenuItem item){
        item.setVisible(item.isVisible() ? false : true);
    }

    private void openCameraIntent(){

        String filename = ToolkitService.getInstance().makeImageFileName(this.unitId+"", false);
        File sdImageMainDirectory = new File(root, filename);
        mCameraImageFilePath = sdImageMainDirectory.getAbsolutePath();
        outputFileUri = getFileUri(sdImageMainDirectory);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
            //intent.setClipData(ClipData.newRawUri("", fileUri));
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }
        startActivityForResult(intent, SELECT_CAMERA);
    }

    private void openCameraPermission() {
        PermissionListener listener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                openCameraIntent();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }
        };

        Permission.with(this)
                .setPermissionListener(listener)
                .setRationaleTitle("Permission denied")
                .setRationaleMessage("Camera & Storage permission are required")
                .setPermissions(new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE})
                .check();
    }

    private void storageFileAccessPermission() {
        PermissionListener listener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                openLibraryIntent();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }
        };

        Permission.with(this)
                .setPermissionListener(listener)
                .setRationaleTitle("Permission denied")
                .setRationaleMessage("Storage permission is required")
                .setPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE})
                .check();
    }

    private void openLibraryIntent(){
        Intent intent;

        if (Build.VERSION.SDK_INT < 19){
            outputFileUri = generateFileUri(this.unitId+"");
            intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/jpeg");
        } else {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setType("image/jpeg");
        }

        startActivityForResult(intent, SELECT_LIBRARY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode == RESULT_OK){
            if (requestCode == SELECT_LIBRARY) {
                if (Build.VERSION.SDK_INT < 19) {

                    outputFileUri = generateFileUri(this.unitId+"");

                    Uri selectedImageUri = data.getData();
                    if (selectedImageUri != null){
                        if ("content".equals(selectedImageUri.getScheme())) {
                            try {
                                createImageFileWithStreamAndPath(getContentResolver().openInputStream(selectedImageUri), outputFileUri.getPath());
                                dialog.show();
                            }
                            catch (IOException e){
                                e.printStackTrace();
                            }
                        }
                        else {
                            createImageFileWithStreamAndPath(null, outputFileUri.getPath());
                            dialog.show();
                        }
                    }
                    else{
                        cameraError(2);
                    }
                }
                else{
                    System.out.println(data);
                    handleMultipleImageSelections(data);
                }
            }
            else if (requestCode == SELECT_CAMERA){
                imageCount = 1;
                dialog.setMessage("Uploading image.");
                dialog.setIndeterminate(true);

                if (outputFileUri != null) {
                    createImageFileWithStreamAndPath(null, mCameraImageFilePath);
                    dialog.show();
                }
                else{
                    cameraError(1);
                    dialog.dismiss();
                }
            }
        }

    }

    @TargetApi(19)
    private void handleMultipleImageSelections(Intent intent){

        ArrayList<Uri>uris = new ArrayList<Uri>();
        imageCount = 0;
        ClipData cp = intent.getClipData();

        if (cp != null){
            for (int i = 0; i < cp.getItemCount(); ++i){

                ++imageCount;

                Uri finalFileUri = generateFileUri(this.unitId+i+"");

                ClipData.Item item = cp.getItemAt(i);
                Uri dataUri = item.getUri();

                if ("content".equals(dataUri.getScheme())) {
                    try {
                        createImageFileWithStreamAndPath(getContentResolver().openInputStream(dataUri), finalFileUri.getPath());
                    }
                    catch (IOException e){
                        e.printStackTrace();
                    }
                }
                else {
                    createImageFileWithStreamAndPath(null, finalFileUri.getPath());
                }

                uris.add(finalFileUri);
            }
        }
        else{
            ++imageCount;

            Uri finalFileUri = generateFileUri(this.unitId+"");
            Uri dataUri = intent.getData();
            uris.add(finalFileUri);

            if (dataUri != null){
                if ("content".equals(dataUri.getScheme())) {
                    try {
                        createImageFileWithStreamAndPath(getContentResolver().openInputStream(dataUri), finalFileUri.getPath());
                    }
                    catch (IOException e){
                        e.printStackTrace();
                    }
                }
                else {
                    createImageFileWithStreamAndPath(null, finalFileUri.getPath());
                }
            }
        }

        totalCount = imageCount;

        if (imageCount > 1){
            dialog.setMessage("Uploading " + imageCount + " images.");
        }
        else{
            dialog.setMessage("Uploading image.");
        }

        dialog.setProgress(0);
        dialog.setIndeterminate(true);
        dialog.show();
    }

    private Uri generateFileUri(String identifier){
        String filename = ToolkitService.getInstance().makeImageFileName(identifier, false);
        File sdImageMainDirectory = new File(root, filename);
        return Uri.fromFile(sdImageMainDirectory);
    }

    private void cameraError(int errorCode){
        ToolkitService.getInstance().makeToast(getApplicationContext(), "Unable to get picture. Error: " + errorCode);
    }

    private void fileError(String description){
        ToolkitService.getInstance().makeToast(getApplicationContext(), "Error: " + description);
    }

    public void createImageFileWithStreamAndPath(InputStream input, String path){
        BitmapWorkerTask task = new BitmapWorkerTask(input);
        task.execute(path);
    }

    class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
        InputStream input = null;
        String path = null;

        public BitmapWorkerTask(InputStream input) {
            this.input = input;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            try {
                path = params[0];
                File file = new File(path);
                if (!file.exists()) {
                    file.createNewFile();
                }

                if (input == null){
                    input = new BufferedInputStream(new FileInputStream(file));
                }

                bitmap = ToolkitService.getInstance().decodeSampledBitmap(input, path);

                FileOutputStream fileOutput = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fileOutput);
                fileOutput.flush();
                fileOutput.close();
            }
            catch (FileNotFoundException e){
                e.printStackTrace();
                fileError(e.getLocalizedMessage());
            }
            catch (IOException e){
                e.printStackTrace();
                fileError(e.getLocalizedMessage());
                fileError(e.getMessage());
            }
            finally {
                if (input != null) {
                    try{
                        input.close();
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return bitmap;
            }
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            Image image = new Image();
            image.setLocalPath(path);
            image.setUnitId(unitId);
            image.setUnitLocalId(unit.getLocalId());
            image.setSequence(images.size());
            image.setNew(true);
            ImageService.getInstance().postImage(getApplicationContext(), image, false);
        }
    }

    public static int getSelectLibrary() {
        return SELECT_LIBRARY;
    }

    private void reloadGrid(){
        images.clear();
        images.addAll(ImageService.getInstance().getImagesForUnit(this.getApplicationContext(), unit));

        adapter = new ImageGridAdapter(this, images, 4);
        grid.setAdapter(adapter);
    }

    private void displayAlertWithMessage(String message){
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle(message);
        b.setNegativeButton("Dismiss", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface d, int arg1) {
                d.cancel();
            };
        });
        b.show();
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            int resultCode = bundle.getInt("result");
            int statusCode = bundle.getInt(ImageRequestService.CODE);
            boolean isPost = bundle.getBoolean(ImageRequestService.POST, false);
            boolean isPut = bundle.getBoolean(ImageRequestService.PUT, false);
            boolean isDelete = bundle.getBoolean(ImageRequestService.DELETE, false);
            boolean offlineRequest = bundle.getBoolean(ImageRequestService.OFFLINE_REQUEST, false);

            if (isPost){

                if (!offlineRequest) {
                    if (resultCode == Activity.RESULT_OK) {
                        --imageCount;
                        if (imageCount > 0){
                            dialog.setIndeterminate(false);

                            int remainder = totalCount-imageCount;
                            int progress = (int)((((double)remainder)/((double)totalCount))*100);

                            dialog.setProgress(progress);
                        }
                        else {
                            dialog.dismiss();
                            displayAlertWithMessage("Upload complete");
                            reloadGrid();
                        }
                    } else {
                        dialog.dismiss();
                        displayAlertWithMessage("Image saved");
                        reloadGrid();
                        if (statusCode != ImageRequestService.NO_NETWORK_CONNECTION) {
                            displayToast(statusCode);
                        }
                    }
                }
            }
            else if (isDelete){
                if (resultCode == Activity.RESULT_OK){
                    displayToastWithMessage("Image deleted.");
                } else if (resultCode != Activity.RESULT_OK) {
                    displayToast(statusCode);
                    reloadGrid();
                }
            }
            else if (isPut){
                if (resultCode == Activity.RESULT_CANCELED){
                    displayToast(statusCode);
                }
            }

            if (statusCode == SiteRequestService.TIMEOUT){
                displayToast(statusCode);
            }
        }
        }
    };

    int deleteCount;

    public class MultiChoiceModeListener implements GridView.MultiChoiceModeListener {

        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.setTitle("Select Items");
            mode.setSubtitle("1 item selected");
            mode.getMenuInflater().inflate(R.menu.image_action_mode, menu);
            return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return true;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.delete:
                    ArrayList<Image> imagesToDelete = new ArrayList<Image>();
                    SparseBooleanArray selected = grid.getCheckedItemPositions();
                    for (int i = (selected.size() - 1); i >= 0; i--) {
                        if (selected.valueAt(i)) {
                            Image image = (Image)adapter.getItem(selected.keyAt(i));
                            imagesToDelete.add(image);
                        }
                    }

                    deleteCount = imagesToDelete.size();

                    confirmDelete(imagesToDelete, mode);
                    return true;
                default:
                    return false;
            }
        }

        public void onDestroyActionMode(ActionMode mode) {
            System.out.println("onDestroyActionMode");
        }

        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            int selectCount = grid.getCheckedItemCount();

            switch (selectCount) {
                case 1:
                    mode.setSubtitle("1 item selected");
                    break;
                default:
                    mode.setSubtitle("" + selectCount + " items selected");
                    break;
            }
        }

    }


    public void displayToast(int statusCode){
        String message = this.getResources().getString(R.string.toast_exception);
        if (statusCode == ImageRequestService.CONNECTION_CLOSED) {
            message = this.getResources().getString(R.string.toast_connection_closed);
        } else if (statusCode == SiteRequestService.TIMEOUT) {
            message = this.getResources().getString(R.string.toast_network_timeout);
        } else if (statusCode == ImageRequestService.NO_NETWORK_CONNECTION) {
            message = this.getResources().getString(R.string.toast_no_network_connection);
        }
        displayToastWithMessage(message);
    }

    public void displayToastWithMessage(String message) {
        ToolkitService.getInstance().makeToast(this, message);
    }

    private Uri getFileUri(File path){
        return ARIProvider.getUriForFile(this,
                getString(R.string.file_provider_auth),
                path);
    }
}
