package arinet.com.mim.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Spinner;

import arinet.com.mim.model.Attribute;
import arinet.com.mim.model.UnitAttribute;

/**
 * Created by kevinstueber on 10/30/14.
 */
public class AddDetailSpinner extends Spinner {

    UnitAttribute unitAttribute;
    Attribute attribute;

    public AddDetailSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UnitAttribute getUnitAttribute() {
        return unitAttribute;
    }

    public void setUnitAttribute(UnitAttribute unitAttribute) {
        this.unitAttribute = unitAttribute;
    }

    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }
}
