package arinet.com.mim.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import arinet.com.mim.model.Filter;

/**
 * Created by kevinstueber on 7/29/14.
 */
public class FilterDrawerButtonView extends RelativeLayout {

    private Filter filter;

    public FilterDrawerButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Filter getFilter() {
        return filter;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
    }
}
