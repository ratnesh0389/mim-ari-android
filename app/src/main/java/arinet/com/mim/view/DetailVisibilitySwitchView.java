package arinet.com.mim.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import arinet.com.mim.model.Visibility;

/**
 * Created by kevinstueber on 8/6/14.
 */
public class DetailVisibilitySwitchView extends LinearLayout {

    private Visibility switchVisibility;

    public DetailVisibilitySwitchView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Visibility getSwitchVisibility() {
        return switchVisibility;
    }

    public void setSwitchVisibility(Visibility switchVisibility) {
        this.switchVisibility = switchVisibility;
    }
}
