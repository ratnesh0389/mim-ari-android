package arinet.com.mim.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import arinet.com.mim.model.Unit;

/**
 * Created by kevinstueber on 8/21/14.
 */
public class DetailEditText extends EditText {

    Unit unit;

    public DetailEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }
}
