package arinet.com.mim.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import arinet.com.mim.model.Visibility;

/**
 * Created by kevinstueber on 9/30/14.
 */
public class FragmentDialogVisibilityItem extends LinearLayout {

	private Visibility itemVisibility;

	public FragmentDialogVisibilityItem(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public Visibility getItemVisibility() {
		return itemVisibility;
	}

	public void setItemVisibility(Visibility itemVisibility) {
		this.itemVisibility = itemVisibility;
	}
}
