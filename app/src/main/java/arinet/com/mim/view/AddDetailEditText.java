package arinet.com.mim.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import arinet.com.mim.model.Attribute;
import arinet.com.mim.model.Unit;
import arinet.com.mim.model.UnitAttribute;

/**
 * Created by kevinstueber on 8/21/14.
 */
public class AddDetailEditText extends EditText {

    UnitAttribute unitAttribute;
    Attribute attribute;

    public AddDetailEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public UnitAttribute getUnitAttribute() {
        return unitAttribute;
    }

    public void setUnitAttribute(UnitAttribute unitAttribute) {
        this.unitAttribute = unitAttribute;
    }

    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }
}
