package arinet.com.mim.permission;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.StringRes;

/**
 * Created by Dev Android  on 1/8/2018.
 */

public abstract class PermissionBuilder <T extends PermissionBuilder>{

    private PermissionListener listener;
    private String[] permissions;
    private CharSequence explainPermissionTitle;
    private CharSequence explainPermissionMessage;
    private CharSequence rationaleTitle;
    private CharSequence rationaleMessage;

    private Context context;

    public PermissionBuilder(Context context) {
        this.context = context;
    }


    protected void checkPermissions() {
        if (ObjectUtils.isEmpty(permissions)) {
            throw new IllegalArgumentException("You must setPermissions() on Permission");
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            listener.onPermissionGranted();
            return;
        }

        Intent intent = new Intent(context, PermissionActivity.class);
        intent.putExtra(PermissionActivity.EXTRA_PERMISSIONS, permissions);

        intent.putExtra(PermissionActivity.EXTRA_RATIONAL_TITLE, rationaleTitle);
        intent.putExtra(PermissionActivity.EXTRA_RATIONAL_MESSAGE, rationaleMessage);
        intent.putExtra(PermissionActivity.EXTRA_EXPLAIN_PERMISSION_TITLE, explainPermissionTitle);
        intent.putExtra(PermissionActivity.EXTRA_EXPLAIN_PERMISSION_MESSAGE, explainPermissionMessage);
        intent.putExtra(PermissionActivity.EXTRA_PACKAGE_NAME, context.getPackageName());

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_USER_ACTION);
        PermissionActivity.startActivity(context, intent, listener);
        BasePermission.setFirstRequest(context,permissions);
    }


    public T setPermissionListener(PermissionListener listener) {
        this.listener = listener;
        return (T) this;
    }

    public T setPermissions(String... permissions) {
        this.permissions = permissions;
        return (T) this;
    }

    public T setRationaleMessage(@StringRes int stringRes) {
        return setRationaleMessage(getText(stringRes));
    }

    @SuppressLint("ResourceType")
    private CharSequence getText(@StringRes int stringRes) {
        if (stringRes <= 0) {
            throw new IllegalArgumentException("Invalid String resource id");
        }
        return context.getText(stringRes);
    }

    public T setRationaleMessage(CharSequence rationaleMessage) {
        this.rationaleMessage = rationaleMessage + "\n\nPlease turn on permissions at [Setting] > [Permission]";
        return (T) this;
    }


    public T setRationaleTitle(@StringRes int stringRes) {
        return setRationaleTitle(getText(stringRes));
    }

    public T setRationaleTitle(CharSequence rationaleMessage) {
        this.rationaleTitle = rationaleMessage;
        return (T) this;
    }

    public T setExplainPermissionMessage(@StringRes int stringRes) {
        return setExplainPermissionMessage(getText(stringRes));
    }

    public T setExplainPermissionMessage(CharSequence explainPermissionMessage) {
        this.explainPermissionMessage = explainPermissionMessage;
        return (T) this;
    }

    public T setExplainPermissionTitle(@StringRes int stringRes) {
        return setExplainPermissionTitle(getText(stringRes));
    }

    public T setExplainPermissionTitle(CharSequence explainPermissionTitle) {
        this.explainPermissionTitle = explainPermissionTitle;
        return (T) this;
    }

}
