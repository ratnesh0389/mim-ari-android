package arinet.com.mim.permission;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;


import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import arinet.com.mim.R;

/**
 * Created by Dev Android  on 1/8/2018.
 */

public class PermissionActivity extends Activity {

    public static final int REQ_CODE_PERMISSION_REQUEST = 10;

    public static final int REQ_CODE_SYSTEM_ALERT_WINDOW_PERMISSION_REQUEST = 30;
    public static final int REQ_CODE_SYSTEM_ALERT_WINDOW_PERMISSION_REQUEST_SETTING = 31;


    public static final String EXTRA_PERMISSIONS = "permissions";
    public static final String EXTRA_EXPLAIN_PERMISSION_TITLE = "explainPermissionTitle";
    public static final String EXTRA_EXPLAIN_PERMISSION_MESSAGE = "explainPermissionMessage";
    public static final String EXTRA_RATIONAL_TITLE = "rational_title";
    public static final String EXTRA_RATIONAL_MESSAGE = "rational_message";
    public static final String EXTRA_PACKAGE_NAME = "package_name";
    private static Deque<PermissionListener> permissionListenerStack;
    CharSequence explainPermissionTitle;
    CharSequence explainPermissionMessage;
    CharSequence rationaleTitle;
    CharSequence rationaleMessage;
    String[] permissions;
    String packageName;
    String settingButtonText;
    String deniedCloseButtonText = "Later";
    String rationaleConfirmText = "OK";
    boolean isShownExplainDialog;

    boolean hasSettingButton = true;
    private String SETTING_BTN_TEXT = "Setting";

    public static void startActivity(Context context, Intent intent, PermissionListener listener) {
        if (permissionListenerStack == null) {
            permissionListenerStack = new ArrayDeque<>();
        }
        permissionListenerStack.push(listener);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(0, 0);
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        setupFromSavedInstanceState(savedInstanceState);
        // check windows
        if (needWindowPermission()) {
            requestWindowPermission();
        } else {
            checkPermissions(false);
        }
    }

    private void setupFromSavedInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            permissions = savedInstanceState.getStringArray(EXTRA_PERMISSIONS);
            explainPermissionTitle = savedInstanceState.getCharSequence(EXTRA_EXPLAIN_PERMISSION_TITLE);
            explainPermissionMessage = savedInstanceState.getCharSequence(EXTRA_EXPLAIN_PERMISSION_MESSAGE);
            rationaleTitle = savedInstanceState.getCharSequence(EXTRA_RATIONAL_TITLE);
            rationaleMessage = savedInstanceState.getCharSequence(EXTRA_RATIONAL_MESSAGE);
            packageName = savedInstanceState.getString(EXTRA_PACKAGE_NAME);
        } else {
            Intent intent = getIntent();
            permissions = intent.getStringArrayExtra(EXTRA_PERMISSIONS);
            explainPermissionTitle = intent.getCharSequenceExtra(EXTRA_EXPLAIN_PERMISSION_TITLE);
            explainPermissionMessage = intent.getCharSequenceExtra(EXTRA_EXPLAIN_PERMISSION_MESSAGE);
            rationaleTitle = intent.getCharSequenceExtra(EXTRA_RATIONAL_TITLE);
            rationaleMessage = intent.getCharSequenceExtra(EXTRA_RATIONAL_MESSAGE);
            packageName = intent.getStringExtra(EXTRA_PACKAGE_NAME);
        }
    }

    private boolean needWindowPermission() {
        for (String permission : permissions) {
            if (permission.equals(Manifest.permission.SYSTEM_ALERT_WINDOW)) {
                return !hasWindowPermission();
            }
        }
        return false;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean hasWindowPermission() {
        return Settings.canDrawOverlays(getApplicationContext());
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestWindowPermission() {
        Uri uri = Uri.fromParts("package", packageName, null);
        final Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, uri);

        if (!TextUtils.isEmpty(explainPermissionMessage)) {
            new AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog_Alert)
                    .setMessage(explainPermissionMessage)
                    .setCancelable(false)

                    .setNegativeButton(rationaleConfirmText, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivityForResult(intent, REQ_CODE_SYSTEM_ALERT_WINDOW_PERMISSION_REQUEST);
                        }
                    })
                    .show();
            isShownExplainDialog = true;
        } else {
            startActivityForResult(intent, REQ_CODE_SYSTEM_ALERT_WINDOW_PERMISSION_REQUEST);
        }
    }

    private void checkPermissions(boolean fromOnActivityResult) {

        ArrayList<String> needPermissions = new ArrayList<>();

        for (String permission : permissions) {
            if (permission.equals(Manifest.permission.SYSTEM_ALERT_WINDOW)) {
                if (!hasWindowPermission()) {
                    needPermissions.add(permission);
                }
            } else {
                if (BasePermission.isDenied(this, permission)) {
                    needPermissions.add(permission);
                }
            }
        }

        if (needPermissions.isEmpty()) {
            permissionResult(null);
        } else if (fromOnActivityResult) { //From Setting Activity
            permissionResult(needPermissions);
        } else if (needPermissions.size() == 1 && needPermissions
                .contains(Manifest.permission.SYSTEM_ALERT_WINDOW)) {   // window permission deny
            permissionResult(needPermissions);
        } else if (!isShownExplainDialog
                && !TextUtils.isEmpty(explainPermissionMessage)) { // //Need Show Rationale
            showPermissionExplainDialog(needPermissions);
        } else { // //Need Request Permissions
            requestPermissions(needPermissions);
        }
    }

    private void permissionResult(ArrayList<String> deniedPermissions) {
        Log.v(Permission.TAG, "permissionResult(): " + deniedPermissions);
        if (permissionListenerStack != null) {
            PermissionListener listener = permissionListenerStack.pop();

            if (ObjectUtils.isEmpty(deniedPermissions)) {
                listener.onPermissionGranted();
            } else {
                listener.onPermissionDenied(deniedPermissions);
            }
            if (permissionListenerStack.size() == 0) {
                permissionListenerStack = null;
            }
        }

        finish();
        overridePendingTransition(0, 0);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }

    private void showPermissionExplainDialog(final ArrayList<String> needPermissions) {

        new AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog_Alert)
                .setTitle(explainPermissionTitle)
                .setMessage(explainPermissionMessage)
                .setCancelable(false)

                .setNegativeButton(rationaleConfirmText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        requestPermissions(needPermissions);

                    }
                })
                .show();
        isShownExplainDialog = true;
    }

    public void requestPermissions(ArrayList<String> needPermissions) {
        ActivityCompat.requestPermissions(this, needPermissions.toArray(new String[needPermissions.size()]),
                REQ_CODE_PERMISSION_REQUEST);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putStringArray(EXTRA_PERMISSIONS, permissions);
        outState.putCharSequence(EXTRA_EXPLAIN_PERMISSION_TITLE, explainPermissionTitle);
        outState.putCharSequence(EXTRA_EXPLAIN_PERMISSION_MESSAGE, explainPermissionMessage);
        outState.putCharSequence(EXTRA_RATIONAL_TITLE, rationaleTitle);
        outState.putCharSequence(EXTRA_RATIONAL_MESSAGE, rationaleMessage);
        outState.putString(EXTRA_PACKAGE_NAME, packageName);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        ArrayList<String> deniedPermissions = BasePermission.getDeniedPermissions(this, permissions);

        if (deniedPermissions.isEmpty()) {
            permissionResult(null);
        } else {
            showPermissionDenyDialog(deniedPermissions);
        }
    }

    public void showPermissionDenyDialog(final ArrayList<String> deniedPermissions) {

        if (TextUtils.isEmpty(rationaleMessage)) {
            // rationaleMessage
            //permissionResult(deniedPermissions);
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog_Alert);

        builder.setTitle(rationaleTitle)
                .setMessage(rationaleMessage)
                .setCancelable(false)
                .setNegativeButton(deniedCloseButtonText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });

        if (hasSettingButton) {

            if (TextUtils.isEmpty(settingButtonText)) {
                settingButtonText = SETTING_BTN_TEXT;
            }

            builder.setPositiveButton(settingButtonText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    BasePermission.startSettingActivityForResult(PermissionActivity.this);

                }
            });

        }
        builder.show();
    }

    public boolean shouldShowRequestPermissionRationale(ArrayList<String> needPermissions) {

        if (needPermissions == null) {
            return false;
        }

        for (String permission : needPermissions) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(PermissionActivity.this, permission)) {
                return false;
            }
        }

        return true;

    }

    public void showWindowPermissionDenyDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.Theme_AppCompat_Light_Dialog_Alert);
        builder.setMessage(rationaleMessage)
                .setCancelable(false)
                .setNegativeButton(deniedCloseButtonText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        checkPermissions(false);
                    }
                });

        if (hasSettingButton) {
            if (TextUtils.isEmpty(settingButtonText)) {
                settingButtonText = SETTING_BTN_TEXT;
            }

            builder.setPositiveButton(settingButtonText, new DialogInterface.OnClickListener() {
                @TargetApi(Build.VERSION_CODES.M)
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Uri uri = Uri.fromParts("package", packageName, null);
                    final Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, uri);
                    startActivityForResult(intent, REQ_CODE_SYSTEM_ALERT_WINDOW_PERMISSION_REQUEST_SETTING);
                }
            });

        }
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case BasePermission.REQ_CODE_REQUEST_SETTING:
                checkPermissions(true);
                break;
            case REQ_CODE_SYSTEM_ALERT_WINDOW_PERMISSION_REQUEST:   //ALERT WINDOW
                if (!hasWindowPermission() && !TextUtils.isEmpty(rationaleMessage)) {  //rationaleMessage
                    showWindowPermissionDenyDialog();
                } else {     // denyMessage permission
                    checkPermissions(false);
                }
                break;
            case REQ_CODE_SYSTEM_ALERT_WINDOW_PERMISSION_REQUEST_SETTING:   //  ALERT WINDOW
                checkPermissions(false);
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }

    }
}
