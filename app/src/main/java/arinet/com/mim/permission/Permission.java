package arinet.com.mim.permission;

import android.content.Context;

/**
 * Created by Dev Android  on 1/8/2018.
 */

public class Permission extends BasePermission{

    /**
     *  1. If rational Message available it means Its Mandatory Permission other wise not
     *  2. Rational Message will be shown with Setting & Later Button
     *  3. Single Permission allowed
     */

    public static final String TAG=Permission.class.getSimpleName();

    public static Builder with(Context context) {
        return new Builder(context);
    }


    public static class Builder extends PermissionBuilder<Builder> {

        private Builder(Context context) {
            super(context);
        }

        public void check() {
            checkPermissions();
        }

    }
}
