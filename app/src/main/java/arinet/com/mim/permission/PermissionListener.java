package arinet.com.mim.permission;

import java.util.ArrayList;

/**
 * Created by Dev Android  on 1/8/2018.
 */

public interface PermissionListener {
    void onPermissionGranted();
    void onPermissionDenied(ArrayList<String> deniedPermissions);
}
