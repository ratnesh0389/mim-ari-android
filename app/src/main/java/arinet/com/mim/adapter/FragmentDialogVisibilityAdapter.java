package arinet.com.mim.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import arinet.com.mim.R;
import arinet.com.mim.model.Visibility;
import arinet.com.mim.view.FragmentDialogVisibilityItem;

/**
 * Created by kevinstueber on 9/30/14.
 */
public class FragmentDialogVisibilityAdapter  extends ArrayAdapter<Visibility> {

	private List<Visibility> visibilities;
	private Context context;

	public FragmentDialogVisibilityAdapter(Context context, int resource, List<Visibility> objects) {
		super(context, resource, objects);
		this.visibilities = objects;
		this.context = context;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = new ViewHolder();

		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (convertView == null){
			convertView = (LinearLayout) inflater.inflate(R.layout.fragment_dialog_visibility_item, null);
			holder.header = (LinearLayout)convertView.findViewById(R.id.header);
			holder.headerTextView = (TextView)convertView.findViewById(R.id.header_textview);
			holder.visibilityTextView = (TextView)convertView.findViewById(R.id.visibility_name);
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}

		Visibility visibility = this.visibilities.get(position);
		Visibility previousVisibility = null;

		((FragmentDialogVisibilityItem)convertView).setItemVisibility(visibility);

		boolean hasHeader = false;

		if (position > 0){
			previousVisibility = this.visibilities.get(position-1);
			if (!previousVisibility.getType().equals(visibility.getType())){
				hasHeader = true;
			}
		}
		else if (position == 0){
			hasHeader = true;
		}

		if (hasHeader){
			holder.header.setVisibility(View.VISIBLE);
            String headertext = visibility.getType();
            if (visibility.getType().equalsIgnoreCase("saleschannel")){
                headertext = "Sales Channel";
            }
			holder.headerTextView.setText(headertext);
		}
		else{
			holder.header.setVisibility(View.GONE);
		}

		holder.visibilityTextView.setText(visibility.getName());

		return convertView;
	}

	static class ViewHolder {
		LinearLayout header;
		TextView headerTextView;
		TextView visibilityTextView;
	}

}
