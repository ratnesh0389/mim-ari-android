package arinet.com.mim.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import arinet.com.mim.R;
import arinet.com.mim.model.Unit;

/**
 * Created by kevinstueber on 9/2/14.
 */
public class ProductResultAdapter extends ArrayAdapter<Unit> {

    private List<Unit>units;
    private Context context;

    public ProductResultAdapter(Context context, int resource, List<Unit> objects) {
        super(context, resource, objects);
        this.units = objects;
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout view = (LinearLayout)convertView;
        ViewHolder holder = new ViewHolder();

        final Unit unit = this.units.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null){
            convertView = (LinearLayout) inflater.inflate(R.layout.grid_product_result_item, null);
            holder.unitTitle = (TextView)convertView.findViewById(R.id.text);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.unitTitle.setText(unit.getYear() + " " + unit.getMake() + " " + unit.getModel());

        return convertView;
    }

    static class ViewHolder {
        TextView unitTitle;
    }

}
