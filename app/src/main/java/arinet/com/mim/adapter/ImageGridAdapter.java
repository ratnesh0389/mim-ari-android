package arinet.com.mim.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import arinet.com.mim.R;
import arinet.com.mim.activity.ImageSlider;
import arinet.com.mim.model.Image;
import arinet.com.mim.service.toolkit.ToolkitService;
import arinet.com.mim.vendor.dynamicgrid.BaseDynamicGridAdapter;
import arinet.com.mim.view.ImageGridRelativeLayout;
import arinet.com.mim.view.SquareImageView;

/**
 * Created by kevinstueber on 8/18/14.
 */
public class ImageGridAdapter extends BaseDynamicGridAdapter {
    private Context mContext;
    private List<Image> imageList;
    public ImageGridAdapter(Context context, List<Image> items, int columnCount) {
        super(context, items, columnCount);
        mContext = context;
        imageList = items;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final Image image = (Image) getItem(position);

        if (convertView == null) {
            convertView = (ImageGridRelativeLayout) LayoutInflater.from(getContext()).inflate(R.layout.grid_image_item, null);
        }

        ((ImageGridRelativeLayout) convertView).setImage(image);

        SquareImageView imageView = (SquareImageView) convertView.findViewById(R.id.image_view);
        imageView.setTag(position);
        /*imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int index = (int)v.getTag();
                Intent intent = new Intent(mContext, ImageSlider.class);
                intent.putExtra(ImageSlider.CURRENT_INDEX,index);
                intent.putParcelableArrayListExtra(ImageSlider.IMAGES_LIST,(ArrayList<Image>) imageList);
                mContext.startActivity(intent);
            }
        });*/

        if (image.getThumbLocation() != null) {
            Picasso.with(getContext()).load("http:" + image.getThumbLocation()).placeholder(getContext().getResources().getDrawable(R.drawable.placeholder_offline)).into(imageView);
        } else if (image.getLocalPath() != null) {

            File imgFile = new File(image.getLocalPath());
            if (imgFile.exists()) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
                options.inSampleSize = ToolkitService.getInstance().calculateInSampleSize(options, 100, 100);
                options.inPurgeable = true;
                options.inInputShareable = true;
                options.inJustDecodeBounds = false;
                options.inPreferredConfig = Bitmap.Config.RGB_565;

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
                imageView.setImageBitmap(myBitmap);
            }
        }

        return convertView;
    }

}
