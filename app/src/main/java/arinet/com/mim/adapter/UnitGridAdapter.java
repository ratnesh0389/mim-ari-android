package arinet.com.mim.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import arinet.com.mim.R;
import arinet.com.mim.activity.Stream;
import arinet.com.mim.model.Metric;
import arinet.com.mim.model.Unit;
import arinet.com.mim.service.helper.BitmapWorkerTask;

/**
 * Created by kevinstueber on 7/28/14.
 */
public class UnitGridAdapter extends ArrayAdapter<Unit> {

    private Context context;
    private List<Unit>units;
    private Stream streamActivity;

    public UnitGridAdapter(Context context, int resource, List<Unit> objects) {
        super(context, resource, objects);
        this.context = context;
        this.units = objects;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        RelativeLayout view = (RelativeLayout)convertView;
        ViewHolder holder = new ViewHolder();

        final Unit unit = this.units.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null){
            convertView = (RelativeLayout) inflater.inflate(R.layout.grid_unit_item, null);
            holder.topAccent = (View)convertView.findViewById(R.id.view_top_accent);
            holder.unitTitle = (TextView)convertView.findViewById(R.id.textview_unit_title);
            holder.unitImages = (TextView)convertView.findViewById(R.id.textview_unit_images);
            holder.unitViews = (TextView)convertView.findViewById(R.id.textview_unit_views);
            holder.unitPrice = (TextView)convertView.findViewById(R.id.grid_item_price_value);
            holder.unitUsage = (TextView)convertView.findViewById(R.id.grid_item_usage_value);
            holder.unitVin = (TextView)convertView.findViewById(R.id.grid_item_vin_value);
            holder.unitStock = (TextView)convertView.findViewById(R.id.grid_item_stock_value);
            holder.imageView = (ImageView)convertView.findViewById(R.id.grid_item_image_view);
            holder.detailButtonView = (LinearLayout)convertView.findViewById(R.id.grid_item_details_button_view);
            holder.imagesButtonView = (LinearLayout)convertView.findViewById(R.id.grid_item_images_button_view);
            holder.visibilityButtonView = (LinearLayout)convertView.findViewById(R.id.grid_item_visibility_button_view);
            holder.unitViewsImage = (ImageView)convertView.findViewById(R.id.imageview_views_icon);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (unit.getInventoryStatusValue().equals("Finalized")){
            holder.topAccent.setBackgroundResource(R.drawable.griditemtopaccent);
        }
        else{
            holder.topAccent.setBackgroundResource(R.drawable.griditemtopaccentorange);
        }

        holder.unitTitle.setText(unit.getYear() + " " + unit.getMake() + " " + unit.getModel());
        holder.unitImages.setText(unit.getImages().size() + " images");

        if (unit.getHitsThirty() > 0){
            holder.unitViewsImage.setVisibility(View.VISIBLE);
            holder.unitViews.setVisibility(View.VISIBLE);
            holder.unitViews.setText(unit.getHitsThirty() + " views this month");
        }
        else{
            holder.unitViewsImage.setVisibility(View.GONE);
            holder.unitViews.setVisibility(View.GONE);
        }

        holder.unitUsage.setText("");

        for (Metric metric : unit.getMetrics()) {
            if (metric.isSelected() && metric.getValue() > -1) {
                holder.unitUsage.setText(metric.getValue() + " " + metric.getAbbreviation());
                break;
            }
        }

        if (unit.getPrice() > 0 && unit.getPriceLabel() == 5){
            holder.unitPrice.setText(String.format( "$%.2f", unit.getPrice() ));
        }
        else{
            holder.unitPrice.setText("");
        }
        holder.unitVin.setText(unit.getVin());
        holder.unitStock.setText(unit.getStockNumber());

        if (unit.getImages().size() > 0){
            String imageLocation = unit.getImages().get(0).getLocation();
            String imagePath = unit.getImages().get(0).getLocalPath();

            if (imageLocation != null){
                Picasso.with(context).load("http:"+imageLocation).placeholder(context.getResources().getDrawable(R.drawable.placeholder_offline)).into(holder.imageView);
            }
            else if (imagePath != null){
                holder.imageView.setImageBitmap(null);

                BitmapWorkerTask task = new BitmapWorkerTask(holder.imageView);
                task.execute(imagePath);
            }
        }
        else{
            holder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.placeholder_offline));
        }

        holder.detailButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                streamActivity.goToDetail(unit, false);

            }
        });

        enableViews(holder.imagesButtonView,unit.getInventoryStatusValue().equals("Finalized"));
        holder.imagesButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                streamActivity.goToImages(unit);
            }
        });

        holder.visibilityButtonView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                streamActivity.goToDetail(unit, true);
            }
        });


        return convertView;
    }

    static class ViewHolder {
        View topAccent;
        TextView unitTitle;
        TextView unitImages;
        TextView unitViews;
        TextView unitPrice;
        TextView unitUsage;
        TextView unitVin;
        TextView unitStock;
        LinearLayout detailButtonView;
        LinearLayout imagesButtonView;
        LinearLayout visibilityButtonView;
        ImageView imageView;
        ImageView unitViewsImage;
    }

    public Stream getStreamActivity() {
        return streamActivity;
    }

    public void setStreamActivity(Stream streamActivity) {
        this.streamActivity = streamActivity;
    }


    private void enableViews(View v, boolean enabled) {
        if (v instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) v;
            for (int i = 0;i<vg.getChildCount();i++) {
                enableViews(vg.getChildAt(i), enabled);
            }
        }
        v.setEnabled(enabled);
    }
}
