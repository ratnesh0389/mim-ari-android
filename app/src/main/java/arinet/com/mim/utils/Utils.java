package arinet.com.mim.utils;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import arinet.com.mim.activity.Browser;

public class Utils {

    public static void openPrivacyBrowser(Activity activity){
        String privacyUrl = "https://www.iubenda.com/privacy-policy/56167286/full-legal";
        Intent intent = new Intent(activity, Browser.class);
        intent.putExtra("url",privacyUrl);
        activity.startActivity(intent);
        /*String privacyUrl = "https://www.iubenda.com/privacy-policy/56167286/full-legal";
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(privacyUrl));
        activity.startActivity(browserIntent);*/
    }
}
