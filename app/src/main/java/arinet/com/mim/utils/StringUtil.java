package arinet.com.mim.utils;

import java.lang.reflect.Array;
import java.util.regex.PatternSyntaxException;

/**
 * Created by Dev Android on 2/9/2018.
 */

public class StringUtil {

    /**
     * @param string
     * @return true if String is null or empty or contains space only
     */
    public static boolean isNullOrEmpty(String string) {
        if (string == null
                || string.trim().equalsIgnoreCase("")
                || string.trim().equalsIgnoreCase("null")
                || string.trim().equalsIgnoreCase("na"))
            return true;
        /*if(TextUtils.isEmpty(string) || string.trim().equalsIgnoreCase("null"))
        return  true;*/
        return false;
    }


    /**
     *
     * @param string
     * @param splitString
     * @return
     */
    public static String[] splitString(String string, String splitString) {

        String[] splitArray = {string};
        try {
            splitArray = string.split(splitString);
        } catch (PatternSyntaxException ex) {

        }
        return splitArray;
    }
}
