package arinet.com.mim.model;

/**
 * Created by kevinstueber on 8/6/14.
 */
public class Industry {

    private int industryId;
    private String name;

    public int getIndustryId() {
        return industryId;
    }

    public void setIndustryId(int industryId) {
        this.industryId = industryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
