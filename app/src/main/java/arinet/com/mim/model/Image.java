package arinet.com.mim.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by kevinstueber on 7/29/14.
 */
public class Image implements Comparable<Image>, Parcelable {

    private long localId;
    private int imageId;
    private int stockImageId;
    private int unitId;
    private long unitLocalId;
    private int sequence;
    private boolean isPrimary;
    private boolean isNew;
    private boolean delete;
    private boolean updated;
    private boolean productResult;
    private String location;
    private String thumbLocation;
    private String localPath;

    public Image () {}

    public Image(Image another) {
        this.imageId = another.getImageId();
        this.stockImageId = another.getStockImageId();
        this.location = another.getLocation();
        this.thumbLocation = another.getThumbLocation();
        this.sequence = another.getSequence();
        this.isPrimary = another.isPrimary();
        this.localId = 0;
        this.productResult = false;
    }

    protected Image(Parcel in) {
        localId = in.readLong();
        imageId = in.readInt();
        stockImageId = in.readInt();
        unitId = in.readInt();
        unitLocalId = in.readLong();
        sequence = in.readInt();
        isPrimary = in.readByte() != 0;
        isNew = in.readByte() != 0;
        delete = in.readByte() != 0;
        updated = in.readByte() != 0;
        productResult = in.readByte() != 0;
        location = in.readString();
        thumbLocation = in.readString();
        localPath = in.readString();
    }

    public static final Creator<Image> CREATOR = new Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel in) {
            return new Image(in);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };

    public int compareTo(Image anotherInstance) {
        return this.getSequence() - anotherInstance.getSequence();
    }

    public long getLocalId() {
        return localId;
    }

    public void setLocalId(long localId) {
        this.localId = localId;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public int getStockImageId() {
        return stockImageId;
    }

    public void setStockImageId(int stockImageId) {
        this.stockImageId = stockImageId;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public long getUnitLocalId() {
        return unitLocalId;
    }

    public void setUnitLocalId(long unitLocalId) {
        this.unitLocalId = unitLocalId;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public boolean isPrimary() {
        return isPrimary;
    }

    public void setPrimary(boolean isPrimary) {
        this.isPrimary = isPrimary;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean isNew) {
        this.isNew = isNew;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getThumbLocation() {
        return thumbLocation;
    }

    public void setThumbLocation(String thumbLocation) {
        this.thumbLocation = thumbLocation;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public boolean isProductResult() {
        return productResult;
    }

    public void setProductResult(boolean productResult) {
        this.productResult = productResult;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(localId);
        dest.writeInt(imageId);
        dest.writeInt(stockImageId);
        dest.writeInt(unitId);
        dest.writeLong(unitLocalId);
        dest.writeInt(sequence);
        dest.writeByte((byte) (isPrimary ? 1 : 0));
        dest.writeByte((byte) (isNew ? 1 : 0));
        dest.writeByte((byte) (delete ? 1 : 0));
        dest.writeByte((byte) (updated ? 1 : 0));
        dest.writeByte((byte) (productResult ? 1 : 0));
        dest.writeString(location);
        dest.writeString(thumbLocation);
        dest.writeString(localPath);
    }
}
