package arinet.com.mim.model;

/**
 * Created by kevinstueber on 8/6/14.
 */
public class Visibility implements Comparable<Visibility>{

    private int visibilityId;
    private int siteId;
    private int unitId;
    private String name;
    private String type;
    private boolean active;
	private long localUnitId;

	public Visibility(){}

	public Visibility(Visibility another) {
		this.visibilityId = another.getVisibilityId();
		this.siteId = another.getSiteId();
		this.name = another.getName();
		this.type = another.getType();
		this.active = another.isActive();
	}

	public int compareTo(Visibility anotherInstance) {
		return this.getName().compareTo(anotherInstance.getName());
	}

    public int getVisibilityId() {
        return visibilityId;
    }

    public void setVisibilityId(int visibilityId) {
        this.visibilityId = visibilityId;
    }

    public int getSiteId() {
        return siteId;
    }

    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

	public long getLocalUnitId() {
		return localUnitId;
	}

	public void setLocalUnitId(long localUnitId) {
		this.localUnitId = localUnitId;
	}
}
