package arinet.com.mim.model;

/**
 * Created by kevinstueber on 8/6/14.
 */
public class Metric {

    private int metricId;
    private int unitId;
    private int profileId;
    private int sequence;
    private int value;
    private boolean selected;
    private String abbreviation;
    private String name;

    public Metric () {}

    public Metric(Metric another) {
        this.metricId = another.getMetricId();
        this.profileId = another.getProfileId();
        this.sequence = another.getSequence();
        this.value = another.getValue();
        this.selected = another.isSelected();
        this.abbreviation = another.getAbbreviation();
        this.name = another.getName();
    }

    public int getMetricId() {
        return metricId;
    }

    public void setMetricId(int metricId) {
        this.metricId = metricId;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public int getProfileId() {
        return profileId;
    }

    public void setProfileId(int profileId) {
        this.profileId = profileId;
    }

    public int getSequence() {
        return sequence;
    }

    public void setSequence(int sequence) {
        this.sequence = sequence;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
