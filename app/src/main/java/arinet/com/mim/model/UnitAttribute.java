package arinet.com.mim.model;

/**
 * Created by kevinstueber on 10/21/14.
 */
public class UnitAttribute {

    private long unitAttributeId;//Local db id
    private long attributeId;//id from api
    private long unitId;//associated unit id
    private String  name;
    private String  type;
    private String  value;

    public long getUnitAttributeId() {
        return unitAttributeId;
    }

    public void setUnitAttributeId(long unitAttributeId) {
        this.unitAttributeId = unitAttributeId;
    }

    public long getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(long attributeId) {
        this.attributeId = attributeId;
    }

    public long getUnitId() {
        return unitId;
    }

    public void setUnitId(long unitId) {
        this.unitId = unitId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
