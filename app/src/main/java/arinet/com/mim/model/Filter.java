package arinet.com.mim.model;

/**
 * Created by kevinstueber on 7/29/14.
 */
public class Filter {

    private String name;
    private int count;
    private String query;
    private String location;
    private int storeLocationId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getStoreLocationId() {
        return storeLocationId;
    }

    public void setStoreLocationId(int storeLocationId) {
        this.storeLocationId = storeLocationId;
    }
}
