package arinet.com.mim.model;

/**
 * Created by kevinstueber on 9/3/14.
 */
public class StyleType {

    private int styleTypeId;
    private int typeId;
    private String name;

    public int getStyleTypeId() {
        return styleTypeId;
    }

    public void setStyleTypeId(int styleTypeId) {
        this.styleTypeId = styleTypeId;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
