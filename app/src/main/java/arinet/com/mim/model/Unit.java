package arinet.com.mim.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Date;

/*
  Created by kevinstueber on 7/28/14.
*/
public class Unit implements Comparable<Unit>{

    private long localId;
    private int unitId;
    private int year;
    private int makeId;
    private double price;
    private int equipmentTypeId;
    private int styleId;
    private int linkedProductId;
    private int hitsSeven;
    private int hitsThirty;
    private int hitsNintey;
    private int hitsThreeSixFive;
    private int dmsPrice;
    private int titleStatus;
    private int condition;
    private int priceLabel;
    private int usageStatus;
    private int siteId;
    private int locationId;
    private double salePrice;
    private double bestPrice;
    private boolean callForPrice;
    private boolean publishedOnWeb;
    private boolean useStockImages;
    private boolean onSale;
    private boolean displayInShowcaseCount;
    private boolean saleActive;
    private boolean isNew;
    private boolean updated;
    private boolean delete;
    private boolean productResult;
    private String model;
    private String stockNumber;
    private String vin;
    private String globalIdentifier;
    private String inventoryStatusValue;
    private String make;
    private String description;
    private String equipmentType;
    private String linkedProductName;
    private String primaryColor;
    private String trimColor;
    private String lastUpdate;
    private String saleType;
    private String saleStartDate;
    private String saleEndDate;
    private String unfinalizedUnitId;
    private String url;
    private ArrayList<Image> images;
    private ArrayList<Visibility> visibilities;
    private ArrayList<Metric> metrics;
    private int industryId;
    private ArrayList<UnitAttribute> unitAttributes;

    public Unit(){
        this.metrics = new ArrayList<Metric>();
	    this.visibilities = new ArrayList<Visibility>();
	    this.images = new ArrayList<Image>();
        this.unitAttributes = new ArrayList<UnitAttribute>();
    }

    public Unit(Unit another) {
        this.unitId = 0;
        this.year = another.getYear();
        this.makeId = another.getMakeId();
        this.price = another.getPrice();
        this.equipmentTypeId = another.getEquipmentTypeId();
        this.styleId = another.getStyleId();
        this.linkedProductId = another.getLinkedProductId();
        this.hitsSeven = 0;
        this.hitsThirty = 0;
        this.hitsNintey = 0;
        this.hitsThreeSixFive = 0;
        this.dmsPrice = another.getDmsPrice();
        this.titleStatus = another.getTitleStatus();
        this.condition = another.getCondition();
        this.priceLabel = another.getPriceLabel();
        this.usageStatus = another.getUsageStatus();
        this.siteId = another.getSiteId();
        this.locationId = another.getLocationId();
        this.salePrice = another.getSalePrice();
        this.bestPrice = another.getBestPrice();
        this.callForPrice = another.isCallForPrice();
        this.publishedOnWeb = another.isPublishedOnWeb();
        this.useStockImages = another.isUseStockImages();
        this.onSale  = another.isOnSale();
        this.displayInShowcaseCount = another.isDisplayInShowcaseCount();
        this.saleActive = another.isSaleActive();
        this.isNew = true;
        this.updated = false;
        this.delete = false;
        this.model = another.getModel();
        this.inventoryStatusValue = another.getInventoryStatusValue();
        this.make = another.getMake();
        this.description = another.getDescription();
        this.equipmentType = another.getEquipmentType();
        this.linkedProductName = another.getLinkedProductName();
        this.primaryColor = another.getPrimaryColor();
        this.trimColor = another.getTrimColor();
        this.lastUpdate =
        this.saleType = another.getSaleType();
        this.saleStartDate = another.getSaleStartDate();
        this.saleEndDate = another.getSaleEndDate();
        this.productResult = another.isProductResult();

        ArrayList<Image>images = new ArrayList<Image>();

        for (Image image : another.images) {
            if (image.getStockImageId() > 0) {
                Image imageCopy = new Image(image);
                images.add(imageCopy);
            }
        }
        this.images = images;

        ArrayList<Metric>metrics = new ArrayList<Metric>();

        for (Metric metric : another.getMetrics()){
            Metric metricCopy = new Metric(metric);
            metrics.add(metricCopy);
        }

	    this.metrics = metrics;

	    ArrayList<Visibility> visibilities = new ArrayList<Visibility>();

	    for (Visibility visibility : another.getVisibilities()){
			Visibility visibilityCopy = new Visibility(visibility);
			visibilities.add(visibilityCopy);
	    }

		this.visibilities = visibilities;
    }

    @Override
    public int compareTo(Unit unit) {
        Date date1 = null;
        Date date2 = null;
        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH).parse(this.getLastUpdate());
            date2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH).parse(unit.getLastUpdate());
        }
        catch (ParseException e){
            e.printStackTrace();
        }

        return date1.compareTo(date2);
    }

    public String getUnfinalizedUnitId() {
        return unfinalizedUnitId;
    }

    public void setUnfinalizedUnitId(String unfinalizedUnitId) {
        this.unfinalizedUnitId = unfinalizedUnitId;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setSalePrice(double salePrice) {
        this.salePrice = salePrice;
    }

    public double getPrice() {
        return price;
    }

    public double getSalePrice() {
        return salePrice;
    }

    public double getBestPrice() {
        return bestPrice;
    }

    public void setBestPrice(double bestPrice) {
        this.bestPrice = bestPrice;
    }

    public long getLocalId() {
        return localId;
    }

    public void setLocalId(long localId) {
        this.localId = localId;
    }

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMakeId() {
        return makeId;
    }

    public void setMakeId(int makeId) {
        this.makeId = makeId;
    }

    public int getEquipmentTypeId() {
        return equipmentTypeId;
    }

    public void setEquipmentTypeId(int equipmentTypeId) {
        this.equipmentTypeId = equipmentTypeId;
    }

    public int getStyleId() {
        return styleId;
    }

    public void setStyleId(int styleId) {
        this.styleId = styleId;
    }

    public int getLinkedProductId() {
        return linkedProductId;
    }

    public void setLinkedProductId(int linkedProductId) {
        this.linkedProductId = linkedProductId;
    }

    public int getHitsSeven() {
        return hitsSeven;
    }

    public void setHitsSeven(int hitsSeven) {
        this.hitsSeven = hitsSeven;
    }

    public int getHitsThirty() {
        return hitsThirty;
    }

    public void setHitsThirty(int hitsThirty) {
        this.hitsThirty = hitsThirty;
    }

    public int getHitsNintey() {
        return hitsNintey;
    }

    public void setHitsNintey(int hitsNintey) {
        this.hitsNintey = hitsNintey;
    }

    public int getHitsThreeSixFive() {
        return hitsThreeSixFive;
    }

    public void setHitsThreeSixFive(int hitsThreeSixFive) {
        this.hitsThreeSixFive = hitsThreeSixFive;
    }

    public int getDmsPrice() {
        return dmsPrice;
    }

    public void setDmsPrice(int dmsPrice) {
        this.dmsPrice = dmsPrice;
    }

    public int getTitleStatus() {
        return titleStatus;
    }

    public void setTitleStatus(int titleStatus) {
        this.titleStatus = titleStatus;
    }

    public int getCondition() {
        return condition;
    }

    public void setCondition(int condition) {
        this.condition = condition;
    }

    public int getPriceLabel() {
        return priceLabel;
    }

    public void setPriceLabel(int priceLabel) {
        this.priceLabel = priceLabel;
    }

    public int getUsageStatus() {
        return usageStatus;
    }

    public void setUsageStatus(int usageStatus) {
        this.usageStatus = usageStatus;
    }

    public int getSiteId() {
        return siteId;
    }

    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public boolean isCallForPrice() {
        return callForPrice;
    }

    public void setCallForPrice(boolean callForPrice) {
        this.callForPrice = callForPrice;
    }

    public boolean isPublishedOnWeb() {
        return publishedOnWeb;
    }

    public void setPublishedOnWeb(boolean publishedOnWeb) {
        this.publishedOnWeb = publishedOnWeb;
    }

    public boolean isUseStockImages() {
        return useStockImages;
    }

    public void setUseStockImages(boolean useStockImages) {
        this.useStockImages = useStockImages;
    }

    public boolean isOnSale() {
        return onSale;
    }

    public void setOnSale(boolean onSale) {
        this.onSale = onSale;
    }

    public boolean isDisplayInShowcaseCount() {
        return displayInShowcaseCount;
    }

    public void setDisplayInShowcaseCount(boolean displayInShowcaseCount) {
        this.displayInShowcaseCount = displayInShowcaseCount;
    }

    public boolean isSaleActive() {
        return saleActive;
    }

    public void setSaleActive(boolean saleActive) {
        this.saleActive = saleActive;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean isNew) {
        this.isNew = isNew;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getStockNumber() {
        return stockNumber;
    }

    public void setStockNumber(String stockNumber) {
        this.stockNumber = stockNumber;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getGlobalIdentifier() {
        return globalIdentifier;
    }

    public void setGlobalIdentifier(String globalIdentifier) {
        this.globalIdentifier = globalIdentifier;
    }

    public String getInventoryStatusValue() {
        return inventoryStatusValue;
    }

    public void setInventoryStatusValue(String inventoryStatusValue) {
        this.inventoryStatusValue = inventoryStatusValue;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(String equipmentType) {
        this.equipmentType = equipmentType;
    }

    public String getLinkedProductName() {
        return linkedProductName;
    }

    public void setLinkedProductName(String linkedProductName) {
        this.linkedProductName = linkedProductName;
    }

    public String getPrimaryColor() {
        return primaryColor;
    }

    public void setPrimaryColor(String primaryColor) {
        this.primaryColor = primaryColor;
    }

    public String getTrimColor() {
        return trimColor;
    }

    public void setTrimColor(String trimColor) {
        this.trimColor = trimColor;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getSaleType() {
        return saleType;
    }

    public void setSaleType(String saleType) {
        this.saleType = saleType;
    }

    public String getSaleStartDate() {
        return saleStartDate;
    }

    public void setSaleStartDate(String saleStartDate) {
        this.saleStartDate = saleStartDate;
    }

    public String getSaleEndDate() {
        return saleEndDate;
    }

    public void setSaleEndDate(String saleEndDate) {
        this.saleEndDate = saleEndDate;
    }

    public ArrayList<Image> getImages() {
        return images;
    }

    public void setImages(ArrayList<Image> images) {
        this.images = images;
    }

    public ArrayList<Visibility> getVisibilities() {
        return visibilities;
    }

    public void setVisibilities(ArrayList<Visibility> visibilities) {
        this.visibilities = visibilities;
    }

    public ArrayList<Metric> getMetrics() {
        return metrics;
    }

    public void setMetrics(ArrayList<Metric> metrics) {
        this.metrics = metrics;
    }

    public boolean isProductResult() {
        return productResult;
    }

    public void setProductResult(boolean productResult) {
        this.productResult = productResult;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIndustryId() {
        return industryId;
    }

    public void setIndustryId(int industryId) {
        this.industryId = industryId;
    }

    public ArrayList<UnitAttribute> getUnitAttributes() {
        return unitAttributes;
    }

    public void setUnitAttributes(ArrayList<UnitAttribute> unitAttributes) {
        this.unitAttributes = unitAttributes;
    }
}
