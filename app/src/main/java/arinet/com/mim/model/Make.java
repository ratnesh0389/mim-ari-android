package arinet.com.mim.model;

/**
 * Created by kevinstueber on 8/6/14.
 */
public class Make {

    private int makeId;
    private int industryId;
    private String name;

    public int getMakeId() {
        return makeId;
    }

    public void setMakeId(int makeId) {
        this.makeId = makeId;
    }

    public int getIndustryId() {
        return industryId;
    }

    public void setIndustryId(int industryId) {
        this.industryId = industryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
