package arinet.com.mim.fragment;



import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;

import androidx.fragment.app.Fragment;
import arinet.com.mim.R;
import arinet.com.mim.activity.Detail;
import arinet.com.mim.activity.ImageSlider;
import arinet.com.mim.service.toolkit.ToolkitService;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class HeroImageFragment extends Fragment {

    private int currentIndex=0;

    public HeroImageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hero_image, container, false);

        String imageLocation = this.getArguments().getString("image_location");
        String imagePath = this.getArguments().getString("image_path");
        currentIndex = this.getArguments().getInt(ImageSlider.CURRENT_INDEX,0);

        ImageView imageView = (ImageView)view.findViewById(R.id.hero_pager_image_view);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(getActivity() instanceof Detail){
                    ((Detail)getActivity()).openFullView(currentIndex);
                }
            }
        });

        if (imageLocation != null){
            Picasso.with(this.getActivity().getApplicationContext())
                    .load("http:"+imageLocation)
                    .placeholder(this.getActivity().getApplicationContext().getResources().getDrawable(R.drawable.placeholder_offline)).into(imageView);
        }
        else if (imagePath != null){
            File imgFile = new File(imagePath);
            if(imgFile.exists()){
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
                options.inSampleSize = ToolkitService.getInstance().calculateInSampleSize(options, 400, 400);
                options.inPurgeable = true;
                options.inInputShareable = true;
                options.inJustDecodeBounds = false;
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
                imageView.setImageBitmap(myBitmap);
            }
        }

        return view;
    }


}
