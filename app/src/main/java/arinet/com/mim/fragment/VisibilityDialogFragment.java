package arinet.com.mim.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

import arinet.com.mim.R;
import arinet.com.mim.activity.Stream;
import arinet.com.mim.adapter.FragmentDialogVisibilityAdapter;
import arinet.com.mim.model.Visibility;
import arinet.com.mim.service.site.SiteService;
import arinet.com.mim.service.visibility.VisibilityService;

/**
 * Created by kevinstueber on 9/30/14.
 */
public class VisibilityDialogFragment extends DialogFragment {

	private ListView list;
	private FragmentDialogVisibilityAdapter adapter;

	public static VisibilityDialogFragment newInstance() {
		VisibilityDialogFragment f = new VisibilityDialogFragment();
		return f;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View v = inflater.inflate(R.layout.fragment_dialog_visibility, null);

		ArrayList<Visibility>visibilities = VisibilityService.getInstance().getVisibilitiesForSite(getActivity().getApplicationContext(), SiteService.getInstance().getSite(getActivity().getApplicationContext()));

		list = (ListView)v.findViewById(R.id.list);
		adapter = new FragmentDialogVisibilityAdapter(getActivity().getApplicationContext(), android.R.id.text1,visibilities);
		list.setAdapter(adapter);
		list.setOnItemClickListener(((Stream)getActivity()).dialogOnClickListener);

		builder.setView(v);
		return builder.create();
	}
}
