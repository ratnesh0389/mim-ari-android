package arinet.com.mim.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import arinet.com.mim.R;
import arinet.com.mim.activity.Detail;
import arinet.com.mim.activity.Stream;
import arinet.com.mim.adapter.FragmentDialogVisibilityAdapter;
import arinet.com.mim.model.Visibility;
import arinet.com.mim.service.site.SiteService;
import arinet.com.mim.service.visibility.VisibilityService;

/**
 * Created by kevinstueber on 10/22/14.
 */
public class DescriptionDialogFragment extends DialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_description_dialog, container, false);

        getDialog().setTitle("Description");

        final EditText et = (EditText)v.findViewById(R.id.description_edit_text);
        et.setText(((Detail)getActivity()).getUnit().getDescription());

        Button b = (Button)v.findViewById(R.id.description_save_button);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Detail)getActivity()).getUnit().setDescription(et.getText().toString());
                ((Detail)getActivity()).updateDescriptionButtonText(et.getText().toString());
                dismiss();
            }
        });

        return v;
    }
}
