package arinet.com.mim.service.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by kevinstueber on 7/28/14.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static DatabaseHelper instance = null;

    static String DATABASE_NAME = "mim.db";
    static int DATABASE_VERSION = 8;

    public static final String TABLE_SITE           = "site";
    public static final String TABLE_UNIT           = "unit";
    public static final String TABLE_IMAGE          = "image";
    public static final String TABLE_FILTER         = "filter";
    public static final String TABLE_LOCATION       = "location";
    public static final String TABLE_VISIBILITY     = "visibility";
    public static final String TABLE_METRIC         = "metric";
    public static final String TABLE_ATTRIBUTE      = "attribute";
    public static final String TABLE_UNIT_ATTRIBUTE = "unit_attribute";

    public static final String COLUMN_ID            = "id";
    public static final String SITE_ID              = "site_id";
    public static final String UNIT_ID              = "unit_id";
    public static final String NEW                  = "new";
    public static final String UPDATED              = "updated";
    public static final String DELETE               = "_delete";
    public static final String LOCATION             = "location";
    public static final String NAME                 = "name";
    public static final String LOCATION_ID          = "location_id";
    public static final String YEAR                 = "year";
    public static final String MAKE_ID              = "make_id";
    public static final String PRICE                = "price";
    public static final String EQUIPMENT_TYPE_ID    = "equipment_type_id";
    public static final String STYLE_ID             = "style_id";
    public static final String LINKED_PRODUCT_ID    = "linked_product_id";
    public static final String HITS_SEVEN           = "hits_seven";
    public static final String HITS_THIRTY          = "hits_thirty";
    public static final String HITS_NINETY          = "hits_ninety";
    public static final String HITS_THREE_SIXTY_FIVE = "hits_three_six_five";
    public static final String DMS_PRICE            = "dms_price";
    public static final String TITLE_STATUS         = "title_status";
    public static final String CONDITION            = "condition";
    public static final String PRICE_LABEL          = "price_label";
    public static final String USAGE_STATUS         = "usage_status";
    public static final String SALE_PRICE           = "sale_price";
    public static final String CALL_FOR_PRICE       = "call_for_price";
    public static final String PUBLISHED_ON_WEB     = "published_on_web";
    public static final String USE_STOCK_IMAGES     = "use_stock_images";
    public static final String ON_SALE              = "on_sale";
    public static final String DISPLAY_IN_SHOWCASE_COUNT = "display_in_showcase_count";
    public static final String SALE_ACTIVE          = "sale_active";
    public static final String MODEL                = "model";
    public static final String STOCK_NUMBER         = "stock_number";
    public static final String VIN                  = "vin";
    public static final String GLOBAL_IDENTIFIER    = "global_identifier";
    public static final String INVENTORY_STATUS_VALUE = "inventory_status_value";
    public static final String MAKE                 = "make";
    public static final String DESCRIPTION          = "description";
    public static final String EQUIPMENT_TYPE       = "equipment_type";
    public static final String LINKED_PRODUCT_NAME  = "linked_product_name";
    public static final String PRIMARY_COLOR        = "primary_color";
    public static final String TRIM_COLOR           = "trim_color";
    public static final String LAST_UPDATE          = "last_update";
    public static final String SALE_TYPE            = "sale_type";
    public static final String SALE_START_DATE      = "sale_start_date";
    public static final String SALE_END_DATE        = "sale_end_date";
    public static final String UNFINALIZED_UNIT_ID  = "unfinalized_unit_id";
    public static final String PRODUCT_RESULT       = "product_result";
    public static final String URL                  = "url";
    public static final String BEST_PRICE           = "best_price";
    public static final String ACCOUNT_GUID         = "account_guid";
    public static final String ORGANIZATION_GUID    = "organization_guid";
    public static final String EXPIRES              = "expires";
    public static final String EMAIL                = "email";
    public static final String TOKEN                = "token";
    public static final String CURRENT              = "current";
    public static final String IMAGE_ID             = "image_id";
    public static final String STOCK_IMAGE_ID       = "stock_image_id";
    public static final String UNIT_LOCAL_ID        = "unit_local_id";
    public static final String SEQUENCE             = "sequence";
    public static final String IS_PRIMARY           = "is_primary";
    public static final String THUMB_LOCATION       = "thumb_location";
    public static final String LOCAL_PATH           = "local_path";
    public static final String COUNT                = "count";
    public static final String QUERY                = "query";
    public static final String STORE_LOCATION_ID    = "store_location_id";
    public static final String STREET               = "street";
    public static final String CITY                 = "city";
    public static final String REGION               = "region";
    public static final String COUNTRY              = "country";
    public static final String POSTAL_CODE          = "postal_code";
    public static final String PHONE                = "phone";
    public static final String VISIBILITY_ID        = "visibility_id";
    public static final String TYPE                 = "type";
    public static final String ACTIVE               = "active";
    public static final String METRIC_ID            = "metric_id";
    public static final String PROFILE_ID           = "profile_id";
    public static final String VALUE                = "value";
    public static final String SELECTED             = "selected";
    public static final String ABBREVIATION         = "abbreviation";
    public static final String ATTRIBUTE_ID         = "attribute_id";
    public static final String INDUSTRY_ID          = "industry_id";

    private static final String DATABASE_CREATE_SITE_TABLE = "create table "
            + TABLE_SITE 						+ "("
            + COLUMN_ID             			+ " integer primary key autoincrement, "
            + SITE_ID                           + " integer, "
            + ACCOUNT_GUID                      + " text, "
            + ORGANIZATION_GUID                 + " text, "
            + EXPIRES                           + " text, "
            + EMAIL                             + " text, "
            + TOKEN                             + " text, "
            + CURRENT                           + " boolean); ";

    private static final String DATABASE_CREATE_UNIT_TABLE = "create table "
            + TABLE_UNIT 						+ "("
            + COLUMN_ID             			+ " integer primary key autoincrement, "
            + UNIT_ID                			+ " integer, "
            + YEAR                              + " integer, "
            + MAKE_ID                           + " integer, "
            + PRICE                             + " real, "
            + EQUIPMENT_TYPE_ID                 + " integer, "
            + STYLE_ID                          + " integer, "
            + LINKED_PRODUCT_ID                 + " integer, "
            + HITS_SEVEN                        + " integer, "
            + HITS_THIRTY                       + " integer, "
            + HITS_NINETY                       + " integer, "
            + HITS_THREE_SIXTY_FIVE             + " integer, "
            + DMS_PRICE                         + " real, "
            + SITE_ID                           + " integer, "
            + LOCATION_ID                       + " integer, "
            + TITLE_STATUS                      + " integer, "
            + CONDITION                         + " integer, "
            + PRICE_LABEL                       + " integer, "
            + USAGE_STATUS                      + " integer, "
            + SALE_PRICE                        + " real, "
            + CALL_FOR_PRICE                    + " boolean, "
            + PUBLISHED_ON_WEB                  + " boolean, "
            + USE_STOCK_IMAGES                  + " boolean, "
            + ON_SALE                           + " boolean, "
            + DISPLAY_IN_SHOWCASE_COUNT         + " boolean, "
            + SALE_ACTIVE                       + " boolean, "
            + NEW                               + " boolean, "
            + UPDATED                           + " boolean, "
            + DELETE                            + " boolean, "
            + MODEL                             + " text, "
            + STOCK_NUMBER                      + " text, "
            + VIN                               + " text, "
            + GLOBAL_IDENTIFIER                 + " text, "
            + INVENTORY_STATUS_VALUE            + " text, "
            + MAKE                              + " text, "
            + DESCRIPTION                       + " text, "
            + EQUIPMENT_TYPE                    + " text, "
            + LINKED_PRODUCT_NAME               + " text, "
            + PRIMARY_COLOR                     + " text, "
            + TRIM_COLOR                        + " text, "
            + LAST_UPDATE                       + " text, "
            + SALE_TYPE                         + " text, "
            + SALE_START_DATE                   + " text, "
            + SALE_END_DATE                     + " text, "
            + UNFINALIZED_UNIT_ID               + " text, "
            + PRODUCT_RESULT                    + " boolean, "
            + URL                               + " text, "
            + BEST_PRICE                        + " real, "
            + INDUSTRY_ID                       + " integer);";


    private static final String DATABASE_CREATE_IMAGE_TABLE = "create table "
            + TABLE_IMAGE 						+ "("
            + COLUMN_ID             			+ " integer primary key autoincrement, "
            + IMAGE_ID                          + " integer, "
            + STOCK_IMAGE_ID                    + " integer, "
            + UNIT_ID                           + " integer, "
            + UNIT_LOCAL_ID                     + " integer, "
            + SEQUENCE                          + " integer, "
            + IS_PRIMARY                        + " boolean, "
            + NEW                               + " boolean, "
            + DELETE                            + " boolean, "
            + UPDATED                           + " boolean, "
            + LOCATION                          + " text, "
            + THUMB_LOCATION                    + " text, "
            + LOCAL_PATH                        + " text, "
            + PRODUCT_RESULT                    + " boolean);";


    private static final String DATABASE_CREATE_FILTER_TABLE = "create table "
            + TABLE_FILTER 						+ "("
            + COLUMN_ID             			+ " integer primary key autoincrement, "
            + NAME                              + " text, "
            + COUNT                             + " integer, "
            + QUERY                             + " text, "
            + LOCATION                          + " text, "
            + STORE_LOCATION_ID                 + " integer);";

    private static final String DATABASE_CREATE_LOCATION_TABLE = "create table "
            + TABLE_LOCATION                    + "("
            + COLUMN_ID                         + " integer primary key autoincrement, "
            + LOCATION_ID                       + " integer, "
            + NAME                              + " text, "
            + STREET                            + " text, "
            + CITY                              + " text, "
            + REGION                            + " text, "
            + COUNTRY                           + " text, "
            + POSTAL_CODE                       + " text, "
            + PHONE                             + " text); ";

    private static final String DATABASE_CREATE_VISIBILITY_TABLE = "create table "
            + TABLE_VISIBILITY                  + "("
            + COLUMN_ID                         + " integer primary key autoincrement, "
            + VISIBILITY_ID                     + " integer, "
            + SITE_ID                           + " integer, "
            + UNIT_ID                           + " integer, "
            + NAME                              + " text, "
            + TYPE                              + " text, "
            + ACTIVE                            + " boolean, "
			+ UNIT_LOCAL_ID                     + " integer); ";

    public static final String DATABASE_CREATE_METRIC_TABLE = "create table "
            + TABLE_METRIC                      + "("
            + COLUMN_ID                         + " integer primary key autoincrement, "
            + METRIC_ID                         + " integer, "
            + UNIT_ID                           + " integer, "
            + PROFILE_ID                        + " integer, "
            + SEQUENCE                          + " integer, "
            + VALUE                             + " integer, "
            + SELECTED                          + " boolean, "
            + ABBREVIATION                      + " text, "
            + NAME                              + " text); ";


    public static final String DATABASE_CREATE_ATTRIBUTE_TABLE = "create table "
            + TABLE_ATTRIBUTE                   + "("
            + COLUMN_ID                         + " integer primary key autoincrement, "
            + ATTRIBUTE_ID                      + " integer NOT NULL, "
            + NAME                              + " text NOT NULL, "
            + TYPE                              + " text NOT NULL, "
            + INDUSTRY_ID                       + " integer NOT NULL, "
            + VALUE                             + " text);";

    public static final String DATABASE_CREATE_UNIT_ATTRIBUTE_TABLE = "create table "
            + TABLE_UNIT_ATTRIBUTE              + "("
            + COLUMN_ID                         + " integer primary key autoincrement, "
            + ATTRIBUTE_ID                      + " integer NOT NULL, "
            + UNIT_ID                           + " integer NOT NULL, "
            + NAME                              + " text NOT NULL, "
            + TYPE                              + " text NOT NULL, "
            + VALUE                             + " text);";

    public static synchronized DatabaseHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseHelper(context.getApplicationContext());
        }
        return instance;
    }

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE_SITE_TABLE);
        database.execSQL(DATABASE_CREATE_UNIT_TABLE);
        database.execSQL(DATABASE_CREATE_IMAGE_TABLE);
        database.execSQL(DATABASE_CREATE_FILTER_TABLE);
        database.execSQL(DATABASE_CREATE_LOCATION_TABLE);
        database.execSQL(DATABASE_CREATE_VISIBILITY_TABLE);
        database.execSQL(DATABASE_CREATE_METRIC_TABLE);
        database.execSQL(DATABASE_CREATE_ATTRIBUTE_TABLE);
        database.execSQL(DATABASE_CREATE_UNIT_ATTRIBUTE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        if (oldVersion < newVersion && newVersion == 8){
            database.execSQL(DATABASE_CREATE_ATTRIBUTE_TABLE);
            database.execSQL(DATABASE_CREATE_UNIT_ATTRIBUTE_TABLE);
            database.execSQL("ALTER TABLE " + TABLE_UNIT + " ADD COLUMN " + INDUSTRY_ID + " integer;");
        }
    }
}