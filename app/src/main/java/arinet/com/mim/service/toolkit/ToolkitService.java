package arinet.com.mim.service.toolkit;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import arinet.com.mim.model.Image;

/**
 * Created by kevinstueber on 7/29/14.
 */
public class ToolkitService {

    private static ToolkitService instance = null;

    public static ToolkitService getInstance() {
        if(instance == null) {
            instance = new ToolkitService();
        }
        return instance;
    }

    protected ToolkitService(){}

    public void makeToast(Context context, String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public float convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public void copyfile(String srFile, String dtFile){
        try{
            File f1 = new File(srFile);
            File f2 = new File(dtFile);
            InputStream in = new FileInputStream(f1);

            OutputStream out = new FileOutputStream(f2);

            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0){
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
        catch(FileNotFoundException ex){
            System.out.println(ex.getMessage() + " in the specified directory.");
            System.exit(0);
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    public String makeImageFileName(String unitId, boolean withDir){
        Date now = new Date();
        String nowstring = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(now);
        if (withDir){
            return Environment.getExternalStorageDirectory() + File.separator + "MIM" + File.separator + unitId + "_" + nowstring;
        }
        else{
            return unitId + "_" + nowstring;
        }
    }

    public Bitmap decodeSampledBitmap(InputStream inputStream, String path) {

        int reqWidth;
        int reqHeight;
        int orientation;

        try {
            byte[] byteArr = IOUtils.toByteArray(inputStream);

            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(byteArr, 0, byteArr.length, options);

            boolean isLandscape = false;
            if (options.outHeight < options.outWidth){
                isLandscape = true;
            }

            double ratio = 1;

            if (isLandscape){
                if (options.outWidth > 1600){
                    ratio = (double)1600/(double)options.outWidth;
                }

                reqWidth = 1600;
                reqHeight = (int)(ratio*options.outHeight);
            }
            else{
                if (options.outHeight > 1600) {
                    ratio = (double)1600/(double)options.outHeight;
                }

                reqWidth = (int)(ratio*options.outWidth);
                reqHeight = 1600;
            }

            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inJustDecodeBounds = false;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;


            Bitmap bmp = BitmapFactory.decodeByteArray(byteArr, 0, byteArr.length, options);
            Bitmap bitmap = null;

            ExifInterface exifReader = new ExifInterface(path);
            orientation = exifReader.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                bitmap = RotateBitmap(bmp, 90);
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                bitmap = RotateBitmap(bmp, 180);
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                bitmap = RotateBitmap(bmp, 270);
            } else {
                bitmap = RotateBitmap(bmp, 360);
            }

            return bitmap;

        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    public static Bitmap RotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public ProgressDialog makeDeterminateDialog(Context context, String title, String message){
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage(message);
        dialog.setTitle(title);
        dialog.setIndeterminate(false);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setProgressNumberFormat(null);
        dialog.setProgressPercentFormat(null);
        return dialog;
    }

    public ProgressDialog makeIndeterminateDialog(Context context, String title, String message){
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage(message);
        dialog.setTitle(title);
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setProgressNumberFormat(null);
        dialog.setProgressPercentFormat(null);
        return dialog;
    }

    public ArrayList<Image> sortImageArray(ArrayList<Image> images){
        Image primary = null;

        for (Image image : images){
            if (image.isPrimary()){
                primary = image;
                images.remove(image);
                break;
            }
        }

        Collections.sort(images);

        ArrayList<Image>evenMoreSortedImages = new ArrayList<Image>();

        if (primary != null){
            evenMoreSortedImages.add(primary);
        }

        for (Image image : images){
            evenMoreSortedImages.add(image);
        }

        return evenMoreSortedImages ;
    }

    public String formatDateStringForDisplay(String originalString){
        if (originalString.length() > 0){
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            try{
                Date date = sdf.parse(originalString);

                SimpleDateFormat sdf2 = new SimpleDateFormat("MM-dd-yyyy");
                return sdf2.format(date);
            }
            catch (ParseException e){
                e.printStackTrace();
            }
        }
        return originalString;
    }

    public static boolean isInteger(String str){
        try{
            Integer i = Integer.parseInt(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

    public static boolean isDouble(String str){
        try{
            Double i = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }
}
