package arinet.com.mim.service.image;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeoutException;

import arinet.com.mim.R;
import arinet.com.mim.model.Image;
import arinet.com.mim.model.Unit;
import arinet.com.mim.service.connection.ConnectionService;
import arinet.com.mim.service.database.DatabaseService;
import arinet.com.mim.service.json.JsonParser;
import arinet.com.mim.service.site.SiteService;
import arinet.com.mim.service.unit.UnitService;

/**
 * Created by kevinstueber on 8/25/14.
 */
public class ImageRequestService extends IntentService {

    public static final String NOTIFICATION = "arinet.com.mim.service.receiver";
    private int result = Activity.RESULT_CANCELED;
    private String sender = "image_request_service";

    public static final String SENDER = "sender";
    public static final String RESULT = "result";
    public static final String POST = "post";
    public static final String PROGRESS = "progress";
    public static final String DOWNLOADED = "downloaded";
    public static final String TOTAL = "total";
    public static final String PUT = "put";
    public static final String DELETE = "delete";
    public static final String LOCAL_PATH = "local_path";
    public static final String IMAGE_ID = "image_id";
    public static final String OFFLINE_REQUEST = "offline_request";
    public static final String CODE = "code";

    public static final int OKAY = 200;
    public static final int TIMEOUT = 408;
    public static final int BAD_REQUEST = 400;
    public static final int EXCEPTION = 500;
    public static final int CONNECTION_CLOSED = 600;
    public static final int NO_NETWORK_CONNECTION = 700;

    public ImageRequestService() {
        super("ImageRequestService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        boolean isPost = intent.getBooleanExtra(POST, false);
        boolean isPut = intent.getBooleanExtra(PUT, false);
        boolean isDelete = intent.getBooleanExtra(DELETE, false);
        boolean offlineRequest = intent.getBooleanExtra(OFFLINE_REQUEST, false);

        if (isPost){
            if (ConnectionService.getInstance().isOnline(this.getApplicationContext())) {
                String localPath = intent.getStringExtra(LOCAL_PATH);
                if (localPath != null) {
                    Image image = ImageService.getInstance().getImageWithLocalPath(this.getApplicationContext(), localPath);
                    if (image != null) {
                        postImage(image, offlineRequest);
                    } else {
                        publishPost(Activity.RESULT_CANCELED, offlineRequest, BAD_REQUEST);
                    }
                } else {
                    publishPost(Activity.RESULT_CANCELED, offlineRequest, BAD_REQUEST);
                }
            }
            else{
                publishPost(Activity.RESULT_CANCELED, offlineRequest, NO_NETWORK_CONNECTION);
            }
        }
        else if (isPut){
            if (ConnectionService.getInstance().isOnline(this.getApplicationContext())) {
                int imageId = intent.getIntExtra(IMAGE_ID, -1);
                if (imageId > 0) {
                    Image image = ImageService.getInstance().getImageWithImageId(this.getApplicationContext(), imageId);
                    if (image != null) {
                        putImage(image, offlineRequest);
                    } else {
                        publishPut(result, offlineRequest, BAD_REQUEST);
                    }
                } else {
                    publishPut(result, offlineRequest, BAD_REQUEST);
                }
            }
            else{
                publishPut(result, offlineRequest, NO_NETWORK_CONNECTION);
            }
        }
        else if (isDelete){
            if (ConnectionService.getInstance().isOnline(this.getApplicationContext())) {
                int imageId = intent.getIntExtra(IMAGE_ID, -1);
                if (imageId > 0){
                    Image image = ImageService.getInstance().getImageWithImageId(this.getApplicationContext(), imageId);
                    if (image != null){
                        deleteImage(image);
                    }
                    else{
                        publishDelete(result, BAD_REQUEST);
                    }
                }
                else {
                    publishDelete(result, BAD_REQUEST);
                }
            }
            else{
                publishDelete(result, NO_NETWORK_CONNECTION);
            }
        }

    }

    private void postImage(final Image image, final boolean offlineRequest){
        String token = SiteService.getInstance().getSite(this.getApplicationContext()).getToken();
        String urlString = this.getResources().getString(R.string.base_url) + "Upload/" + image.getUnitId();

        Ion.with(this.getApplicationContext()).load("POST", urlString)
                .setHeader("Authorization", token)
                .setTimeout(60000)
                .setMultipartFile("image.jpeg", new File(image.getLocalPath()))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        int statusCode = OKAY;
                        if (e != null){
                            statusCode = EXCEPTION;

                            if (e.getClass() == TimeoutException.class) {
                                statusCode = TIMEOUT;
                            } else if (e.getClass() == HttpHostConnectException.class) {
                                statusCode = CONNECTION_CLOSED;
                            }

                            publishPost(Activity.RESULT_CANCELED, offlineRequest, statusCode);
                        }
                        else if (result != null){

                            boolean success = true;

                            Image updatedImage = JsonParser.parseImageResult(result.toString());

                            if (updatedImage != null && updatedImage.getUnitId() == image.getUnitId()) {
                                Unit unit = UnitService.getInstance().getUnitWithUnitId(getApplicationContext(), image.getUnitId());
                                if (unit != null) {
                                    updatedImage.setUnitId(unit.getUnitId());
                                    updatedImage.setUnitLocalId(unit.getLocalId());
                                    if (ImageService.getInstance().addImage(getApplicationContext(), updatedImage) > 0) {
                                        ImageService.getInstance().deleteLocalImage(getApplicationContext(), image);
                                    } else {
                                        success = false;
                                    }
                                } else {
                                    success = false;
                                }
                            } else {
                                success = false;
                            }

                            publishPost(success ? Activity.RESULT_OK : Activity.RESULT_CANCELED, offlineRequest, EXCEPTION);
                        }
                    }
                });
    }

    private void putImage(Image image, boolean offlineRequest){
        String token = SiteService.getInstance().getSite(this.getApplicationContext()).getToken();
        String urlString = this.getResources().getString(R.string.base_url) + "Images?id=" + image.getImageId();
        int statusCode = OKAY;

        if (token.length() > 0 && image != null){
            try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpPut httpput = new HttpPut(urlString);
                httpput.setHeader("Authorization", token);
                httpput.setEntity(new StringEntity(jObjectFromImage(image).toString()));
                httpput.setHeader("Accept", "application/json");
                httpput.setHeader("Content-type", "application/json");

                HttpResponse response = httpclient.execute(httpput);

                statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == OKAY){
                    result = Activity.RESULT_OK;
                    image.setUpdated(false);
                    DatabaseService.getInstance(getApplicationContext()).updateImage(image);
                }
            }
            catch(UnsupportedEncodingException e){
                e.printStackTrace();
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (HttpHostConnectException e){
                e.printStackTrace();
                statusCode = TIMEOUT;
            } catch (UnknownHostException e){
                e.printStackTrace();
                statusCode = TIMEOUT;
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                publishPut(result, offlineRequest, statusCode);
            }
        }
    }

    private JSONObject jObjectFromImage(Image image){
        try {
            JSONObject jObjectUnit = new JSONObject();
            jObjectUnit.put("Sequence", image.getSequence());
            jObjectUnit.put("IsPrimary", image.isPrimary());

            return jObjectUnit;
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return null;
    }


    private void deleteImage(final Image image){
        String token = SiteService.getInstance().getSite(this.getApplicationContext()).getToken();
        String urlString = this.getResources().getString(R.string.base_url) + "Images?imageId=" + image.getImageId() + "&unitId=" + image.getUnitId();

        Ion.with(this.getApplicationContext()).load("DELETE", urlString)
                .setHeader("Authorization", token)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        int statusCode = OKAY;
                        int requestResult = Activity.RESULT_OK;
                        if (e != null){
                            requestResult = Activity.RESULT_CANCELED;
                            statusCode = EXCEPTION;

                            if (e.getClass() == TimeoutException.class) {
                                statusCode = TIMEOUT;
                            } else if (e.getClass() == HttpHostConnectException.class) {
                                statusCode = CONNECTION_CLOSED;
                            }
                        }
                        else if (result != null){
                            DatabaseService.getInstance(getApplicationContext()).deleteImageWithImageId(image.getImageId());
                        }
                        publishDelete(requestResult, statusCode);
                    }
                });
    }

    private void publishPost(int resultStatus, boolean offlineRequest, int statusCode){
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, resultStatus);
        intent.putExtra(SENDER, sender);
        intent.putExtra(POST, true);
        intent.putExtra(OFFLINE_REQUEST, offlineRequest);
        intent.putExtra(CODE, statusCode);
        sendBroadcast(intent);
    }

    private void publishPut(int resultStatus, boolean offlineRequest, int statusCode){
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, resultStatus);
        intent.putExtra(SENDER, sender);
        intent.putExtra(PUT, true);
        intent.putExtra(OFFLINE_REQUEST, offlineRequest);
        intent.putExtra(CODE, statusCode);
        sendBroadcast(intent);
    }

    private void publishDelete(int resultStatus, int statusCode){
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, resultStatus);
        intent.putExtra(SENDER, sender);
        intent.putExtra(DELETE, true);
        intent.putExtra(CODE, statusCode);
        sendBroadcast(intent);
    }
}
