package arinet.com.mim.service.visibility;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import arinet.com.mim.R;
import arinet.com.mim.model.Site;
import arinet.com.mim.model.Visibility;
import arinet.com.mim.service.database.DatabaseService;
import arinet.com.mim.service.json.JsonParser;
import arinet.com.mim.service.site.SiteService;

/**
 * Created by kevinstueber on 8/6/14.
 */
public class VisibilityRequestService extends IntentService {

    public static final String NOTIFICATION = "arinet.com.mim.service.receiver";
    private int result = Activity.RESULT_CANCELED;
    private String sender = "visibility_request_service";

    public static final String SENDER = "sender";
    public static final String RESULT = "result";
    public static final String GET = "get";

    public VisibilityRequestService() {
        super("VisibilityRequestService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        boolean isGet = intent.getBooleanExtra(GET, false);

        if (isGet){
            getVisibilities();
        }
    }

    private void getVisibilities(){
        Site site = SiteService.getInstance().getSite(this.getApplicationContext());

        if (site != null){

            String token = site.getToken();
            int siteId = site.getSiteId();
            String urlString = this.getResources().getString(R.string.base_url) + "Visibilities";

            if (token.length() > 0){
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpGet httpget = new HttpGet(urlString);
                    httpget.setHeader("Authorization", token);
                    httpget.setHeader("Accept", "application/json");
                    httpget.setHeader("Content-type", "application/json");

                    HttpResponse response = httpclient.execute(httpget);

                    int statusCode = response.getStatusLine().getStatusCode();

                    if (statusCode == 200){
                        ArrayList<Visibility> visibilities = JsonParser.parseVisibilityResult(response, siteId, 0);

                        if (DatabaseService.getInstance(this.getApplicationContext()).deleteVisibilitiesForSite(site)){
                            DatabaseService.getInstance(this.getApplicationContext()).insertVisibilities(visibilities);
                        }
                        result = Activity.RESULT_OK;
                        publishVisibilityResults(result);
                    }
                    else{
                        //publishError(result, statusCode);
                    }
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                    publishVisibilityResults(result);
                }
                catch (IOException e) {
                    e.printStackTrace();
                    publishVisibilityResults(result);
                }
            }
        }
        else{
            publishVisibilityResults(result);
        }
    }

    private void publishVisibilityResults(int result){

    }
}
