package arinet.com.mim.service.unitAttribute;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import arinet.com.mim.R;
import arinet.com.mim.model.Attribute;
import arinet.com.mim.model.Site;
import arinet.com.mim.service.database.DatabaseService;
import arinet.com.mim.service.json.JsonParser;
import arinet.com.mim.service.site.SiteService;

/**
 * Created by kevinstueber on 10/22/14.
 */
public class UnitAttributeRequestService extends IntentService {

    public static final String NOTIFICATION = "arinet.com.mim.service.receiver";
    private int result = Activity.RESULT_CANCELED;
    private String sender = "unit_attribute_request_service";

    public static final String SENDER = "sender";
    public static final String RESULT = "result";
    public static final String GET = "get";
    public static final String UNIT_ID = "unitId";

    public UnitAttributeRequestService() {
        super("UnitAttributeRequestService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Boolean getUnitAttributes = intent.getBooleanExtra(GET, false);
        int unitId = intent.getIntExtra(UNIT_ID, 0);

        if (getUnitAttributes) {
            requestUnitAttributesForUnit(unitId);
        }
    }

    private void requestUnitAttributesForUnit(int unitId){
        Site site = SiteService.getInstance().getSite(this.getApplicationContext());
        String urlString = this.getResources().getString(R.string.base_url) + "Attribute?id=" + unitId;

        if (site != null && unitId > 0){
            String token = site.getToken();
            if (token.length() > 0){
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpGet httpGet = new HttpGet(urlString);
                    httpGet.setHeader("Authorization", token);

                    HttpResponse response = httpclient.execute(httpGet);

                    System.out.println(EntityUtils.toString(response.getEntity()));

                    //ArrayList<Attribute> attributes = JsonParser.parseUnitAttributeResult(response);
                    /*
                    DatabaseService.getInstance(this.getApplicationContext()).deleteAttributes();
                    DatabaseService.getInstance(getApplicationContext()).insertAttributes(attributes);
                    */
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
