package arinet.com.mim.service.yearMakeModel;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;

import com.google.gson.JsonArray;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

import arinet.com.mim.R;
import arinet.com.mim.model.Industry;
import arinet.com.mim.model.Make;
import arinet.com.mim.model.StyleType;
import arinet.com.mim.model.Type;
import arinet.com.mim.service.site.SiteService;

/**
 * Created by kevinstueber on 9/3/14.
 */
public class YearMakeModelService extends IntentService {

    public static final String NOTIFICATION = "arinet.com.mim.service.receiver";
    private int result = Activity.RESULT_CANCELED;
    private String sender = "year_make_model_request_service";

    public static final String SENDER = "sender";
    public static final String RESULT = "result";

    private String token;

    public YearMakeModelService() {
        super("YearMakeModelService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        getIndustries();
    }

    private void getIndustries(){
        token = SiteService.getInstance().getSite(this.getApplicationContext()).getToken();

        if (token.length() > 0){
            String urlString = this.getResources().getString(R.string.base_url) + "Industries";

            Ion.with(getApplicationContext())
                    .load(urlString)
                    .setHeader("Authorization", token)
                    .asJsonArray()
                    .setCallback(new FutureCallback<JsonArray>() {
                        @Override
                        public void onCompleted(Exception e, JsonArray resultObj) {
                            if (e != null) {
                                e.printStackTrace();
                            } else {
                                //Delete industries, makes, types, styletypes in db
                                //System.out.println(resultObj);
                                ArrayList<Industry>industries = JsonParser.parseIndustryResults(resultObj);
                                //insert industries
                                int counter = 0;
                                for (Industry industry : industries){
                                    ++counter;
                                    if (counter == industries.size()){
                                        System.out.println("last industry");
                                    }

                                    getMakes(industry.getIndustryId(), counter == industries.size());
                                }
                            }
                        }
                    });
        }
    }

    private void getMakes(final int industryId, final boolean isLast){
        if (token.length() > 0){
            String urlString = this.getResources().getString(R.string.base_url) + "Manufacturers/"+industryId;

            Ion.with(getApplicationContext())
                    .load(urlString)
                    .setHeader("Authorization", token)
                    .asJsonArray()
                    .setCallback(new FutureCallback<JsonArray>() {
                        @Override
                        public void onCompleted(Exception e, JsonArray resultObj) {
                            if (e != null) {
                                e.printStackTrace();
                            } else {
                                //System.out.println("MAKES: " + resultObj);
                                ArrayList<Make>makes = JsonParser.parseMakeResults(resultObj, industryId);
                                //insert Makes into db
                                int counter = 0;
                                for (Make make : makes){
                                    ++counter;
                                    getTypes(make.getMakeId(), (isLast && counter == makes.size()));
                                }
                            }
                        }
                    });
        }
    }

    private void getTypes(final int makeId, final boolean isLast){
         if (token.length() > 0){
            String urlString = this.getResources().getString(R.string.base_url) + "EquipmentTypes/"+makeId;

            Ion.with(getApplicationContext())
                    .load(urlString)
                    .setHeader("Authorization", token)
                    .asJsonArray()
                    .setCallback(new FutureCallback<JsonArray>() {
                        @Override
                        public void onCompleted(Exception e, JsonArray resultObj) {
                            if (e != null) {
                                e.printStackTrace();
                            } else {
                                ArrayList<Type> types = JsonParser.parseTypeResults(resultObj, makeId);

                                int counter = 0;
                                for (Type type : types){
                                    ++counter;
                                    getStyleTypes(type.getTypeId(), (isLast && counter == types.size()));
                                }
                            }
                        }
                    });
        }
    }

    private void getStyleTypes(final int typeId, final boolean isLast){
        if (token.length() > 0){
            String urlString = this.getResources().getString(R.string.base_url) + "TypeStyles/"+typeId;
            Ion.with(getApplicationContext())
                    .load(urlString)
                    .setHeader("Authorization", token)
                    .setTimeout(10 * 10 * 6000)
                    .asJsonArray()
                    .setCallback(new FutureCallback<JsonArray>() {
                        @Override
                        public void onCompleted(Exception e, JsonArray resultObj) {
                            if (e != null) {
                                e.printStackTrace();
                            } else {
                                //System.out.println(resultObj);

                                ArrayList<StyleType> styleTypes = JsonParser.parseStyleTypeResults(resultObj, typeId);
                                if (isLast) {
                                    result = Activity.RESULT_OK;
                                    publishComplete(result);
                                }
                            }
                        }
                    });
        }
    }

    private void publishComplete(int result) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, result);
        intent.putExtra(SENDER, sender);
        sendBroadcast(intent);
    }
}
