package arinet.com.mim.service.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;

import java.util.ArrayList;

import arinet.com.mim.model.Attribute;
import arinet.com.mim.model.Filter;
import arinet.com.mim.model.Image;
import arinet.com.mim.model.Location;
import arinet.com.mim.model.Metric;
import arinet.com.mim.model.Site;
import arinet.com.mim.model.Unit;
import arinet.com.mim.model.UnitAttribute;
import arinet.com.mim.model.Visibility;

/**
 * Created by kevinstueber on 7/28/14.
 */
public class DatabaseService {

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private Context context;

    private static DatabaseService instance = null;

    public static DatabaseService getInstance(Context context) {
        if(instance == null) {
            instance = new DatabaseService(context);
        }
        return instance;
    }

    private String[] siteColumns = {
            DatabaseHelper.COLUMN_ID,
            DatabaseHelper.SITE_ID,
            DatabaseHelper.ACCOUNT_GUID,
            DatabaseHelper.ORGANIZATION_GUID,
            DatabaseHelper.EXPIRES,
            DatabaseHelper.EMAIL,
            DatabaseHelper.TOKEN,
            DatabaseHelper.CURRENT
    };

    public DatabaseService(Context context) {
        dbHelper = DatabaseHelper.getInstance(context);
        this.context = context;
        open();
    }

    public void open() {
        try{
            database = dbHelper.getWritableDatabase();
        }
        catch(SQLiteDatabaseLockedException e){
            e.printStackTrace();
        }
    }


    /* Site */

    public long insertSite(Site site) {

        ContentValues values = new ContentValues();

        values.put(DatabaseHelper.SITE_ID, site.getSiteId());
        values.put(DatabaseHelper.ACCOUNT_GUID, site.getAccountGuid());
        values.put(DatabaseHelper.ORGANIZATION_GUID, site.getOrganizationGuid());
        values.put(DatabaseHelper.EXPIRES, site.getExpires());
        values.put(DatabaseHelper.EMAIL, site.getEmail());
        values.put(DatabaseHelper.TOKEN, site.getToken());
        values.put(DatabaseHelper.CURRENT, site.isCurrent());

        long id = database.insert(DatabaseHelper.TABLE_SITE, null, values);

        return id;
    }

    public Site selectSite() {
        

        Site site = null;

        database.beginTransaction();

        Cursor cursor = database.query(DatabaseHelper.TABLE_SITE, siteColumns, null, null, null, null, null);

        try{
            if (cursor != null && cursor.moveToNext()){
                site = cursorToSite(cursor);
            }
        } finally {
            if (cursor != null){
                cursor.close();
            }
            database.endTransaction();
        }

        

        return site;
    }

    public boolean deleteSites(){
        
        database.execSQL("delete from " + DatabaseHelper.TABLE_SITE + ";");
        
        return true;
    }

    public Site cursorToSite(Cursor cursor){
        Site site = new Site();

        site.setSiteId(cursor.getInt(1));
        site.setAccountGuid(cursor.getString(2));
        site.setOrganizationGuid(cursor.getString(3));
        site.setExpires(cursor.getString(4));
        site.setEmail(cursor.getString(5));
        site.setToken(cursor.getString(6));

        boolean isCurrent = cursor.getInt(7) > 0;
        site.setCurrent(isCurrent);

        return site;
    }


    /* Unit */

    public long insertUnit(Unit unit) {
	    long id = 0;

	    database.beginTransaction();

        try{
	        ContentValues values = getValuesForUnit(unit);
	        id = database.insert(DatabaseHelper.TABLE_UNIT, null, values);
	        database.setTransactionSuccessful();
        }
        finally {
	        database.endTransaction();
        }

        return id;
    }

	public int updateUnit(Unit unit){
		int val = 0;

		database.beginTransaction();

		try{
			ContentValues values = getValuesForUnit(unit);

			String q = "id=" + unit.getLocalId();
			val = database.update(DatabaseHelper.TABLE_UNIT, values, q, null);

			database.setTransactionSuccessful();
		}
		finally {
			database.endTransaction();
		}

        return val;
    }

	private ContentValues getValuesForUnit(Unit unit){
		ContentValues values = new ContentValues();

		values.put(DatabaseHelper.UNIT_ID, unit.getUnitId());
		values.put(DatabaseHelper.YEAR, unit.getYear());
		values.put(DatabaseHelper.MAKE_ID, unit.getMakeId());
		values.put(DatabaseHelper.PRICE, unit.getPrice());
		values.put(DatabaseHelper.EQUIPMENT_TYPE_ID, unit.getEquipmentTypeId());
		values.put(DatabaseHelper.STYLE_ID, unit.getStyleId());
		values.put(DatabaseHelper.LINKED_PRODUCT_ID, unit.getLinkedProductId());
		values.put(DatabaseHelper.HITS_SEVEN, unit.getHitsSeven());
		values.put(DatabaseHelper.HITS_THIRTY, unit.getHitsThirty());
		values.put(DatabaseHelper.HITS_NINETY, unit.getHitsNintey());
		values.put(DatabaseHelper.HITS_THREE_SIXTY_FIVE, unit.getHitsThreeSixFive());
		values.put(DatabaseHelper.DMS_PRICE, unit.getDmsPrice());
		values.put(DatabaseHelper.SITE_ID, unit.getSiteId());
		values.put(DatabaseHelper.LOCATION_ID, unit.getLocationId());
		values.put(DatabaseHelper.TITLE_STATUS, unit.getTitleStatus());
		values.put(DatabaseHelper.CONDITION, unit.getCondition());
		values.put(DatabaseHelper.PRICE_LABEL, unit.getPriceLabel());
		values.put(DatabaseHelper.USAGE_STATUS, unit.getUsageStatus());
		values.put(DatabaseHelper.SALE_PRICE, unit.getSalePrice());
		values.put(DatabaseHelper.CALL_FOR_PRICE, unit.isCallForPrice());
		values.put(DatabaseHelper.PUBLISHED_ON_WEB, unit.isPublishedOnWeb());
		values.put(DatabaseHelper.USE_STOCK_IMAGES, unit.isUseStockImages());
		values.put(DatabaseHelper.ON_SALE, unit.isOnSale());
		values.put(DatabaseHelper.DISPLAY_IN_SHOWCASE_COUNT, unit.isDisplayInShowcaseCount());
		values.put(DatabaseHelper.SALE_ACTIVE, unit.isSaleActive());
		values.put(DatabaseHelper.NEW, unit.isNew());
		values.put(DatabaseHelper.UPDATED, unit.isUpdated());
		values.put(DatabaseHelper.DELETE, unit.isDelete());
		values.put(DatabaseHelper.MODEL, unit.getModel());
		values.put(DatabaseHelper.STOCK_NUMBER, unit.getStockNumber());
		values.put(DatabaseHelper.VIN, unit.getVin());
		values.put(DatabaseHelper.GLOBAL_IDENTIFIER, unit.getGlobalIdentifier());
		values.put(DatabaseHelper.INVENTORY_STATUS_VALUE, unit.getInventoryStatusValue());
		values.put(DatabaseHelper.MAKE, unit.getMake());
		values.put(DatabaseHelper.DESCRIPTION, unit.getDescription());
		values.put(DatabaseHelper.EQUIPMENT_TYPE, unit.getEquipmentType());
		values.put(DatabaseHelper.LINKED_PRODUCT_NAME, unit.getLinkedProductName());
		values.put(DatabaseHelper.PRIMARY_COLOR, unit.getPrimaryColor());
		values.put(DatabaseHelper.TRIM_COLOR,unit.getTrimColor());
		values.put(DatabaseHelper.LAST_UPDATE, unit.getLastUpdate());
		values.put(DatabaseHelper.SALE_TYPE, unit.getSaleType());
		values.put(DatabaseHelper.SALE_START_DATE, unit.getSaleStartDate());
		values.put(DatabaseHelper.SALE_END_DATE, unit.getSaleEndDate());
		values.put(DatabaseHelper.UNFINALIZED_UNIT_ID, unit.getUnfinalizedUnitId());
		values.put(DatabaseHelper.PRODUCT_RESULT, unit.isProductResult());
		values.put(DatabaseHelper.URL, unit.getUrl());
		values.put(DatabaseHelper.BEST_PRICE, unit.getBestPrice());
        values.put(DatabaseHelper.INDUSTRY_ID, unit.getIndustryId());

		return values;
	}


    public Unit selectUnitWithUnitId(int unitId){
        Unit unit = null;

        String queryStr = new String("SELECT * FROM " + DatabaseHelper.TABLE_UNIT + " WHERE " + DatabaseHelper.UNIT_ID + " = ?");

        String queryValue = new String(""+unitId);

        Cursor cursor = database.rawQuery(queryStr, new String[] { queryValue });

        try{
            if (cursor != null && cursor.moveToFirst()){
                unit = cursorToUnit(cursor);
            }
        } finally {
            if (cursor != null){
                cursor.close();
            }
        }

        return unit;
    }

    public Unit selectUnitWithLocalUnitId(long localUnitId){
        Unit unit = null;

        String queryStr = new String("SELECT * FROM " + DatabaseHelper.TABLE_UNIT + " WHERE " + DatabaseHelper.COLUMN_ID + " = ?");

        String queryValue = new String(""+localUnitId);

        Cursor cursor = database.rawQuery(queryStr, new String[] { queryValue });

        try{
            if (cursor != null && cursor.moveToFirst()){
                unit = cursorToUnit(cursor);
            }
        } finally {
            if (cursor != null){
                cursor.close();
            }
        }

        return unit;
    }

    public ArrayList<Unit> selectAllUnits() {

        ArrayList<Unit> units = new ArrayList<Unit>();

        String queryStr = new String("SELECT * FROM " + DatabaseHelper.TABLE_UNIT + " WHERE " + DatabaseHelper.DELETE + "=0 AND " + DatabaseHelper.PRODUCT_RESULT + "=0;");

        Cursor cursor = database.rawQuery(queryStr, null);

        try{
            if (cursor != null){
                while (cursor.moveToNext()) {
                    Unit unit = cursorToUnit(cursor);
                    units.add(unit);
                }
            }
        } finally {
            if (cursor != null){
                cursor.close();
            }
        }

        return units;
    }

    public ArrayList<Unit> selectAllProductResults() {


        ArrayList<Unit> units = new ArrayList<Unit>();

        String queryStr = new String("SELECT * FROM " + DatabaseHelper.TABLE_UNIT + " WHERE " + DatabaseHelper.PRODUCT_RESULT + "=1;");

        Cursor cursor = database.rawQuery(queryStr, null);

        try{
            if (cursor != null){
                while (cursor.moveToNext()) {
                    Unit unit = cursorToUnit(cursor);
                    units.add(unit);
                }
            }
        } finally {
            if (cursor != null){
                cursor.close();
            }
        }

        return units;
    }

    public ArrayList<Unit> selectOfflineUnits() {


        ArrayList<Unit> units = new ArrayList<Unit>();

        String queryStr = new String("SELECT * FROM " + DatabaseHelper.TABLE_UNIT + " WHERE " + DatabaseHelper.UPDATED + "=1 OR " + DatabaseHelper.DELETE + "=1 OR " + DatabaseHelper.NEW + "=1 AND " + DatabaseHelper.PRODUCT_RESULT + "=0;");

        Cursor cursor = database.rawQuery(queryStr, null);

        try{
            if (cursor != null){
                while (cursor.moveToNext()) {
                    Unit unit = cursorToUnit(cursor);
                    units.add(unit);
                }
            }
        } finally {
            if (cursor != null){
                cursor.close();
            }
        }

        return units;
    }


    public boolean deleteUnit(Unit unit){
        database.execSQL("delete from " + DatabaseHelper.TABLE_UNIT + " WHERE " + DatabaseHelper.COLUMN_ID + "=" + unit.getLocalId() + ";");
        return true;
    }

    public boolean deleteProductResults(){
        database.execSQL("delete from " + DatabaseHelper.TABLE_UNIT + " WHERE " + DatabaseHelper.PRODUCT_RESULT + "=1;");

        return true;
    }


    private Unit cursorToUnit(Cursor cursor){
        Unit unit = new Unit();

        unit.setLocalId(cursor.getLong(0));
        unit.setUnitId(cursor.getInt(1));
        unit.setYear(cursor.getInt(2));
        unit.setMakeId(cursor.getInt(3));
        unit.setPrice(cursor.getDouble(4));
        unit.setEquipmentTypeId(cursor.getInt(5));
        unit.setStyleId(cursor.getInt(6));
        unit.setLinkedProductId(cursor.getInt(7));


        unit.setHitsSeven(cursor.getInt(8));
        unit.setHitsThirty(cursor.getInt(9));
        unit.setHitsNintey(cursor.getInt(10));
        unit.setHitsThreeSixFive(cursor.getInt(11));

        unit.setDmsPrice(cursor.getInt(12));

        //unit.setSiteId(cursor.getInt("siteId"));
        unit.setLocationId(cursor.getInt(14));
        unit.setTitleStatus(cursor.getInt(15));
        unit.setCondition(cursor.getInt(16));
        unit.setPriceLabel(cursor.getInt(17));
        unit.setUsageStatus(cursor.getInt(18));

        unit.setSalePrice(cursor.getDouble(19));

        boolean callForPrice = cursor.getInt(20) > 0;
        unit.setCallForPrice(callForPrice);

        boolean publishedOnWeb = cursor.getInt(21) > 0;
        unit.setPublishedOnWeb(publishedOnWeb);

        boolean useStockImages = cursor.getInt(22) > 0;
        unit.setUseStockImages(useStockImages);

        boolean onSale = cursor.getInt(23) > 0;
        unit.setOnSale(onSale);

        boolean displayInShowcaseCount = cursor.getInt(24) > 0;
        unit.setDisplayInShowcaseCount(displayInShowcaseCount);

        boolean saleActive = cursor.getInt(25) > 0;
        unit.setSaleActive(saleActive);

        boolean isNew = cursor.getInt(26) > 0;
        unit.setNew(isNew);

        boolean isUpdated = cursor.getInt(27) > 0;
        unit.setUpdated(isUpdated);

        boolean isDelete = cursor.getInt(28) > 0;
        unit.setDelete(isDelete);

        unit.setModel(cursor.getString(29));
        unit.setStockNumber(cursor.getString(30));
        unit.setVin(cursor.getString(31));
        unit.setGlobalIdentifier(cursor.getString(32));
        unit.setInventoryStatusValue(cursor.getString(33));
        unit.setMake(cursor.getString(34));
        unit.setDescription(cursor.getString(35));
        unit.setEquipmentType(cursor.getString(36));
        unit.setLinkedProductName(cursor.getString(37));
        unit.setPrimaryColor(cursor.getString(38));
        unit.setTrimColor(cursor.getString(39));
        unit.setLastUpdate(cursor.getString(40));
        unit.setSaleType(cursor.getString(41));
        unit.setSaleStartDate(cursor.getString(42));
        unit.setSaleEndDate(cursor.getString(43));
        unit.setUnfinalizedUnitId(cursor.getString(44));

        boolean isProductResult = cursor.getInt(45) > 0;
        unit.setProductResult(isProductResult);

        unit.setUrl(cursor.getString(46));
        unit.setBestPrice(cursor.getDouble(47));
        unit.setIndustryId(cursor.getInt(48));

        return unit;
    }


    /* Image */

    public long insertImage(Image image) {
		long id = 0;
	    database.beginTransaction();

	    try {
			ContentValues values = getValuesForImage(image);
			id = database.insert(DatabaseHelper.TABLE_IMAGE, null, values);

		    database.setTransactionSuccessful();
	    }
	    finally {
		    database.endTransaction();
	    }
        return id;
    }

    public void insertImages(ArrayList<Image>images){
        if (images.size() > 0){
	        database.beginTransaction();

	        try {
		        for (int i = 0; i < images.size(); i++) {
			        Image image = images.get(i);

			        ContentValues values = getValuesForImage(image);
			        database.insert(DatabaseHelper.TABLE_IMAGE, null, values);
		        }

		        database.setTransactionSuccessful();
	        }
	        finally {
		        database.endTransaction();
	        }
        }
    }


	public void updateImage(Image image){
		database.beginTransaction();
		try{

			ContentValues values = getValuesForImage(image);
			String q = null;

			if (image.getImageId() > 0){
				q = "image_id=" + image.getImageId();
			}
			else{
				q = "local_path='" + image.getLocalPath() + "'";
			}

			database.update(DatabaseHelper.TABLE_IMAGE, values, q, null);
			database.setTransactionSuccessful();
		}
		finally {
			database.endTransaction();
		}
	}

	private ContentValues getValuesForImage(Image image){
		ContentValues values = new ContentValues();

		values.put(DatabaseHelper.IMAGE_ID, image.getImageId());
		values.put(DatabaseHelper.STOCK_IMAGE_ID, image.getStockImageId());
		values.put(DatabaseHelper.UNIT_ID, image.getUnitId());
		values.put(DatabaseHelper.UNIT_LOCAL_ID, image.getUnitLocalId());
		values.put(DatabaseHelper.SEQUENCE, image.getSequence());
		values.put(DatabaseHelper.IS_PRIMARY, image.isPrimary());
		values.put(DatabaseHelper.NEW, image.isNew());
		values.put(DatabaseHelper.DELETE, image.isDelete());
		values.put(DatabaseHelper.UPDATED, image.isUpdated());
		values.put(DatabaseHelper.LOCATION, image.getLocation());
		values.put(DatabaseHelper.THUMB_LOCATION, image.getThumbLocation());
		values.put(DatabaseHelper.LOCAL_PATH, image.getLocalPath());
		values.put(DatabaseHelper.PRODUCT_RESULT, image.isProductResult());

		return values;
	}


	public ArrayList<Image>selectImagesForUnit(Unit unit){

        ArrayList<Image> images = new ArrayList<Image>();

        if (unit != null){
            String queryStr = new String("SELECT * FROM " + DatabaseHelper.TABLE_IMAGE + " WHERE " + DatabaseHelper.UNIT_LOCAL_ID + " = ? AND " + DatabaseHelper.DELETE + "=0");
            String queryValue = new String(""+unit.getLocalId());

            Cursor cursor = database.rawQuery(queryStr, new String[] { queryValue });

            try{
                if (cursor != null){
                    while (cursor.moveToNext()) {
                        Image image = cursorToImage(cursor);
                        images.add(image);
                    }
                }
            } finally {
                if (cursor != null){
                    cursor.close();
                }
            }
        }

        return images;
    }

    public ArrayList<Image>selectOfflineImagesForUnit(Unit unit){

        ArrayList<Image> images = new ArrayList<Image>();
        if(unit==null)
            return images;

        String queryStr = new String("SELECT * FROM " + DatabaseHelper.TABLE_IMAGE + " WHERE " + DatabaseHelper.UNIT_ID + " = ? AND " + DatabaseHelper.NEW + "=1");
        String queryValue = new String(""+unit.getUnitId());

        Cursor cursor = database.rawQuery(queryStr, new String[] { queryValue });

        try{
            if (cursor != null){
                while (cursor.moveToNext()) {
                    Image image = cursorToImage(cursor);
                    images.add(image);
                }
            }
        } finally {
            if (cursor != null){
                cursor.close();
            }
        }

        return images;
    }

    public ArrayList<Image>selectOfflineImages(){

        ArrayList<Image> images = new ArrayList<Image>();

        String queryStr = new String("SELECT * FROM " + DatabaseHelper.TABLE_IMAGE + " WHERE " + DatabaseHelper.NEW + "=1 OR " + DatabaseHelper.UPDATED + "=1;");

        Cursor cursor = database.rawQuery(queryStr, null);

        try{
            if (cursor != null){
                while (cursor.moveToNext()) {
                    Image image = cursorToImage(cursor);
                    images.add(image);
                }
            }
        } finally {
            if (cursor != null){
                cursor.close();
            }
        }

        return images;
    }

    public Image selectImageWithLocalPath(String localPath){
        String queryStr = new String("SELECT * FROM " + DatabaseHelper.TABLE_IMAGE + " WHERE " + DatabaseHelper.LOCAL_PATH + " = ?");
        String queryValue = new String(localPath);

        Cursor cursor = database.rawQuery(queryStr, new String[] { queryValue });

        Image image = null;

        if (cursor != null){
            if (cursor.moveToFirst()){
                image = cursorToImage(cursor);
            }
        }
        return image;
    }

    public Image selectImageWithImageId(int imageId){
        String queryStr = new String("SELECT * FROM " + DatabaseHelper.TABLE_IMAGE + " WHERE " + DatabaseHelper.IMAGE_ID + " = ?");
        String queryValue = new String(""+imageId);

        Cursor cursor = database.rawQuery(queryStr, new String[] { queryValue });

        Image image = null;

        if (cursor != null){
            if (cursor.moveToFirst()){
                image = cursorToImage(cursor);
            }
        }
        return image;
    }


    public boolean deleteImagesForUnit(Unit unit){
        database.execSQL("delete from " + DatabaseHelper.TABLE_IMAGE + " WHERE " + DatabaseHelper.UNIT_LOCAL_ID + " = " + unit.getLocalId() + " AND " + DatabaseHelper.NEW + " = 0");
        return true;
    }

    public boolean deleteImagesForProductResults(){
        database.execSQL("delete from " + DatabaseHelper.TABLE_IMAGE + " WHERE " + DatabaseHelper.PRODUCT_RESULT + " = 1");
        return true;
    }

    public boolean deleteLocalImage(Image image){
        database.execSQL("delete from " + DatabaseHelper.TABLE_IMAGE + " WHERE " + DatabaseHelper.LOCAL_PATH + " = \"" + image.getLocalPath() + "\"");
        return true;
    }

    public boolean deleteImageWithImageId(int imageId){
        database.execSQL("delete from " + DatabaseHelper.TABLE_IMAGE + " WHERE " + DatabaseHelper.IMAGE_ID + " = \"" + imageId + "\"");
        return true;
    }

    private Image cursorToImage(Cursor cursor){
        Image image = new Image();

        image.setLocalId(cursor.getLong(0));
        image.setImageId(cursor.getInt(1));
        image.setStockImageId(cursor.getInt(2));
        image.setUnitId(cursor.getInt(3));
        image.setUnitLocalId(cursor.getLong(4));
        image.setSequence(cursor.getInt(5));

        boolean isPrimary = cursor.getInt(6) > 0;
        image.setPrimary(isPrimary);

        boolean isNew = cursor.getInt(7) > 0;
        image.setNew(isNew);

        boolean isDelete = cursor.getInt(8) > 0;
        image.setDelete(isDelete);

        boolean isUpdated = cursor.getInt(9) > 0;
        image.setUpdated(isUpdated);

        image.setLocation(cursor.getString(10));
        image.setThumbLocation(cursor.getString(11));
        image.setLocalPath(cursor.getString(12));

        boolean isProductResult = cursor.getInt(13) > 0;
        image.setProductResult(isProductResult);

        return image;
    }


    /* Filter */

    public long insertFilter(Filter filter) {
        ContentValues values = new ContentValues();

        values.put(DatabaseHelper.NAME, filter.getName());
        values.put(DatabaseHelper.COUNT, filter.getCount());
        values.put(DatabaseHelper.QUERY, filter.getQuery());
        values.put(DatabaseHelper.LOCATION, filter.getLocation());
        values.put(DatabaseHelper.STORE_LOCATION_ID, filter.getStoreLocationId());

        return database.insert(DatabaseHelper.TABLE_FILTER, null, values);
    }

    public ArrayList<Filter> selectAllFilters() {

        ArrayList<Filter> filters = new ArrayList<Filter>();

        String queryStr = new String("SELECT * FROM " + DatabaseHelper.TABLE_FILTER);

        Cursor cursor = database.rawQuery(queryStr, null);

        try{
            if (cursor != null){
                while (cursor.moveToNext()) {
                    Filter filter = cursorToFilter(cursor);
                    filters.add(filter);
                }
            }
        } finally {
            if (cursor != null){
                cursor.close();
            }
        }

        return filters;
    }

    public boolean deleteFilters(){
        
        database.execSQL("delete from " + DatabaseHelper.TABLE_FILTER + ";");
        
        return true;
    }

    private Filter cursorToFilter(Cursor cursor){
        Filter filter = new Filter();

        filter.setName(cursor.getString(1));
        filter.setCount(cursor.getInt(2));
        filter.setQuery(cursor.getString(3));
        filter.setLocation(cursor.getString(4));
        filter.setStoreLocationId(cursor.getInt(5));

        return filter;
    }


    /* Attributes */

    public void insertAttributes(ArrayList<Attribute> attributes){
        if (attributes.size() > 0){
            database.beginTransaction();

            try {
                for (int i = 0; i < attributes.size(); i++) {
                    Attribute attribute = attributes.get(i);

                    ContentValues values = getValuesForAttribute(attribute);
                    database.insert(DatabaseHelper.TABLE_ATTRIBUTE, null, values);
                }

                database.setTransactionSuccessful();
            }
            finally {
                database.endTransaction();
            }
        }
    }

    private ContentValues getValuesForAttribute(Attribute attribute){
        ContentValues values = new ContentValues();

        values.put(DatabaseHelper.ATTRIBUTE_ID, attribute.getAttributeId());
        values.put(DatabaseHelper.NAME, attribute.getName());
        values.put(DatabaseHelper.TYPE, attribute.getType());
        values.put(DatabaseHelper.INDUSTRY_ID, attribute.getIndustryId());
        values.put(DatabaseHelper.VALUE, attribute.getValue());

        return values;
    }

    public boolean deleteAttributes(){

        database.execSQL("delete from " + DatabaseHelper.TABLE_ATTRIBUTE + ";");

        return true;
    }

    public ArrayList<Attribute> selectAllAttributes(){
        ArrayList<Attribute> attributes = new ArrayList<Attribute>();

        String queryStr = new String("SELECT * FROM " + DatabaseHelper.TABLE_ATTRIBUTE);

        Cursor cursor = database.rawQuery(queryStr, null);

        try{
            if (cursor != null){
                while (cursor.moveToNext()) {
                    Attribute attribute = cursorToAttribute(cursor);
                    attributes.add(attribute);
                }
            }
        } finally {
            if (cursor != null){
                cursor.close();
            }
        }

        return attributes;
    }

    private Attribute cursorToAttribute(Cursor cursor){
        Attribute attribute = new Attribute();

        attribute.setAttributeId(cursor.getLong(1));
        attribute.setName(cursor.getString(2));
        attribute.setType(cursor.getString(3));
        attribute.setIndustryId(cursor.getInt(4));
        attribute.setValue(cursor.getString(5));

        return attribute;
    }


    /* UnitAttributes */

    public boolean insertUnitAttributes(ArrayList<UnitAttribute> unitAttributes){
        boolean success = false;
        if (unitAttributes.size() > 0){
            database.beginTransaction();

            try {
                for (int i = 0; i < unitAttributes.size(); i++) {
                    UnitAttribute unitAttribute = unitAttributes.get(i);

                    ContentValues values = getValuesForUnitAttribute(unitAttribute);
                    database.insert(DatabaseHelper.TABLE_UNIT_ATTRIBUTE, null, values);
                }
                database.setTransactionSuccessful();
                success = true;
            }
            finally {
                database.endTransaction();
            }
        }
        return success;
    }

    private ContentValues getValuesForUnitAttribute(UnitAttribute unitAttribute){
        ContentValues values = new ContentValues();

        values.put(DatabaseHelper.ATTRIBUTE_ID, unitAttribute.getAttributeId());
        values.put(DatabaseHelper.UNIT_ID, unitAttribute.getUnitId());
        values.put(DatabaseHelper.NAME, unitAttribute.getName());
        values.put(DatabaseHelper.TYPE, unitAttribute.getType());
        values.put(DatabaseHelper.VALUE, unitAttribute.getValue());

        return values;
    }

    public ArrayList<UnitAttribute> selectUnitAttributesForUnit(Unit unit){
        ArrayList<UnitAttribute> attributes = new ArrayList<UnitAttribute>();

        String queryStr = new String("SELECT * FROM " + DatabaseHelper.TABLE_UNIT_ATTRIBUTE + " WHERE " + DatabaseHelper.UNIT_ID + "=" + unit.getUnitId());

        Cursor cursor = database.rawQuery(queryStr, null);

        try{
            if (cursor != null){
                while (cursor.moveToNext()) {
                    UnitAttribute unitAttribute = cursorToUnitAttribute(cursor);
                    attributes.add(unitAttribute);
                }
            }
        } finally {
            if (cursor != null){
                cursor.close();
            }
        }

        return attributes;
    }

    private UnitAttribute cursorToUnitAttribute(Cursor cursor){
        UnitAttribute unitAttribute = new UnitAttribute();

        unitAttribute.setAttributeId(cursor.getLong(1));
        unitAttribute.setUnitId(cursor.getLong(2));
        unitAttribute.setName(cursor.getString(3));
        unitAttribute.setType(cursor.getString(4));
        unitAttribute.setValue(cursor.getString(5));



        return unitAttribute;
    }

    public boolean deleteUnitAttributesForUnit(Unit unit){
        database.execSQL("delete from " + DatabaseHelper.TABLE_UNIT_ATTRIBUTE + " where " + DatabaseHelper.UNIT_ID + "=" + unit.getUnitId());
        return true;
    }

    /* LOCATION */

    public boolean insertLocations(ArrayList<Location>locations){
        boolean success = false;
        if (locations.size() > 0){
            database.beginTransaction();

            try {
                for (int i = 0; i < locations.size(); i++) {
                    Location location = locations.get(i);

                    ContentValues values = getValuesForLocation(location);
                    database.insert(DatabaseHelper.TABLE_LOCATION, null, values);
                }
                database.setTransactionSuccessful();
                success = true;
            }
            finally {
                database.endTransaction();
            }
        }
        return success;
    }

    private ContentValues getValuesForLocation(Location location){
        ContentValues values = new ContentValues();

        values.put(DatabaseHelper.LOCATION_ID, location.getLocationId());
        values.put(DatabaseHelper.NAME, location.getName());
        values.put(DatabaseHelper.STREET, location.getStreet());
        values.put(DatabaseHelper.CITY, location.getCity());
        values.put(DatabaseHelper.REGION, location.getRegion());
        values.put(DatabaseHelper.COUNTRY, location.getCountry());
        values.put(DatabaseHelper.POSTAL_CODE, location.getPostalCode());
        values.put(DatabaseHelper.PHONE, location.getPhone());

        return values;
    }

    public ArrayList<Location> selectAllLocations() {

        ArrayList<Location> locations = new ArrayList<Location>();

        String queryStr = new String("SELECT * FROM " + DatabaseHelper.TABLE_LOCATION);

        Cursor cursor = database.rawQuery(queryStr, null);

        try{
            if (cursor != null){
                while (cursor.moveToNext()) {
                    Location location = cursorToLocation(cursor);
                    locations.add(location);
                }
            }
        } finally {
            if (cursor != null){
                cursor.close();
            }
        }

        return locations;
    }

    public boolean deleteLocations(){
        database.execSQL("delete from " + DatabaseHelper.TABLE_LOCATION);
        return true;
    }

    private Location cursorToLocation(Cursor cursor){
        Location location = new Location();

        location.setLocationId(cursor.getInt(1));
        location.setName(cursor.getString(2));
        location.setStreet(cursor.getString(3));
        location.setCity(cursor.getString(4));
        location.setRegion(cursor.getString(5));
        location.setCountry(cursor.getString(6));
        location.setPostalCode(cursor.getString(7));
        location.setPhone(cursor.getString(8));

        return location;
    }


    /* VISIBILITY */

    public void insertVisibilities(ArrayList<Visibility>visibilities){
        if (visibilities.size() > 0) {
	        database.beginTransaction();

	        try {

		        for (int i = 0; i < visibilities.size(); i++) {
			        Visibility visibility = visibilities.get(i);

			        ContentValues values = getValuesForVisibility(visibility);
			        database.insert(DatabaseHelper.TABLE_VISIBILITY, null, values);
		        }

		        database.setTransactionSuccessful();
	        }
	        finally {
		        database.endTransaction();
	        }
        }
    }

	private ContentValues getValuesForVisibility(Visibility visibility){
		ContentValues values = new ContentValues();

		values.put(DatabaseHelper.VISIBILITY_ID, visibility.getVisibilityId());
		values.put(DatabaseHelper.SITE_ID, visibility.getSiteId());
		values.put(DatabaseHelper.UNIT_ID, visibility.getUnitId());
		values.put(DatabaseHelper.NAME, visibility.getName());
		values.put(DatabaseHelper.TYPE, visibility.getType());
		values.put(DatabaseHelper.ACTIVE, visibility.isActive());
		values.put(DatabaseHelper.UNIT_LOCAL_ID, visibility.getLocalUnitId());

		return values;
	}

    public ArrayList<Visibility>selectVisibilitiesForSite(Site site){
        ArrayList<Visibility> visibilities = new ArrayList<Visibility>();

        String queryStr = new String("SELECT * FROM " + DatabaseHelper.TABLE_VISIBILITY + " WHERE site_id=" + site.getSiteId());

        Cursor cursor = database.rawQuery(queryStr, null);

        try{
            if (cursor != null){
                while (cursor.moveToNext()) {
                    Visibility visibility = cursorToVisibility(cursor);
                    visibilities.add(visibility);
                }
            }
        } finally {
            if (cursor != null){
                cursor.close();
            }
        }

        return visibilities;
    }

    public ArrayList<Visibility>selectVisibilitiesForUnit(Unit unit){
        ArrayList<Visibility> visibilities = new ArrayList<Visibility>();

        String queryStr;

	    if (unit.getUnitId() > 0){
		    queryStr = new String("SELECT * FROM " + DatabaseHelper.TABLE_VISIBILITY + " WHERE " + DatabaseHelper.UNIT_ID + "=" + unit.getUnitId());
	    }
	    else{
		    queryStr = new String("SELECT * FROM " + DatabaseHelper.TABLE_VISIBILITY + " WHERE " + DatabaseHelper.UNIT_LOCAL_ID + "=" + unit.getLocalId());
	    }

        Cursor cursor = database.rawQuery(queryStr, null);

        try{
            if (cursor != null){
                while (cursor.moveToNext()) {
                    Visibility visibility = cursorToVisibility(cursor);
                    visibilities.add(visibility);
                }
            }
        } finally {
            if (cursor != null){
                cursor.close();
            }
        }

        return visibilities;
    }

    public long updateVisibility(Visibility visibility){
        ContentValues values = getValuesForVisibility(visibility);

        String q;
        if (visibility.getUnitId() > 0) {
            q = "visibility_id=" + visibility.getVisibilityId() + " AND unit_id=" + visibility.getUnitId();
        }
        else{
            q = "visibility_id=" + visibility.getVisibilityId() + " AND site_id=" + visibility.getSiteId();
        }

        return database.update(DatabaseHelper.TABLE_VISIBILITY, values, q, null);
    }

    private Visibility cursorToVisibility(Cursor cursor){
        Visibility visibility = new Visibility();

        visibility.setVisibilityId(cursor.getInt(1));
        visibility.setSiteId(cursor.getInt(2));
        visibility.setUnitId(cursor.getInt(3));
        visibility.setName(cursor.getString(4));
        visibility.setType(cursor.getString(5));
        visibility.setActive(cursor.getInt(6) > 0);
	    visibility.setLocalUnitId(cursor.getLong(7));

        return visibility;
    }

    public boolean deleteVisibilitiesForSite(Site site){
        database.execSQL("delete from " + DatabaseHelper.TABLE_VISIBILITY + " WHERE site_id=" + site.getSiteId() );
        return true;
    }

	public boolean deleteVisibilitiesForUnit(Unit unit){
		if (unit.getUnitId() > 0){
			database.execSQL("delete from " + DatabaseHelper.TABLE_VISIBILITY + " WHERE " + DatabaseHelper.UNIT_ID + "=" + unit.getUnitId());
		}
		else{
			database.execSQL("delete from " + DatabaseHelper.TABLE_VISIBILITY + " WHERE " + DatabaseHelper.UNIT_LOCAL_ID + "=" + unit.getLocalId());
		}

		return true;
	}


    /* METRIC */

    public void insertMetrics(ArrayList<Metric>metrics){
        if (metrics.size() > 0){
	        database.beginTransaction();

	        try {

		        for (int i = 0; i < metrics.size(); i++) {
			        Metric metric = metrics.get(i);

			        ContentValues values = getValuesForMetric(metric);
			        database.insert(DatabaseHelper.TABLE_METRIC, null, values);
		        }

		        database.setTransactionSuccessful();
	        }
	        finally {
		        database.endTransaction();
	        }
        }
    }

	private ContentValues getValuesForMetric(Metric metric){
		ContentValues values = new ContentValues();

		values.put(DatabaseHelper.METRIC_ID, metric.getMetricId());
		values.put(DatabaseHelper.UNIT_ID, metric.getUnitId());
		values.put(DatabaseHelper.PROFILE_ID, metric.getProfileId());
		values.put(DatabaseHelper.SEQUENCE, metric.getSequence());
		values.put(DatabaseHelper.VALUE, metric.getValue());
		values.put(DatabaseHelper.SELECTED, metric.isSelected());
		values.put(DatabaseHelper.ABBREVIATION, metric.getAbbreviation());
		values.put(DatabaseHelper.NAME, metric.getName());

		return values;
	}

    public ArrayList<Metric>selectMetricsForUnit(Unit unit){
        ArrayList<Metric> metrics = new ArrayList<Metric>();

        String queryStr = new String("SELECT * FROM " + DatabaseHelper.TABLE_METRIC + " WHERE unit_id=" + unit.getUnitId());

        Cursor cursor = database.rawQuery(queryStr, null);

        try{
            if (cursor != null){
                while (cursor.moveToNext()) {
                    Metric metric = cursorToMetric(cursor);
                    metrics.add(metric);
                }
            }
        } finally {
            if (cursor != null){
                cursor.close();
            }
        }

        return metrics;
    }

    private Metric cursorToMetric(Cursor cursor){
        Metric metric = new Metric();

        metric.setMetricId(cursor.getInt(1));
        metric.setUnitId(cursor.getInt(2));
        metric.setProfileId(cursor.getInt(3));
        metric.setSequence(cursor.getInt(4));
        metric.setValue(cursor.getInt(5));
        metric.setSelected(cursor.getInt(6) > 0);
        metric.setAbbreviation(cursor.getString(7));
        metric.setName(cursor.getString(8));

        return metric;
    }

    public boolean deleteMetricsForUnit(Unit unit){
        database.execSQL("delete from " + DatabaseHelper.TABLE_METRIC + " WHERE unit_id=" + unit.getUnitId() );
        return true;
    }


    /* VISIBILITIES + METRICS COMBINED */

    public boolean deleteUnitsImagesVisibilitiesMetrics(){
        database.execSQL("delete from " + DatabaseHelper.TABLE_UNIT + " WHERE " + DatabaseHelper.UPDATED + "=0 OR " + DatabaseHelper.DELETE + "=0 OR " + DatabaseHelper.NEW + "=0;");
        database.execSQL("delete from " + DatabaseHelper.TABLE_IMAGE + " WHERE " + DatabaseHelper.NEW + " = 0");
        database.execSQL("delete from " + DatabaseHelper.TABLE_METRIC+" where " + DatabaseHelper.UNIT_ID + " > 0;");
        database.execSQL("delete from " + DatabaseHelper.TABLE_VISIBILITY + " where " + DatabaseHelper.UNIT_ID + " > 0;");
        database.execSQL("delete from " + DatabaseHelper.TABLE_UNIT_ATTRIBUTE);
        return true;
    }
}
