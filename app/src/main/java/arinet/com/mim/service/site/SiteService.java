package arinet.com.mim.service.site;

import android.content.Context;

import arinet.com.mim.model.Site;
import arinet.com.mim.service.database.DatabaseService;

/**
 * Created by kevinstueber on 7/28/14.
 */
public class SiteService {

    private static SiteService instance = null;

    public static SiteService getInstance() {
        if(instance == null) {
            instance = new SiteService();
        }
        return instance;
    }

    public Site getSite(Context context){
        return DatabaseService.getInstance(context.getApplicationContext()).selectSite();
    }

    public void deleteSite(Context context){
        DatabaseService.getInstance(context.getApplicationContext()).deleteSites();
    }
}
