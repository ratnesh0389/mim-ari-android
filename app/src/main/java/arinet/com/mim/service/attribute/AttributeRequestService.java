package arinet.com.mim.service.attribute;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import arinet.com.mim.R;
import arinet.com.mim.model.Attribute;
import arinet.com.mim.model.Filter;
import arinet.com.mim.model.Site;
import arinet.com.mim.service.database.DatabaseService;
import arinet.com.mim.service.json.JsonParser;
import arinet.com.mim.service.site.SiteService;

/**
 * Created by kevinstueber on 10/21/14.
 */
public class AttributeRequestService extends IntentService {

    public static final String NOTIFICATION = "arinet.com.mim.service.receiver";
    private int result = Activity.RESULT_CANCELED;
    private String sender = "attribute_request_service";

    public static final String SENDER = "sender";
    public static final String RESULT = "result";
    public static final String GET = "get";

    public AttributeRequestService() {
        super("AttributeRequestService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Boolean getAttributes = intent.getBooleanExtra(GET, false);

        if (getAttributes){
            requestAllAttributes();
        }
    }

    private void requestAllAttributes(){
        Site site = SiteService.getInstance().getSite(this.getApplicationContext());
        String urlString = this.getResources().getString(R.string.base_url) + "Attribute";

        if (site != null){
            String token = site.getToken();
            if (token.length() > 0){
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpGet httpGet = new HttpGet(urlString);
                    httpGet.setHeader("Authorization", token);

                    HttpResponse response = httpclient.execute(httpGet);

                    ArrayList<Attribute> attributes = JsonParser.parseAttributeResult(response);

                    DatabaseService.getInstance(this.getApplicationContext()).deleteAttributes();
                    DatabaseService.getInstance(getApplicationContext()).insertAttributes(attributes);
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
