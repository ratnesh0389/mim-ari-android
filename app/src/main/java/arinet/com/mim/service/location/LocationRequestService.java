package arinet.com.mim.service.location;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import arinet.com.mim.R;
import arinet.com.mim.model.Location;
import arinet.com.mim.model.Site;
import arinet.com.mim.service.database.DatabaseService;
import arinet.com.mim.service.json.JsonParser;
import arinet.com.mim.service.site.SiteService;

/**
 * Created by kevinstueber on 8/5/14.
 */
public class LocationRequestService extends IntentService {

    public static final String NOTIFICATION = "arinet.com.mim.service.receiver";
    private int result = Activity.RESULT_CANCELED;
    private String sender = "location_request_service";

    public static final String SENDER = "sender";
    public static final String RESULT = "result";
    public static final String GET = "get";

    public LocationRequestService() {
        super("LocationRequestService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        boolean isGet = intent.getBooleanExtra(GET, false);

        if (isGet){
            getLocations();
        }
    }

    private void getLocations(){
        Site site = SiteService.getInstance().getSite(this.getApplicationContext());

        if (site != null){
            String token = site.getToken();
            String urlString = this.getResources().getString(R.string.base_url) + "Locations";

            if (token.length() > 0){
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpGet httpget = new HttpGet(urlString);
                    httpget.setHeader("Authorization", token);
                    httpget.setHeader("Accept", "application/json");
                    httpget.setHeader("Content-type", "application/json");

                    HttpResponse response = httpclient.execute(httpget);

                    int statusCode = response.getStatusLine().getStatusCode();

                    if (statusCode == 200){
                        ArrayList<Location> locations = JsonParser.parseLocationResult(response);

                        if (DatabaseService.getInstance(this.getApplicationContext()).deleteLocations()){
                            DatabaseService.getInstance(this.getApplicationContext()).insertLocations(locations);
                        }
                        result = Activity.RESULT_OK;
                    }
                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
                finally {
                    publishLocationResults(result);
                }
            }
        }
        else{
            publishLocationResults(result);
        }
    }

    private void publishLocationResults(int result){

    }
}
