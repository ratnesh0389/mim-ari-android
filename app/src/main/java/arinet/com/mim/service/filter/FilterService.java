package arinet.com.mim.service.filter;

import android.content.Context;

import java.util.ArrayList;

import arinet.com.mim.model.Filter;
import arinet.com.mim.service.database.DatabaseService;

/**
 * Created by kevinstueber on 7/29/14.
 */
public class FilterService {

    private static FilterService instance = null;

    public static FilterService getInstance() {
        if(instance == null) {
            instance = new FilterService();
        }
        return instance;
    }

    public ArrayList<Filter> getFilters(Context context){
        return DatabaseService.getInstance(context.getApplicationContext()).selectAllFilters();
    }

    public Filter createPopularFilter(){
        Filter filter = new Filter();
        filter.setName("Home");
        filter.setLocation("");
        filter.setQuery("{ \"Flags\": [ { \"Name\": \"status\", \"Is\": \"popular\" } ] }");
        return filter;
    }

    public Filter createRecentFilter(){
        Filter filter = new Filter();
        filter.setName("Recent");
        filter.setLocation("");
        filter.setQuery("{ \"Flags\": [ { \"Name\": \"status\", \"Is\": \"recentlyModified\" } ] }");
        return filter;
    }

    public Filter createVisibilityFilterForName(String name){
        Filter filter = new Filter();
        filter.setName(name);
        filter.setLocation("");
        filter.setQuery("{ \"Flags\": [ { \"Name\": \"visibility\", \"Is\": \"" + name + "\" } ] }");
        return filter;
    }


}
