package arinet.com.mim.service.unit;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeoutException;

import arinet.com.mim.R;
import arinet.com.mim.model.Image;
import arinet.com.mim.model.Metric;
import arinet.com.mim.model.Unit;
import arinet.com.mim.model.UnitAttribute;
import arinet.com.mim.model.Visibility;
import arinet.com.mim.service.connection.ConnectionService;
import arinet.com.mim.service.database.DatabaseService;
import arinet.com.mim.service.image.ImageService;
import arinet.com.mim.service.json.JsonParser;
import arinet.com.mim.service.site.SiteService;

/**
 * Created by kevinstueber on 7/28/14.
 */
public class UnitRequestService extends IntentService {

    public static final String NOTIFICATION = "arinet.com.mim.service.receiver";
    private int result = Activity.RESULT_CANCELED;
    private String sender = "unit_request_service";

    public static final String SENDER = "sender";
    public static final String RESULT = "result";
    public static final String GET = "get";
    public static final String CODE = "code";
    public static final String QUERY = "query";
    public static final String PUT = "put";
    public static final String UNIT_ID = "unit_id";
    public static final String LOCAL_UNIT_ID = "local_unit_id";
    public static final String DELETE = "delete";
    public static final String OFFLINE_REQUEST = "offline_request";
    public static final String GET_PRODUCTS = "get_products";
    public static final String ADD = "add";
    public static final String DUPLICATE = "duplicate";
    public static final String SEARCH = "search";

    public static final int OKAY = 200;
    public static final int TIMEOUT = 408;
    public static final int BAD_REQUEST = 400;
    public static final int EXCEPTION = 500;

    public UnitRequestService() {
        super("UnitRequestService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Boolean getUnits = intent.getBooleanExtra(GET, false);
        Boolean putUnit = intent.getBooleanExtra(PUT, false);
        Boolean deleteUnit = intent.getBooleanExtra(DELETE, false);
        Boolean offlineRequest = intent.getBooleanExtra(OFFLINE_REQUEST, false);
        Boolean getProducts = intent.getBooleanExtra(GET_PRODUCTS, false);
        Boolean add = intent.getBooleanExtra(ADD, false);
        Boolean duplicate = intent.getBooleanExtra(DUPLICATE, false);
        boolean search = intent.getBooleanExtra(SEARCH, false);

        String query = intent.getStringExtra(QUERY);
        int unitId = intent.getIntExtra(UNIT_ID, -1);
        long localUnitId = intent.getLongExtra(LOCAL_UNIT_ID, -1);

        if (getUnits){
            getUnitResults(query);
        }
        else if (search){
            getSearchResults(query);
        }
        else if (putUnit){
            if (ConnectionService.getInstance().isOnline(getApplicationContext()) && unitId > 0) {
                putUnitUpdate(UnitService.getInstance().getUnitWithUnitId(this.getApplicationContext(), unitId), offlineRequest, false);
            }
            else{
                publishPutResults(result, offlineRequest, BAD_REQUEST);
            }
        }
        else if (deleteUnit) {
            if (ConnectionService.getInstance().isOnline(getApplicationContext()) && unitId > 0) {
                deleteUnit(UnitService.getInstance().getUnitWithUnitId(this.getApplicationContext(), unitId), offlineRequest);
            } else {
                publishDeleteResults(result, offlineRequest, BAD_REQUEST);
            }
        }
        else if (getProducts){
            getProductsForQuery(query);
        }
        else if (add){
            postAddUnit(localUnitId, offlineRequest);
        }
        else if (duplicate){
	        if (ConnectionService.getInstance().isOnline(getApplicationContext())) {
		        postDuplicateUnit(localUnitId);
	        }
	        else{
				publishDuplicateResults(Activity.RESULT_OK, localUnitId, BAD_REQUEST);
	        }
        }
    }

    private void getUnitResults(String query){
        String token = SiteService.getInstance().getSite(this.getApplicationContext()).getToken();
        String urlString = this.getResources().getString(R.string.base_url) + "Search";

        if (token.length() > 0){
            JsonObject jsonParamsObject = new Gson().fromJson(query, JsonObject.class);
            Ion.getDefault(getApplicationContext()).cancelAll(getApplicationContext());
            Ion.with(getApplicationContext())
                .load("POST", urlString)
                .setHeader("Authorization", token)
                .setJsonObjectBody(jsonParamsObject)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray resultObj) {
                        if (e != null) {

                            int statusCode = EXCEPTION;

                            if (e.getClass() == HttpHostConnectException.class || e.getClass() == UnknownHostException.class || e.getClass() == TimeoutException.class){
                                statusCode = TIMEOUT;
                            }

                            publishSearchResults(result, statusCode);
                        } else {
                            System.out.println(resultObj);
                            new ProcessUnitsTask().execute(JsonParser.parseSearchResults(resultObj).toArray(new Unit[0]));
                        }
                    }
                });
        }
    }

    private void getSearchResults(String query){
        try {
            String token = SiteService.getInstance().getSite(this.getApplicationContext()).getToken();
            String urlString = this.getResources().getString(R.string.base_url) + "Search?keyword=" + URLEncoder.encode(query, "utf-8");
            System.out.println(urlString);
            if (token.length() > 0) {
                Ion.getDefault(getApplicationContext()).cancelAll(getApplicationContext());
                Ion.with(getApplicationContext())
                        .load("GET", urlString)
                        .setHeader("Authorization", token)
                        .asJsonArray()
                        .setCallback(new FutureCallback<JsonArray>() {
                            @Override
                            public void onCompleted(Exception e, JsonArray resultObj) {
                                if (e != null) {
                                    int statusCode = EXCEPTION;

                                    if (e.getClass() == HttpHostConnectException.class || e.getClass() == UnknownHostException.class || e.getClass() == TimeoutException.class){
                                        statusCode = TIMEOUT;
                                    }
                                    publishSearchResults(result, statusCode);
                                } else {
                                    new ProcessUnitsTask().execute(JsonParser.parseSearchResults(resultObj).toArray(new Unit[0]));
                                }
                            }
                        });
            }
        }
        catch (UnsupportedEncodingException e){
            publishSearchResults(result, BAD_REQUEST);
        }
    }

    private class ProcessUnitsTask extends AsyncTask<Unit, Void, ArrayList<Unit>> {

        protected ArrayList<Unit> doInBackground(Unit... units) {

            ArrayList<Unit>allUnits = new ArrayList<Unit>(Arrays.asList(units));
            ArrayList<Visibility>allVisibilities = new ArrayList<Visibility>();
            ArrayList<Metric>allMetrics = new ArrayList<Metric>();
	        ArrayList<Image>allImages = new ArrayList<Image>();
            ArrayList<UnitAttribute>allUnitAttributes = new ArrayList<UnitAttribute>();

            System.out.println("Start Processing");

            if (DatabaseService.getInstance(getApplicationContext()).deleteUnitsImagesVisibilitiesMetrics()) {
                for (Unit unit : allUnits) {
                    allVisibilities.addAll(unit.getVisibilities());
                    allMetrics.addAll(unit.getMetrics());
                    allUnitAttributes.addAll(unit.getUnitAttributes());

                    long localId = DatabaseService.getInstance(getApplicationContext()).insertUnit(unit);
                    for (Image image : unit.getImages()){
                        image.setUnitLocalId(localId);
                    }
	                allImages.addAll(unit.getImages());
                }
            }
	        DatabaseService.getInstance(getApplicationContext()).insertImages(allImages);
	        DatabaseService.getInstance(getApplicationContext()).insertVisibilities(allVisibilities);
	        DatabaseService.getInstance(getApplicationContext()).insertMetrics(allMetrics);
            DatabaseService.getInstance(getApplicationContext()).insertUnitAttributes(allUnitAttributes);

            System.out.println("End Processing");
            return allUnits;
        }

        protected void onPostExecute(ArrayList<Unit> units) {
            result = Activity.RESULT_OK;
            publishSearchResults(result, OKAY);
        }
    }

    public void putUnitUpdate(final Unit unit, final boolean offlineRequest, final boolean isDuplicate){
        String token = SiteService.getInstance().getSite(this.getApplicationContext()).getToken();
        String urlString = this.getResources().getString(R.string.base_url) + "Unit/" + unit.getUnitId();

	    if (token.length() > 0 && unit != null) {
		    JsonObject jsonParamsObject = new Gson().fromJson(jObjectFromUnit(unit, false).toString(), JsonObject.class);
		    Ion.with(getApplicationContext())
				    .load("PUT",urlString)
				    .setHeader("Authorization", token)
				    .setJsonObjectBody(jsonParamsObject)
				    .asJsonObject()
				    .setCallback(new FutureCallback<JsonObject>() {
					    @Override
					    public void onCompleted(Exception e, JsonObject resultObj) {
                            int statusCode = 0;
						    if (e != null) {

                                statusCode = EXCEPTION;

                                if (e.getClass() == HttpHostConnectException.class || e.getClass() == UnknownHostException.class || e.getClass() == TimeoutException.class){
                                    statusCode = TIMEOUT;
                                }

							    if (isDuplicate){
								    publishDuplicateResults(result, unit.getLocalId(), statusCode);
							    }
							    else{
								    publishPutResults(result, offlineRequest, statusCode);
							    }
						    } else {

                                statusCode = OKAY;

							    unit.setUpdated(false);
							    DatabaseService.getInstance(getApplicationContext()).updateUnit(unit);
							    result = Activity.RESULT_OK;

							    if (isDuplicate){
								    publishDuplicateResults(result, unit.getLocalId(), statusCode);
							    }
							    else{
								    publishPutResults(result, offlineRequest, statusCode);
							    }
						    }
					    }
				    });
		    }
	}

    private void deleteUnit(Unit unit, boolean offlineRequest){
        String token = SiteService.getInstance().getSite(this.getApplicationContext()).getToken();
        String urlString = this.getResources().getString(R.string.base_url) + "Unit/" + unit.getUnitId();

        int statusCode = OKAY;

        if (token.length() > 0 && unit != null){
            try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpDelete httpdelete = new HttpDelete(urlString);
                httpdelete.setHeader("Authorization", token);
                httpdelete.setHeader("Accept", "application/json");
                httpdelete.setHeader("Content-type", "application/json");

                HttpResponse response = httpclient.execute(httpdelete);

                statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == OKAY){
                    result = Activity.RESULT_OK;
                    DatabaseService.getInstance(getApplicationContext()).deleteUnit(unit);
                }
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (HttpHostConnectException e){
                e.printStackTrace();
                statusCode = TIMEOUT;
            } catch (UnknownHostException e){
                e.printStackTrace();
                statusCode = TIMEOUT;
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                publishDeleteResults(result, offlineRequest, statusCode);
            }
        }
    }

    private void postAddUnit(final long localUnitId, final boolean offlineRequest){
        String token = SiteService.getInstance().getSite(this.getApplicationContext()).getToken();
        String urlString = this.getResources().getString(R.string.base_url) + "Unit";
        final Unit unit = UnitService.getInstance().getUnitWithLocalUnitId(getApplicationContext(), localUnitId);

        if (token.length() > 0 && unit != null){

            JsonObject jsonParamsObject = new Gson().fromJson(jObjectFromUnit(unit, true).toString(), JsonObject.class);

            Ion.with(getApplicationContext())
                .load("POST", urlString)
                .setHeader("Authorization", token)
                .setJsonObjectBody(jsonParamsObject)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject resultObj) {
                        int unitId = 0;
                        int statusCode = 0;
                        if (e != null) {
                            statusCode = EXCEPTION;

                            if (e.getClass() == HttpHostConnectException.class || e.getClass() == UnknownHostException.class || e.getClass() == TimeoutException.class){
                                statusCode = TIMEOUT;
                            }
                            publishAddResults(result, unitId, offlineRequest, statusCode);
                        } else {
                            Unit updatedUnit = JsonParser.parseAddResult(resultObj);

                            if (updatedUnit != null) {
                                result = Activity.RESULT_OK;

                                long updateLocalUnitId = DatabaseService.getInstance(getApplicationContext()).insertUnit(updatedUnit);
                                updatedUnit.setLocalId(updateLocalUnitId);
                                unitId = updatedUnit.getUnitId();

                                DatabaseService.getInstance(getApplicationContext()).insertVisibilities(updatedUnit.getVisibilities());
                                DatabaseService.getInstance(getApplicationContext()).insertMetrics(updatedUnit.getMetrics());

                                if (updatedUnit.getImages().size() > 0) {
                                    for (Image image : updatedUnit.getImages()) {
                                        image.setUnitLocalId(updateLocalUnitId);
                                        image.setUnitId(updatedUnit.getUnitId());
                                    }
                                    DatabaseService.getInstance(getApplicationContext()).insertImages(updatedUnit.getImages());
                                }

                                Unit offlineUnit = DatabaseService.getInstance(getApplicationContext()).selectUnitWithLocalUnitId(localUnitId);
                                ArrayList<Image> offlineImages;
                                if (offlineUnit != null) {
                                    offlineImages = DatabaseService.getInstance(getApplicationContext()).selectImagesForUnit(offlineUnit);
                                    for (int i = 0; i < offlineImages.size(); ++i) {
                                        Image offlineImage = offlineImages.get(i);
                                        if (offlineImage.getImageId() == 0) {
                                            offlineImage.setUnitId(unitId);
                                            offlineImage.setUnitLocalId(updateLocalUnitId);
                                            DatabaseService.getInstance(getApplicationContext()).updateImage(offlineImage);
                                        }
                                    }
                                }
                                DatabaseService.getInstance(getApplicationContext()).deleteUnit(unit);
                                DatabaseService.getInstance(getApplicationContext()).deleteImagesForProductResults();
                            }
                            publishAddResults(result, unitId, offlineRequest, OKAY);
                        }
                    }
                });
        }
    }

    private void postDuplicateUnit(final long localUnitId){
        String token = SiteService.getInstance().getSite(this.getApplicationContext()).getToken();
        String urlString = this.getResources().getString(R.string.base_url) + "Unit";

	    final Unit unit = UnitService.getInstance().getUnitWithLocalUnitId(getApplicationContext(), localUnitId);

        if (token.length() > 0 && unit != null){
            JsonObject jsonParamsObject = new Gson().fromJson(jObjectFromUnit(unit, true).toString(), JsonObject.class);
            Ion.with(getApplicationContext())
                    .load("POST", urlString)
                    .setHeader("Authorization", token)
                    .setJsonObjectBody(jsonParamsObject)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject resultObj) {
                            int unitId = 0;
                            int statusCode = 0;
                            if (e != null) {
                                statusCode = EXCEPTION;

                                if (e.getClass() == HttpHostConnectException.class || e.getClass() == UnknownHostException.class || e.getClass() == TimeoutException.class){
                                    statusCode = TIMEOUT;
                                }
                                publishDuplicateResults(result, localUnitId, statusCode);
                            } else {
                                Unit updatedUnit = JsonParser.parseAddResult(resultObj);

                                if (updatedUnit != null){

	                                //Delete the old unit and its images
	                                DatabaseService.getInstance(getApplicationContext()).deleteUnit(unit);
                                    DatabaseService.getInstance(getApplicationContext()).deleteImagesForUnit(unit);

	                                //Insert the new unit and set it's local id
                                    updatedUnit.setLocalId(DatabaseService.getInstance(getApplicationContext()).insertUnit(updatedUnit));

	                                for (Visibility visibility : updatedUnit.getVisibilities()){
										for (int i = unit.getVisibilities().size()-1; i >= 0; --i){
											if (unit.getVisibilities().get(i).getVisibilityId() == visibility.getVisibilityId()){
												visibility.setActive(unit.getVisibilities().get(i).isActive());
												unit.getVisibilities().remove(i);
												break;
											}
										}
	                                }
									DatabaseService.getInstance(getApplicationContext()).deleteVisibilitiesForUnit(unit);

	                                //Insert the visibilities and metrics for the new unit
                                    DatabaseService.getInstance(getApplicationContext()).insertVisibilities(updatedUnit.getVisibilities());
                                    DatabaseService.getInstance(getApplicationContext()).insertMetrics(updatedUnit.getMetrics());

	                                //Update the images and insert them
                                    if (updatedUnit.getImages().size() > 0){
                                        for (Image image : updatedUnit.getImages()){
                                            image.setUnitLocalId(updatedUnit.getLocalId());
                                            image.setUnitId(updatedUnit.getUnitId());
                                        }
                                        DatabaseService.getInstance(getApplicationContext()).insertImages(updatedUnit.getImages());
                                    }
                                    result = Activity.RESULT_OK;

	                                putUnitUpdate(updatedUnit, false, true);
                                }
                                else{
                                    publishDuplicateResults(result, localUnitId, OKAY);
                                }
                            }
                        }
                    });
        }
    }

    private void getProductsForQuery(String query){
        String token = SiteService.getInstance().getSite(this.getApplicationContext()).getToken();

        if (token.length() > 0){
            try{
                String urlString = this.getResources().getString(R.string.base_url) + "Search?productData=" + URLEncoder.encode(query, "utf-8");
                Ion.getDefault(getApplicationContext()).cancelAll(getApplicationContext());
                Ion.with(getApplicationContext())
                        .load(urlString)
                        .setHeader("Authorization", token)
                        .asJsonArray()
                        .setCallback(new FutureCallback<JsonArray>() {
                            @Override
                            public void onCompleted(Exception e, JsonArray resultObj) {
                                int statusCode = 0;
                                if (e != null) {
                                    statusCode = EXCEPTION;

                                    if (e.getClass() == HttpHostConnectException.class || e.getClass() == UnknownHostException.class || e.getClass() == TimeoutException.class){
                                        statusCode = TIMEOUT;
                                    }
                                    publishProductResults(result, statusCode);
                                } else {
                                    ArrayList<Unit> units = JsonParser.parseProductResult(resultObj);
                                    if (DatabaseService.getInstance(getApplicationContext()).deleteProductResults() && DatabaseService.getInstance(getApplicationContext()).deleteImagesForProductResults()){
                                        for (Unit unit : units){
                                            unit.setProductResult(true);
                                            unit.setPublishedOnWeb(true);
                                            long localId = DatabaseService.getInstance(getApplicationContext()).insertUnit(unit);

                                            if (unit.getImages().size() > 0){
                                                for (Image image : unit.getImages()){
                                                    image.setProductResult(true);
                                                    image.setUnitLocalId(localId);
                                                }
                                                DatabaseService.getInstance(getApplicationContext()).insertImages(unit.getImages());
                                            }
                                        }
                                    }
                                    result = Activity.RESULT_OK;
                                    publishProductResults(result, OKAY);
                                }
                            }
                        });
            }
            catch (UnsupportedEncodingException e){
                e.printStackTrace();
                publishProductResults(result, BAD_REQUEST);
            }
            finally {

            }
        }
    }

    private JSONObject jObjectFromUnit(Unit unit, boolean addImages){
        try {
            JSONObject jObjectUnit = new JSONObject();

            jObjectUnit.put("Id", unit.getUnitId());
            jObjectUnit.put("Model", unit.getModel());
            jObjectUnit.put("MakeId", unit.getMakeId());
            jObjectUnit.put("EquipmentTypeId", unit.getEquipmentTypeId());
            jObjectUnit.put("PriceLabel", unit.getPriceLabel());
            jObjectUnit.put("Make", unit.getModel());
            jObjectUnit.put("Year", unit.getYear());
            jObjectUnit.put("TitleStatus", unit.getTitleStatus());
            jObjectUnit.put("Condition", unit.getCondition());
            jObjectUnit.put("CallForPrice", unit.isCallForPrice());
            jObjectUnit.put("PublishedOnWeb", unit.isPublishedOnWeb());
            jObjectUnit.put("UsageStatus", unit.getUsageStatus());
            jObjectUnit.put("DmsPrice", unit.getDmsPrice());
            jObjectUnit.put("UseStockImages", unit.isUseStockImages());
            jObjectUnit.put("DisplayInShowcaseCount", unit.isDisplayInShowcaseCount());
            if (unit.getPrice() > 0){
                jObjectUnit.put("Price", unit.getPrice());
            }
            jObjectUnit.put("StyleId", unit.getStyleId());
            jObjectUnit.put("LinkedProductId", unit.getLinkedProductId());
            jObjectUnit.put("SaleActive", unit.isSaleActive());
            if (unit.getSalePrice() > 0){
                jObjectUnit.put("SalePrice", unit.getSalePrice());
            }
            jObjectUnit.put("LocationId", unit.getLocationId());
            if (unit.getBestPrice() > 0){
                jObjectUnit.put("BestPrice", unit.getBestPrice());
            }
            jObjectUnit.put("unfinalizedUnitId", unit.getUnfinalizedUnitId());
            jObjectUnit.put("InventoryStatusValue", unit.getInventoryStatusValue());
            jObjectUnit.put("PrimaryColor", unit.getPrimaryColor());
            jObjectUnit.put("TrimColor", unit.getTrimColor());
            jObjectUnit.put("Description", unit.getDescription());
            jObjectUnit.put("StockNumber", unit.getStockNumber());
            jObjectUnit.put("GlobalIdentifier", unit.getGlobalIdentifier());
            jObjectUnit.put("EquipmentType", unit.getEquipmentType());
            jObjectUnit.put("SaleType", unit.getSaleType());
            jObjectUnit.put("saleStartDate", unit.getSaleStartDate());
            jObjectUnit.put("saleEndDate", unit.getSaleEndDate());
            jObjectUnit.put("LastModified", unit.getLastUpdate());
            jObjectUnit.put("Vin", unit.getVin());

            if (unit.getMetrics() != null){
                JSONArray jArrayMetrics = new JSONArray();
                for (Metric metric : unit.getMetrics()){
                    JSONObject jObjectMetric = new JSONObject();

                    jObjectMetric.put("Id", metric.getMetricId());
                    jObjectMetric.put("ProfileId", metric.getProfileId());
                    jObjectMetric.put("Sequence", metric.getSequence());
                    if (metric.getValue() > -1){
                        jObjectMetric.put("Value", metric.getValue());
                    }
                    jObjectMetric.put("Selected", metric.isSelected());
                    jObjectMetric.put("Abbreviation", metric.getAbbreviation());
                    jObjectMetric.put("Name", metric.getName());

                    jArrayMetrics.put(jObjectMetric);
                }
                jObjectUnit.put("Metrics", jArrayMetrics);
            }

            if (unit.getVisibilities() != null) {
                JSONArray jArrayVisibilities = new JSONArray();
                for (Visibility visibility : unit.getVisibilities()) {
                    JSONObject jObjectVisibility = new JSONObject();

                    jObjectVisibility.put("Id", visibility.getVisibilityId());
                    jObjectVisibility.put("Active", visibility.isActive());
                    jObjectVisibility.put("Name", visibility.getName());
                    jObjectVisibility.put("Type", visibility.getType());

                    jArrayVisibilities.put(jObjectVisibility);
                }
                jObjectUnit.put("Visibility", jArrayVisibilities);
            }

            if (unit.getImages() != null && addImages){
                JSONArray jArrayImages = new JSONArray();

                for (Image image : unit.getImages()){
                    if (image.getStockImageId() > 0){
                        JSONObject jObjectImage = new JSONObject();

                        jObjectImage.put("Id", image.getImageId());
                        jObjectImage.put("StockImageId", image.getStockImageId());
                        jObjectImage.put("IsPrimary", image.isPrimary());
                        jObjectImage.put("Location", image.getLocation());
                        jObjectImage.put("ThumbLocation", image.getThumbLocation());
                        jObjectImage.put("Sequence", image.getSequence());

                        jArrayImages.put(jObjectImage);
                    }
                }
                jObjectUnit.put("Images", jArrayImages);
            }

            if (unit.getUnitAttributes() != null){

                JSONArray jArrayUnitAttributes = new JSONArray();

                for (UnitAttribute ua : unit.getUnitAttributes()){
                    JSONObject jObjectUA = new JSONObject();

                    jObjectUA.put("id", ua.getAttributeId());
                    jObjectUA.put("name", ua.getName());
                    jObjectUA.put("type", ua.getType());
                    jObjectUA.put("value", ua.getValue());

                    jArrayUnitAttributes.put(jObjectUA);
                }
                jObjectUnit.put("Attributes", jArrayUnitAttributes);
            }
			//System.out.println(jObjectUnit);
            return jObjectUnit;
        }
        catch (JSONException e){
            e.printStackTrace();
        }
        return null;
    }

    private void publishSearchResults(int r, int statusCode) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, r);
        intent.putExtra(SENDER, sender);
        intent.putExtra(GET, true);
        intent.putExtra(CODE, statusCode);
        sendBroadcast(intent);
    }

    private void publishProductResults(int r, int statusCode) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, r);
        intent.putExtra(SENDER, sender);
        intent.putExtra(GET_PRODUCTS, true);
        intent.putExtra(CODE, statusCode);
        sendBroadcast(intent);
    }

    private void publishAddResults(int r, int unitId, boolean offlineRequest, int statusCode) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, r);
        intent.putExtra(SENDER, sender);
        intent.putExtra(ADD, true);
        intent.putExtra(UNIT_ID, unitId);
        intent.putExtra(OFFLINE_REQUEST, offlineRequest);
        intent.putExtra(CODE, statusCode);
        sendBroadcast(intent);
    }

    private void publishDuplicateResults(int r, long localUnitid, int statusCode){
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, r);
        intent.putExtra(SENDER, sender);
        intent.putExtra(DUPLICATE, true);
        intent.putExtra(LOCAL_UNIT_ID, localUnitid);
        intent.putExtra(CODE, statusCode);
        sendBroadcast(intent);
    }

    private void publishPutResults(int r, boolean offlineRequest, int statusCode) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, r);
        intent.putExtra(SENDER, sender);
        intent.putExtra(PUT, true);
        intent.putExtra(OFFLINE_REQUEST, offlineRequest);
        intent.putExtra(CODE, statusCode);
        sendBroadcast(intent);
    }

    private void publishDeleteResults(int r, boolean offlineRequest, int statusCode) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, r);
        intent.putExtra(SENDER, sender);
        intent.putExtra(DELETE, true);
        intent.putExtra(OFFLINE_REQUEST, offlineRequest);
        intent.putExtra(CODE, statusCode);
        sendBroadcast(intent);
    }
}
