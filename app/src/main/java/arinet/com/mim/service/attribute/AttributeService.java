package arinet.com.mim.service.attribute;

import android.content.Context;

import java.util.ArrayList;

import arinet.com.mim.model.Attribute;
import arinet.com.mim.service.database.DatabaseService;

/**
 * Created by kevinstueber on 10/21/14.
 */
public class AttributeService {

    private static AttributeService instance = null;

    public static AttributeService getInstance() {
        if(instance == null) {
            instance = new AttributeService();
        }
        return instance;
    }

    public ArrayList<Attribute> getAllAttributes(Context context){
        return DatabaseService.getInstance(context).selectAllAttributes();
    }

}
