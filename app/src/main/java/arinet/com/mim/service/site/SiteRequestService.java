package arinet.com.mim.service.site;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import arinet.com.mim.R;
import arinet.com.mim.model.Site;
import arinet.com.mim.service.connection.ConnectionService;
import arinet.com.mim.service.database.DatabaseService;
import arinet.com.mim.service.json.JsonParser;

/**
 * Created by kevinstueber on 7/28/14.
 */
public class SiteRequestService extends IntentService{

    public static final String NOTIFICATION = "arinet.com.mim.service.receiver";
    private String sender = "site_request_service";

    public static final String SENDER = "sender";
    public static final String RESULT = "result";
    public static final String ADD = "add";
    public static final String REMOVE = "remove";
    public static final String VERIFY = "verify";
    public static final String CODE = "code";
    public static final int OFFLINE = -1;

    public static final int OKAY = 200;
    public static final int TIMEOUT = 408;
    public static final int BAD_REQUEST = 400;
    public static final int EXCEPTION = 500;

    public SiteRequestService() {
        super("SiteRequestService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Boolean addSite = intent.getBooleanExtra(ADD, false);
        Boolean verifySites = intent.getBooleanExtra(VERIFY, false);
        Boolean removeSite = intent.getBooleanExtra(REMOVE, false);

        if (addSite){
            addSite(intent.getStringExtra(CODE));
        }
        else if (verifySites){
            if (ConnectionService.getInstance().isOnline(getApplicationContext())) {
                verifySites();
            }
            else{
                publishVerifyResults(Activity.RESULT_CANCELED, OFFLINE);
            }
        }
        else if (removeSite){
            deleteSite();
        }
    }

    private void addSite(String code){

        int statusCode = 0;

        Site site = null;
        int result = Activity.RESULT_CANCELED;

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(this.getResources().getString(R.string.base_url) + "Activate");

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("Code", code));
            params.add(new BasicNameValuePair("Device", "1"));

            httppost.setEntity(new UrlEncodedFormEntity(params));

            HttpResponse response = httpclient.execute(httppost);
            statusCode = response.getStatusLine().getStatusCode();

            if (statusCode == OKAY) {
                site = JsonParser.parseSiteAuthResult(response);

                if (site != null) {
                    if (DatabaseService.getInstance(this.getApplicationContext()).deleteSites()){
                        if (DatabaseService.getInstance(this.getApplicationContext()).insertSite(site) > 0){
                            result = Activity.RESULT_OK;
                        }
                    }
                }
            }
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (HttpHostConnectException e){
            e.printStackTrace();
            statusCode = TIMEOUT;
        } catch (UnknownHostException e){
            e.printStackTrace();
            statusCode = TIMEOUT;
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            publishAddResults(result, statusCode);
        }
    }

    private void verifySites(){
        int statusCode = 0;
        int result = Activity.RESULT_CANCELED;

        Site site = DatabaseService.getInstance(this.getApplicationContext()).selectSite();
        if (site != null && site.getToken().length() > 0){
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(this.getResources().getString(R.string.base_url) + "/Activate");

                httpGet.setHeader("Authorization", site.getToken());

                HttpResponse response = httpclient.execute(httpGet);

                statusCode = response.getStatusLine().getStatusCode();

                if (statusCode == OKAY) {
                    if (JsonParser.parseVerifyAuthResult(getApplicationContext(), response)){
                        result = Activity.RESULT_OK;
                    }
                }
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (HttpHostConnectException e){
                e.printStackTrace();
                statusCode = TIMEOUT;
            } catch (UnknownHostException e){
                e.printStackTrace();
                statusCode = TIMEOUT;
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                publishVerifyResults(result, statusCode);
            }
        }
        else{
            publishVerifyResults(result, statusCode);
        }
    }

    private void deleteSite(){
        Site site = DatabaseService.getInstance(this.getApplicationContext()).selectSite();
        int result = Activity.RESULT_CANCELED;

        if (site != null && site.getToken().length() > 0){
            try {

                HttpClient httpclient = new DefaultHttpClient();
                HttpDelete httpDelete = new HttpDelete(this.getResources().getString(R.string.base_url) + "Activate/"+site.getSiteId());

                httpDelete.setHeader("Authorization", site.getToken());

                HttpResponse response = httpclient.execute(httpDelete);

                int statusCode = response.getStatusLine().getStatusCode();

                if (statusCode == OKAY) {
                    result = Activity.RESULT_OK;
                    SiteService.getInstance().deleteSite(this.getApplicationContext());
                }
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                publishDeleteResults(result);
            }
        }
        else{
            publishDeleteResults(result);
        }
    }

    private void publishAddResults(int result, int statusCode) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, result);
        intent.putExtra(SENDER, sender);
        intent.putExtra(ADD, true);
        intent.putExtra(CODE, statusCode);
        sendBroadcast(intent);
    }

    private void publishVerifyResults(int result, int statusCode) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, result);
        intent.putExtra(SENDER, sender);
        intent.putExtra(VERIFY, true);
        intent.putExtra(CODE, statusCode);
        sendBroadcast(intent);
    }

    private void publishDeleteResults(int result) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, result);
        intent.putExtra(SENDER, sender);
        intent.putExtra(REMOVE, true);
        sendBroadcast(intent);
    }
}
