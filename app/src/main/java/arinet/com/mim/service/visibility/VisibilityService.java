package arinet.com.mim.service.visibility;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;

import arinet.com.mim.model.Site;
import arinet.com.mim.model.Unit;
import arinet.com.mim.model.Visibility;
import arinet.com.mim.service.database.DatabaseService;
import arinet.com.mim.service.unit.UnitService;

/**
 * Created by kevinstueber on 8/6/14.
 */
public class VisibilityService {

    private static VisibilityService instance = null;

    public static VisibilityService getInstance() {
        if(instance == null) {
            instance = new VisibilityService();
        }
        return instance;
    }

    public ArrayList<Visibility> getVisibilitiesForUnit(Context context, Unit unit){
        return DatabaseService.getInstance(context.getApplicationContext()).selectVisibilitiesForUnit(unit);
    }

    public ArrayList<Visibility> getVisibilitiesForSite(Context context, Site site){
	    ArrayList<Visibility> visibilities = DatabaseService.getInstance(context.getApplicationContext()).selectVisibilitiesForSite(site);

	    ArrayList<Visibility>showrooms = new ArrayList<Visibility>();
	    ArrayList<Visibility>saleschannels = new ArrayList<Visibility>();

	    for (Visibility visibility : visibilities){
			if (visibility.getType().equalsIgnoreCase("showroom")){
				showrooms.add(visibility);
			}
		    else if (visibility.getType().equalsIgnoreCase("saleschannel")){
				saleschannels.add(visibility);
			}
	    }

	    Collections.sort(showrooms);
	    Collections.sort(saleschannels);

	    visibilities.clear();

	    visibilities.addAll(saleschannels);
	    visibilities.addAll(showrooms);

	    return visibilities;
    }

    public ArrayList<Visibility> updateVisibility(Context context, Visibility visibility){
        Unit unit = UnitService.getInstance().getUnitWithUnitId(context.getApplicationContext(), visibility.getUnitId());
        ArrayList<Visibility> visibilities = getVisibilitiesForUnit(context.getApplicationContext(), unit);

        for (Visibility v : visibilities){
            if (v.getVisibilityId() == visibility.getVisibilityId()){
                v = visibility;
                DatabaseService.getInstance(context.getApplicationContext()).updateVisibility(visibility);
                break;
            }
        }

        return visibilities;
    }

    public String[] getVisibilityNamesForSite(Context context, Site site){
        ArrayList<Visibility>visibilities = DatabaseService.getInstance(context.getApplicationContext()).selectVisibilitiesForSite(site);
        String[]visibilityNameArray = new String[visibilities.size()];
        for (int i = 0; i < visibilities.size(); ++i){
            visibilityNameArray[i] = visibilities.get(i).getName();
        }
        return visibilityNameArray;
    }

}
