package arinet.com.mim.service.location;

import android.content.Context;

import java.util.ArrayList;

import arinet.com.mim.model.Location;
import arinet.com.mim.service.database.DatabaseService;

/**
 * Created by kevinstueber on 8/5/14.
 */
public class LocationService {

    private static LocationService instance = null;

    public static LocationService getInstance() {
        if(instance == null) {
            instance = new LocationService();
        }
        return instance;
    }

    public ArrayList<Location> getLocations(Context context){
        return DatabaseService.getInstance(context.getApplicationContext()).selectAllLocations();
    }

}
