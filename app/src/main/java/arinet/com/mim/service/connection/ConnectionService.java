package arinet.com.mim.service.connection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by kevinstueber on 8/25/14.
 */
public class ConnectionService {

    private static ConnectionService instance = null;

    public static ConnectionService getInstance() {
        if(instance == null) {
            instance = new ConnectionService();
        }
        return instance;
    }

    public boolean isOnline(Context context){
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return (activeNetwork != null && activeNetwork.isConnectedOrConnecting());
    }

}
