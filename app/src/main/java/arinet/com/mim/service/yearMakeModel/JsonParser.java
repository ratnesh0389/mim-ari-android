package arinet.com.mim.service.yearMakeModel;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import arinet.com.mim.model.Industry;
import arinet.com.mim.model.Make;
import arinet.com.mim.model.StyleType;
import arinet.com.mim.model.Type;

/**
 * Created by kevinstueber on 9/3/14.
 */
public class JsonParser {

    public static ArrayList<Industry> parseIndustryResults(JsonArray response){
        ArrayList<Industry> industries = new ArrayList<Industry>();

        try{

            for (int i = 0; i < response.size(); ++i){
                JsonObject jsonObject = (JsonObject)response.get(i);

                Industry industry = new Industry();
                industry.setIndustryId(jsonObject.get("id").getAsInt());
                industry.setName(jsonObject.get("name").getAsString());

                industries.add(industry);
            }
        }
        catch (IllegalStateException e){
            System.out.println(e);
        }
        return industries;
    }

    public static ArrayList<Make>parseMakeResults(JsonArray response, int industryId){
        ArrayList<Make> makes = new ArrayList<Make>();

        try{

            for (int i = 0; i < response.size(); ++i){
                JsonObject jsonObject = (JsonObject)response.get(i);

                Make make = new Make();
                make.setMakeId(jsonObject.get("id").getAsInt());
                make.setName(jsonObject.get("name").getAsString());
                make.setIndustryId(industryId);

                makes.add(make);
            }
        }
        catch (IllegalStateException e){
            System.out.println(e);
        }
        return makes;
    }

    public static ArrayList<Type>parseTypeResults(JsonArray response, int makeId){
        ArrayList<Type> types = new ArrayList<Type>();

        try{

            for (int i = 0; i < response.size(); ++i){
                JsonObject jsonObject = (JsonObject)response.get(i);

                Type type = new Type();
                type.setTypeId(jsonObject.get("id").getAsInt());
                type.setMakeId(makeId);
                type.setName(jsonObject.get("name").getAsString());

                types.add(type);
            }
        }
        catch (IllegalStateException e){
            System.out.println(e);
        }
        return types;
    }

    public static ArrayList<StyleType>parseStyleTypeResults(JsonArray response, int typeId){
        ArrayList<StyleType> types = new ArrayList<StyleType>();

        try{

            for (int i = 0; i < response.size(); ++i){
                JsonObject jsonObject = (JsonObject)response.get(i);

                StyleType styleType = new StyleType();
                styleType.setStyleTypeId(jsonObject.get("id").getAsInt());
                styleType.setTypeId(typeId);
                styleType.setName(jsonObject.get("name").getAsString());

                types.add(styleType);
            }
        }
        catch (IllegalStateException e){
            System.out.println(e);
        }
        return types;
    }
}
