package arinet.com.mim.service.unit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import arinet.com.mim.model.Image;
import arinet.com.mim.model.Metric;
import arinet.com.mim.model.Unit;
import arinet.com.mim.model.Visibility;
import arinet.com.mim.service.connection.ConnectionService;
import arinet.com.mim.service.database.DatabaseService;
import arinet.com.mim.service.image.ImageService;

/**
 * Created by kevinstueber on 7/28/14.
 */
public class UnitService {

    private static UnitService instance = null;

    public static UnitService getInstance() {
        if(instance == null) {
            instance = new UnitService();
        }
        return instance;
    }

    public void getUnitsForQuery(Context context, String query){
        Intent ui = new Intent(context.getApplicationContext(), UnitRequestService.class);
        ui.putExtra(UnitRequestService.GET, true);
        ui.putExtra(UnitRequestService.QUERY,query);
        context.getApplicationContext().startService(ui);
    }

    public void getUnitsForSearch(Context context, String query){
        Intent ui = new Intent(context.getApplicationContext(), UnitRequestService.class);
        ui.putExtra(UnitRequestService.SEARCH, true);
        ui.putExtra(UnitRequestService.QUERY,query);
        context.getApplicationContext().startService(ui);
    }

    public Unit getUnitWithUnitId(Context context, int unitId){
        Unit unit = DatabaseService.getInstance(context.getApplicationContext()).selectUnitWithUnitId(unitId);
        populateUnitObject(context, unit);

        return unit;
    }

    public Unit getUnitWithLocalUnitId(Context context, long localUnitId){
        Unit unit = DatabaseService.getInstance(context.getApplicationContext()).selectUnitWithLocalUnitId(localUnitId);
        populateUnitObject(context, unit);

        return unit;
    }

    public ArrayList<Unit> getAllUnits(Context context){
        ArrayList<Unit>units = DatabaseService.getInstance(context.getApplicationContext()).selectAllUnits();
        for (Unit unit : units){
            populateUnitObject(context, unit);
        }
        return units;
    }

    public ArrayList<Unit> getOfflineUnits(Context context){
        ArrayList<Unit>units = DatabaseService.getInstance(context.getApplicationContext()).selectOfflineUnits();
        for (Unit unit : units){
            populateUnitObject(context, unit);
        }
        return units;
    }

    private void populateUnitObject(Context context, Unit unit){
        if(unit==null) return;
        unit.setImages(ImageService.getInstance().getImagesForUnit(context.getApplicationContext(), unit));
        if (unit.getUnitId() > 0){
            unit.setMetrics(DatabaseService.getInstance(context.getApplicationContext()).selectMetricsForUnit(unit));
            unit.setVisibilities(DatabaseService.getInstance(context.getApplicationContext()).selectVisibilitiesForUnit(unit));
            unit.setUnitAttributes(DatabaseService.getInstance(context.getApplicationContext()).selectUnitAttributesForUnit(unit));
        }
    }

    public void addUnit(Context context, Unit unit, boolean offlineRequest){
        unit.setNew(true);
        unit.setProductResult(false);

        if (DatabaseService.getInstance(context).updateUnit(unit) > 0){
            Intent ui = new Intent(context, UnitRequestService.class);
            ui.putExtra(UnitRequestService.ADD, true);
            ui.putExtra(UnitRequestService.LOCAL_UNIT_ID, unit.getLocalId());
            ui.putExtra(UnitRequestService.OFFLINE_REQUEST, offlineRequest);
            context.startService(ui);
        }
    }

    public void updateUnit(Context context, Unit unit, boolean offlineRequest){
        unit.setUpdated(true);
        DatabaseService.getInstance(context.getApplicationContext()).updateUnit(unit);

	    if (ConnectionService.getInstance().isOnline(context)){
		    Intent ui = new Intent(context, UnitRequestService.class);
		    ui.putExtra(UnitRequestService.PUT, true);
		    ui.putExtra(UnitRequestService.UNIT_ID, unit.getUnitId());
		    ui.putExtra(UnitRequestService.OFFLINE_REQUEST, offlineRequest);
		    context.startService(ui);
	    }
	    else{
		    Intent intent = new Intent(UnitRequestService.NOTIFICATION);
		    intent.putExtra(UnitRequestService.RESULT, Activity.RESULT_OK);
		    intent.putExtra(UnitRequestService.SENDER, "unit_request_service");
		    intent.putExtra(UnitRequestService.PUT, true);
		    intent.putExtra(UnitRequestService.OFFLINE_REQUEST, false);
		    context.sendBroadcast(intent);
	    }
    }

    public void deleteUnit(Context context, Unit unit, boolean offlineRequest){
        unit.setDelete(true);
        DatabaseService.getInstance(context.getApplicationContext()).updateUnit(unit);

        if (unit.getUnitId() > 0){
            Intent ui = new Intent(context, UnitRequestService.class);
            ui.putExtra(UnitRequestService.DELETE, true);
            ui.putExtra(UnitRequestService.UNIT_ID, unit.getUnitId());
            ui.putExtra(UnitRequestService.OFFLINE_REQUEST, offlineRequest);
            context.startService(ui);
        }
    }

    public void duplicateUnit(Context context, Unit unit){

        Unit unitCopy = new Unit(unit);
        unitCopy.setNew(true);

	    long localUnitId = DatabaseService.getInstance(context).insertUnit(unitCopy);
        unitCopy.setLocalId(localUnitId);

        for (Image image : unitCopy.getImages()){
            image.setUnitLocalId(localUnitId);
            DatabaseService.getInstance(context.getApplicationContext()).insertImage(image);
        }

	    for (Visibility visibility : unitCopy.getVisibilities()){
			visibility.setLocalUnitId(localUnitId);
	    }
	    DatabaseService.getInstance(context.getApplicationContext()).insertVisibilities(unitCopy.getVisibilities());

		Intent ui = new Intent(context, UnitRequestService.class);
		ui.putExtra(UnitRequestService.DUPLICATE, true);
		ui.putExtra(UnitRequestService.LOCAL_UNIT_ID, unitCopy.getLocalId());
		context.startService(ui);
    }

    public void getProductDataForQuery(Context context, String query){
        Intent ui = new Intent(context, UnitRequestService.class);
        ui.putExtra(UnitRequestService.GET_PRODUCTS, true);
        ui.putExtra(UnitRequestService.QUERY, query);
        context.startService(ui);
    }

    public ArrayList<Unit> getProductDataResults(Context context){
        return DatabaseService.getInstance(context).selectAllProductResults();
    }

    public void syncOfflineData(Context context, ArrayList<Unit>offlineUnits){
        for (Unit unit : offlineUnits){
            if (unit.isDelete()){
                deleteUnit(context, unit, true);
            }
            else if (unit.isNew()){
                addUnit(context, unit, true);
            }
            else if (unit.isUpdated()){
                updateUnit(context, unit, true);
            }
        }
    }

    public Unit updateUnitWithUnitDataForFinalization(Context context, Unit unfinalizedUnit, Unit productUnit){
        unfinalizedUnit.setEquipmentTypeId(productUnit.getEquipmentTypeId());
        unfinalizedUnit.setMake(productUnit.getMake());
        unfinalizedUnit.setMakeId(productUnit.getMakeId());
        unfinalizedUnit.setModel(productUnit.getModel());
        unfinalizedUnit.setYear(productUnit.getYear());
        unfinalizedUnit.setStyleId(productUnit.getStyleId());
        unfinalizedUnit.setInventoryStatusValue("Finalized");
        unfinalizedUnit.setPublishedOnWeb(true);

        if (productUnit.getLinkedProductId() > 0) {
            unfinalizedUnit.setLinkedProductId(productUnit.getLinkedProductId());
        }

        for (Image image : productUnit.getImages()){
            if (image.getStockImageId() > 0){
                Image newImage = new Image(image);
                newImage.setUnitLocalId(unfinalizedUnit.getLocalId());
                if (unfinalizedUnit.getUnitId() > 0){
                    newImage.setUnitId(unfinalizedUnit.getUnitId());
                }
                unfinalizedUnit.getImages().add(newImage);
                ImageService.getInstance().addImage(context, newImage);
            }
        }
        return unfinalizedUnit;
    }

    public String[] getTitleStatus(){
        String[] titles = {"Clean","Rental","Salvage","Flood"};
        return titles;
    }

    public int getTitleIntFromString(String titleString){
        int titleStatusInt = 0;
        String[] titleStatusArray = getTitleStatus();

        if (titleString.equals(titleStatusArray[0])){
            titleStatusInt = 0;
        }
        else if (titleString.equals(titleStatusArray[1])){
            titleStatusInt = 1;
        }
        else if (titleString.equals(titleStatusArray[2])){
            titleStatusInt = 2;
        }
        else if (titleString.equals(titleStatusArray[3])){
            titleStatusInt = 3;
        }
        return titleStatusInt;
    }

    public String[] getConditions(){
        String[] conditions = {"Excellent", "Fair", "Good", "Poor"};
        return conditions;
    }

    public int getConditionIntFromString(String conditionString){
        int conditionInt = 0;
        String[] conditionsArray = getConditions();

        if (conditionString.equals(conditionsArray[0])){
            conditionInt = 0;
        }
        else if (conditionString.equals(conditionsArray[1])){
            conditionInt = 1;
        }
        else if (conditionString.equals(conditionsArray[2])){
            conditionInt = 2;
        }
        else if (conditionString.equals(conditionsArray[3])){
            conditionInt = 3;
        }
        return conditionInt;
    }

    public String[] getPriceLabels(){
        String[] priceLabels = {"CallForPrice", "DMSPrice", "Pending", "MakeAnOffer", "WebPrice", "Sold"};
        return priceLabels;
    }

    public int getPriceLabelIntFromString(String priceLabelString){
        int priceLabelInt = 0;
        String[] priceLabelsArray = getPriceLabels();

        if (priceLabelString.equals(priceLabelsArray[0])){
            priceLabelInt = 0;
        }
        else if (priceLabelString.equals(priceLabelsArray[1])){
            priceLabelInt = 1;
        }
        else if (priceLabelString.equals(priceLabelsArray[2])){
            priceLabelInt = 2;
        }
        else if (priceLabelString.equals(priceLabelsArray[3])){
            priceLabelInt = 4;
        }
        else if (priceLabelString.equals(priceLabelsArray[4])){
            priceLabelInt = 5;
        }
        else if (priceLabelString.equals(priceLabelsArray[5])){
            priceLabelInt = 6;
        }
        return priceLabelInt;
    }

    private String[] getUsageStatus(){
        String[] usageStatus = {"CallForPrice", "DMSPrice", "Pending", "None", "MakeAnOffer", "WebPrice", "Sold"};
        return usageStatus;
    }

    public int getUsageStatusIntFromString(String usageStatusString){
        int usageStatusInt = 1;

        if (usageStatusString.equals("New") || usageStatusString.equals("N")){
            usageStatusInt = 2;
        }
        return usageStatusInt;
    }

    public ArrayList<Metric> updateSelectedMetricForUnitWithName(Context context, Unit unit, String name){
        ArrayList<Metric> metrics = DatabaseService.getInstance(context.getApplicationContext()).selectMetricsForUnit(unit);
        int metricValue = -1;
        Metric selectedMetric = null;

        for (Metric metric : metrics){
            if (name.contains(metric.getName())){
                metric.setSelected(true);
                selectedMetric = metric;
            }
            else {
                metric.setSelected(false);
            }

            if (metric.getValue() > 0){
                metricValue = metric.getValue();
            }
        }

        if (selectedMetric != null && metricValue > -1){
            selectedMetric.setValue(metricValue);
        }

        if (DatabaseService.getInstance(context.getApplicationContext()).deleteMetricsForUnit(unit)){
            DatabaseService.getInstance(context.getApplicationContext()).insertMetrics(metrics);
        }

        return metrics;
    }

    public ArrayList<Metric> updateSelectedMetricValueForUnitWithName(Context context, Unit unit, String name, int metricValue){
        ArrayList<Metric> metrics = DatabaseService.getInstance(context.getApplicationContext()).selectMetricsForUnit(unit);
        Metric selectedMetric = null;

        for (Metric metric : metrics){
            if (name.contains(metric.getName())){
                metric.setSelected(true);
                selectedMetric = metric;
                selectedMetric.setValue(metricValue);
            }
            else {
                metric.setSelected(false);
            }
        }

        if (DatabaseService.getInstance(context.getApplicationContext()).deleteMetricsForUnit(unit)){
            DatabaseService.getInstance(context.getApplicationContext()).insertMetrics(metrics);
        }

        return metrics;
    }

    public String[] getSaleTypes(){
        String[] saleTypes = {"Blowout", "Clearance", "Liquidation", "New", "Promotion", "Rebate", "Sale", "Season's End"};
        return saleTypes;
    }

    public int getSaleTypeIntFromString(String saleType){
        String[] saleTypes = getSaleTypes();

        for (int i = 0; i < saleTypes.length; ++i){
            if (saleType.equals(saleTypes[i])){
                return i;
            }
        }
        return 0;
    }

    public void sortUnits(ArrayList<Unit>units){
        Collections.sort(units, unitComparator);
    }

    Comparator unitComparator = new Comparator<Unit>() {
        @Override
        public int compare(Unit p1, Unit p2) {
            return p2.getHitsThirty() - p1.getHitsThirty();
        }
    };
}
