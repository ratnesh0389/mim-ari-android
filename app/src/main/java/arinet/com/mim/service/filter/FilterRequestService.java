package arinet.com.mim.service.filter;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import arinet.com.mim.R;
import arinet.com.mim.model.Filter;
import arinet.com.mim.model.Site;
import arinet.com.mim.service.database.DatabaseService;
import arinet.com.mim.service.json.JsonParser;
import arinet.com.mim.service.site.SiteService;

/**
 * Created by kevinstueber on 7/29/14.
 */
public class FilterRequestService extends IntentService {

    public static final String NOTIFICATION = "arinet.com.mim.service.receiver";
    private int result = Activity.RESULT_CANCELED;
    private String sender = "filter_request_service";

    public static final String SENDER = "sender";
    public static final String RESULT = "result";
    public static final String GET = "get";

    public FilterRequestService() {
        super("FilterRequestService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Boolean getFilters = intent.getBooleanExtra(GET, false);

        if (getFilters){
            requestFilters();
        }
    }

    private void requestFilters(){
        Site site = SiteService.getInstance().getSite(this.getApplicationContext());
        String urlString = this.getResources().getString(R.string.base_url) + "Filters";

        if (site != null){
            String token = site.getToken();
            if (token.length() > 0){
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpGet httpGet = new HttpGet(urlString);
                    httpGet.setHeader("Authorization", token);

                    HttpResponse response = httpclient.execute(httpGet);

                    DatabaseService.getInstance(this.getApplicationContext()).deleteFilters();

                    ArrayList<Filter> filters = JsonParser.parseFilterResult(response);

                    for (Filter filter : filters){
                        DatabaseService.getInstance(this.getApplicationContext()).insertFilter(filter);
                    }
                    result = Activity.RESULT_OK;

                }
                catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                finally {
                    publishGetResults(result);
                }
            }
        }
        else{
            publishGetResults(result);
        }
    }

    private void publishGetResults(int result) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(RESULT, result);
        intent.putExtra(SENDER, sender);
        intent.putExtra(GET, true);
        sendBroadcast(intent);
    }
}
