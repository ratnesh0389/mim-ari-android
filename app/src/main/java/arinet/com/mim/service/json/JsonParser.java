package arinet.com.mim.service.json;

import android.content.Context;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import arinet.com.mim.model.Attribute;
import arinet.com.mim.model.Filter;
import arinet.com.mim.model.Image;
import arinet.com.mim.model.Location;
import arinet.com.mim.model.Metric;
import arinet.com.mim.model.Site;
import arinet.com.mim.model.Unit;
import arinet.com.mim.model.UnitAttribute;
import arinet.com.mim.model.Visibility;
import arinet.com.mim.service.site.SiteService;
import arinet.com.mim.service.toolkit.ToolkitService;
import arinet.com.mim.service.unit.UnitService;

/**
 * Created by kevinstueber on 7/28/14.
 */
public class JsonParser {

    public static Site parseSiteAuthResult(HttpResponse response){
        Site site = null;

        try {
            JSONObject jsonObj = new JSONObject(EntityUtils.toString(response.getEntity()));

            if (jsonObj.has("id")){
                site = new Site();
                site.setSiteId(jsonObj.getInt("id"));
                site.setAccountGuid(jsonObj.getString("accountGuid"));
                site.setOrganizationGuid(jsonObj.getString("organizationGuid"));
                site.setExpires(jsonObj.getString("expires"));
                site.setEmail(jsonObj.getString("email"));
                site.setToken(jsonObj.getString("token"));
                site.setCurrent(true);
            }
        }
        catch (JSONException e){
            System.out.println(e);
        }
        catch (IOException e){
            System.out.println(e);
        }
        return site;
    }

    public static boolean parseVerifyAuthResult(Context context, HttpResponse response){
        try {
            JSONArray jsonArray = new JSONArray(EntityUtils.toString(response.getEntity()));

            Site site = SiteService.getInstance().getSite(context);
            if (site != null && jsonArray.length() > 0){
                for (int i = 0; i < jsonArray.length(); ++i){
                    JSONObject jsonObject = (JSONObject)jsonArray.get(i);
                    if (jsonObject.has("id")){
                        int id = jsonObject.getInt("id");
                        if (id == site.getSiteId() && jsonObject.has("activated")){
                            return true;
                        }
                    }
                }
            }
        }
        catch (JSONException e){
            System.out.println(e);
        }
        catch (IOException e){
            System.out.println(e);
        }
        return false;
    }

    public static ArrayList<Unit> parseSearchResults(JsonArray response){
        ArrayList<Unit> units = new ArrayList<Unit>();

        try{

            for (int i = 0; i < response.size(); ++i){
                JsonObject jsonObject = (JsonObject)response.get(i);
                Unit unit = parseUnit(jsonObject);

                if (unit != null){
                    units.add(unit);
                }
            }
        }
        catch (IllegalStateException e){
            System.out.println(e);
        }
        return units;
    }

    public static ArrayList<Unit> parseProductResult(JsonArray jsonArray){
        ArrayList<Unit> units = new ArrayList<Unit>();
        try{
            for (int i = 0; i < jsonArray.size(); ++i){
                JsonObject jsonObj = (JsonObject)jsonArray.get(i);

                Unit unit = parseUnit(jsonObj);

                if (unit != null){
                    units.add(unit);
                }
            }
        }
        catch (IllegalStateException e){
            System.out.println(e);
        }
        return units;
    }

    public static Unit parseAddResult(JsonObject jsonObject){
        Unit unit = null;
	    System.out.println(jsonObject);
        unit = parseUnit(jsonObject);
        return unit;
    }

    public static Unit parseUnit(JsonObject jsonObject){
        Unit unit = null;

        if (jsonObject.has("id")){

            unit = new Unit();
            unit.setUnitId(jsonObject.get("id").getAsInt());
            unit.setYear(jsonObject.get("year").getAsInt());
            unit.setMakeId(jsonObject.get("makeId").getAsInt());
            unit.setPrice(jsonObject.get("price").getAsDouble());
            unit.setEquipmentTypeId(jsonObject.get("equipmentTypeId").getAsInt());
            unit.setStyleId(jsonObject.get("styleId").getAsInt());
            unit.setLinkedProductId(jsonObject.get("linkedProductId").getAsInt());
            unit.setIndustryId(jsonObject.get("industryId").getAsInt());

            unit.setHitsSeven(0);
            unit.setHitsThirty(0);
            unit.setHitsNintey(0);
            unit.setHitsThreeSixFive(0);

            if (jsonObject.has("images")) {
                JsonArray images = jsonObject.getAsJsonArray("images");
                ArrayList<Image> imagesArray = new ArrayList<Image>();

                for (int e = 0; e < images.size(); ++e) {
                    imagesArray.add(parseImageResult(images.get(e).toString()));
                }

                unit.setImages(imagesArray);
            }

            if (jsonObject.has("visibility")) {
                JsonArray visibilities = jsonObject.getAsJsonArray("visibility");
                ArrayList<Visibility> visibilityArray = new ArrayList<Visibility>();

                for (int e = 0; e < visibilities.size(); ++e) {
                    JsonObject vis = (JsonObject) visibilities.get(e);

                    Visibility visibility = new Visibility();
                    visibility.setVisibilityId(vis.get("id").getAsInt());
                    visibility.setSiteId(-1);
                    visibility.setUnitId(unit.getUnitId());
                    visibility.setName(vis.get("name").getAsString());
                    visibility.setType(vis.get("type").getAsString());
                    visibility.setActive(vis.get("active").getAsBoolean());

                    visibilityArray.add(visibility);
                }
                unit.setVisibilities(visibilityArray);
            }

            if (jsonObject.has("metrics")) {
                JsonArray metrics = jsonObject.getAsJsonArray("metrics");
                ArrayList<Metric> metricsArray = new ArrayList<Metric>();

                for (int e = 0; e < metrics.size(); ++e) {
                    JsonObject met = (JsonObject) metrics.get(e);

                    Metric metric = new Metric();
                    metric.setMetricId(met.get("id").getAsInt());
                    metric.setUnitId(unit.getUnitId());
                    metric.setProfileId(met.get("profileId").getAsInt());
                    metric.setSequence(met.get("sequence").getAsInt());
                    if (met.has("value")) {
                        String metricValue = met.get("value").getAsString();
                        if (ToolkitService.isInteger(metricValue)){
                            metric.setValue(Integer.parseInt(metricValue));
                        }
                        else{
                            metric.setValue(-1);
                        }
                    } else {
                        metric.setValue(-1);
                    }
                    metric.setSelected(met.get("selected").getAsString().equals("true"));
                    metric.setAbbreviation(met.get("abbreviation").getAsString());
                    metric.setName(met.get("name").getAsString());

                    metricsArray.add(metric);
                }
                unit.setMetrics(metricsArray);
            }

            if (jsonObject.has("attributes")){
                JsonArray attributes = jsonObject.getAsJsonArray("attributes");
                ArrayList<UnitAttribute> attributesArray = new ArrayList<UnitAttribute>();

                for (int e = 0; e < attributes.size(); ++e){
                    JsonObject att = (JsonObject) attributes.get(e);

                    UnitAttribute ua = new UnitAttribute();

                    ua.setAttributeId(att.get("id").getAsInt());
                    ua.setUnitId(unit.getUnitId());
                    ua.setName(att.get("name").getAsString());
                    ua.setType(att.get("type").getAsString());

                    if (att.has("value")){
                        ua.setValue(att.get("value").getAsString());
                    }

                    attributesArray.add(ua);
                }
                unit.setUnitAttributes(attributesArray);
            }

            if (jsonObject.has("dmsPrice")) {
                unit.setDmsPrice(jsonObject.get("dmsPrice").getAsInt());
            }

            int titleStatus = UnitService.getInstance().getTitleIntFromString(jsonObject.get("titleStatus").getAsString());
            unit.setTitleStatus(titleStatus);

            int condition = UnitService.getInstance().getConditionIntFromString(jsonObject.get("condition").getAsString());
            unit.setCondition(condition);

            int priceLabel = UnitService.getInstance().getPriceLabelIntFromString(jsonObject.get("priceLabel").getAsString());
            unit.setPriceLabel(priceLabel);

            int usageStatus = UnitService.getInstance().getUsageStatusIntFromString(jsonObject.get("usageStatus").getAsString());
            unit.setUsageStatus(usageStatus);

            //unit.setSiteId(jsonObject.getInt("siteId"));
            unit.setLocationId(jsonObject.get("locationId").getAsInt());

            if (jsonObject.has("salePrice")) {
                unit.setSalePrice(jsonObject.get("salePrice").getAsDouble());
            } else {
                unit.setSalePrice(-1);
            }

            if (jsonObject.has("bestPrice")){
                unit.setBestPrice(jsonObject.get("bestPrice").getAsDouble());
            }
            else{
                unit.setBestPrice(-1);
            }

            unit.setCallForPrice(jsonObject.get("callForPrice").getAsBoolean());
            unit.setPublishedOnWeb(jsonObject.get("publishedOnWeb").getAsBoolean());
            unit.setUseStockImages(jsonObject.get("useStockImages").getAsBoolean());
            unit.setDisplayInShowcaseCount(jsonObject.get("displayInShowcaseCount").getAsBoolean());
            unit.setSaleActive(jsonObject.get("saleActive").getAsBoolean());
            unit.setNew(false);
            unit.setUpdated(false);
            unit.setDelete(false);

            if (jsonObject.has("model")) {
                unit.setModel(jsonObject.get("model").getAsString());
            } else {
                unit.setModel("");
            }

            if (jsonObject.has("stockNumber") && !jsonObject.get("stockNumber").getAsString().equals("(null)")) {
                unit.setStockNumber(jsonObject.get("stockNumber").getAsString());
            } else {
                unit.setStockNumber("");
            }

            if (jsonObject.has("vin") && !jsonObject.get("vin").getAsString().equals("(null)")) {
                unit.setVin(jsonObject.get("vin").getAsString());
            } else {
                unit.setVin("");
            }

            if (jsonObject.has("globalIdentifier")) {
                unit.setGlobalIdentifier(jsonObject.get("globalIdentifier").getAsString());
            } else {
                unit.setGlobalIdentifier("");
            }

            if (jsonObject.has("inventoryStatusValue")) {
                unit.setInventoryStatusValue(jsonObject.get("inventoryStatusValue").getAsString());
            } else {
                unit.setInventoryStatusValue("");
            }

            if (jsonObject.has("make")) {
                unit.setMake(jsonObject.get("make").getAsString());
            } else {
                unit.setMake("");
            }

            if (jsonObject.has("description")) {
                unit.setDescription(jsonObject.get("description").getAsString());
            } else {
                unit.setDescription("");
            }

            if (jsonObject.has("equipmentType")) {
                unit.setEquipmentType(jsonObject.get("equipmentType").getAsString());
            } else {
                unit.setEquipmentType("");
            }

            if (jsonObject.has("linkedProductName")) {
                unit.setLinkedProductName(jsonObject.get("linkedProductName").getAsString());
            } else {
                unit.setLinkedProductName("");
            }

            if (jsonObject.has("primaryColor") && !jsonObject.get("primaryColor").equals("(null)")) {
                unit.setPrimaryColor(jsonObject.get("primaryColor").getAsString());
            } else {
                unit.setPrimaryColor("");
            }

            if (jsonObject.has("trimColor") && !jsonObject.get("trimColor").getAsString().equals("(null)")) {
                unit.setTrimColor(jsonObject.get("trimColor").getAsString());
            } else {
                unit.setTrimColor("");
            }

            if (jsonObject.has("lastModified")) {
                unit.setLastUpdate(jsonObject.get("lastModified").getAsString());
            } else {
                unit.setLastUpdate("");
            }

            if (jsonObject.has("saleType")) {
                unit.setSaleType(jsonObject.get("saleType").getAsString());
            } else {
                unit.setSaleType("");
            }

            if (jsonObject.has("saleStartDate")) {
                unit.setSaleStartDate(jsonObject.get("saleStartDate").getAsString());
            } else {
                unit.setSaleStartDate("");
            }

            if (jsonObject.has("saleEndDate")) {
                unit.setSaleEndDate(jsonObject.get("saleEndDate").getAsString());
            } else {
                unit.setSaleEndDate("");
            }

            if (jsonObject.has("unfinalizedUnitId")) {
                unit.setUnfinalizedUnitId(jsonObject.get("unfinalizedUnitId").getAsString());
            } else {
                unit.setUnfinalizedUnitId("");
            }

            if (jsonObject.has("url")) {
                unit.setUrl(jsonObject.get("url").getAsString());
            } else {
                unit.setUrl("");
            }

        }

        return unit;
    }

    public static ArrayList<Filter> parseFilterResult(HttpResponse response){
        ArrayList<Filter>filters = new ArrayList<Filter>();
        String[] allowedNames = { "images", "details", "visibility", "unfinalized" };

        try {
            JSONArray jsonArray = new JSONArray(EntityUtils.toString(response.getEntity()));

            for (int i = 0; i < jsonArray.length(); ++i) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                Filter filter = new Filter();
                filter.setName(jsonObject.getString("name"));
                filter.setCount(jsonObject.getInt("count"));
                filter.setQuery(jsonObject.getString("query"));

                filter.setLocation("");
                if (jsonObject.has("location")) {
                    filter.setLocation(jsonObject.getString("location"));
                }

                filter.setStoreLocationId(0);
                if (jsonObject.has("storeLocationId")){
                    filter.setStoreLocationId(jsonObject.getInt("storeLocationId"));
                }

                if (filter.getStoreLocationId() > 0 || Arrays.asList(allowedNames).contains(filter.getName())){
                    filters.add(filter);
                }
            }
        }
        catch (JSONException e){
            System.out.println(e);
        }
        catch (IOException e){
            System.out.println(e);
        }
        return filters;
    }

    public static ArrayList<Attribute> parseAttributeResult(HttpResponse response){
        ArrayList<Attribute> attributes = new ArrayList<Attribute>();

        try {
            JSONArray jsonArray = new JSONArray(EntityUtils.toString(response.getEntity()));

            for (int i = 0; i < jsonArray.length(); ++i) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                Attribute attribute = new Attribute();
                attribute.setAttributeId(jsonObject.getLong("id"));
                attribute.setName(jsonObject.getString("name"));
                attribute.setIndustryId(jsonObject.getInt("industryId"));
                attribute.setType(jsonObject.getString("type"));

                if (jsonObject.has("value")){
                    attribute.setValue(jsonObject.getString("value"));
                }

                attributes.add(attribute);
            }
        }
        catch (JSONException e){
            System.out.println(e);
        }
        catch (IOException e){
            System.out.println(e);
        }

        return attributes;
    }

    public static ArrayList<Location> parseLocationResult(HttpResponse response){
        ArrayList<Location>locations = new ArrayList<Location>();

        try {
            JSONArray jsonArray = new JSONArray(EntityUtils.toString(response.getEntity()));

            for (int i = 0; i < jsonArray.length(); ++i) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                Location location = new Location();
                location.setLocationId(jsonObject.getInt("id"));
                location.setName(jsonObject.getString("name"));
                location.setPhone(jsonObject.getString("phone"));

                JSONObject addressObject = jsonObject.getJSONObject("address");
                location.setCity(addressObject.getString("city"));
                location.setCountry(addressObject.getString("country"));
                location.setPostalCode(addressObject.getString("postalCode"));
                location.setRegion(addressObject.getString("region"));
                location.setStreet(addressObject.getString("street"));

                locations.add(location);
            }
        }
        catch (JSONException e){
            System.out.println(e);
        }
        catch (IOException e){
            System.out.println(e);
        }
        return locations;
    }

    public static ArrayList<Visibility> parseVisibilityResult(HttpResponse response, int siteId, int unitId){
        ArrayList<Visibility>visibilities = new ArrayList<Visibility>();

        try {
            JSONArray jsonArray = new JSONArray(EntityUtils.toString(response.getEntity()));

            for (int i = 0; i < jsonArray.length(); ++i) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                if (jsonObject.getString("active").equals("true")){
                    Visibility visibility = new Visibility();
                    visibility.setVisibilityId(jsonObject.getInt("id"));
                    visibility.setName(jsonObject.getString("name"));
                    visibility.setType(jsonObject.getString("type"));
                    if (unitId > 0){
                        visibility.setUnitId(unitId);
                    }
                    else{
                        visibility.setSiteId(siteId);
                    }
                    visibilities.add(visibility);
                }
            }
        }
        catch (JSONException e){
            System.out.println(e);
        }
        catch (IOException e){
            System.out.println(e);
        }
        return visibilities;
    }

    public static Image parseImageResult(String result){
        Image image = null;
        try {
            JSONObject img = new JSONObject(result);
            image = new Image();
            image.setImageId(img.getInt("id"));
            image.setStockImageId(img.getInt("stockImageId"));
            image.setLocation(img.getString("location"));
            image.setThumbLocation(img.getString("thumbLocation"));
            image.setPrimary(img.getString("isPrimary").equals("true"));
            image.setUnitId(img.getInt("unitId"));
            image.setSequence(img.getInt("sequence"));
        }
        catch (JSONException e){
            System.out.println(e);
        }
        finally {
            return image;
        }
    }
}
