package arinet.com.mim.service.unitAttribute;

import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;

import arinet.com.mim.model.Unit;
import arinet.com.mim.model.UnitAttribute;
import arinet.com.mim.service.database.DatabaseService;

/**
 * Created by kevinstueber on 10/22/14.
 */
public class UnitAttributeService {

    private static UnitAttributeService instance = null;

    public static UnitAttributeService getInstance() {
        if(instance == null) {
            instance = new UnitAttributeService();
        }
        return instance;
    }


    public boolean replaceUnitAttributesForUnit(Context context, Unit unit){
        if (DatabaseService.getInstance(context).deleteUnitAttributesForUnit(unit)){
            return DatabaseService.getInstance(context).insertUnitAttributes(unit.getUnitAttributes());
        }
        return false;
    }

    public ArrayList<UnitAttribute> getUnitAttributesForUnit(Context context, Unit unit){
        return DatabaseService.getInstance(context).selectUnitAttributesForUnit(unit);
    }
}
