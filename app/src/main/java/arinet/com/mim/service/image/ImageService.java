package arinet.com.mim.service.image;

import android.content.Context;
import android.content.Intent;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import arinet.com.mim.model.Image;
import arinet.com.mim.model.Unit;
import arinet.com.mim.service.database.DatabaseService;
import arinet.com.mim.service.toolkit.ToolkitService;

/**
 * Created by kevinstueber on 8/19/14.
 */
public class ImageService {

    private static ImageService instance = null;

    public static ImageService getInstance() {
        if(instance == null) {
            instance = new ImageService();
        }
        return instance;
    }

    public long addImage(Context context, Image image){
        return DatabaseService.getInstance(context).insertImage(image);
    }

    public void postImage(Context context, Image image, boolean offlineRequest){
        if(!offlineRequest) {
            addImage(context, image);
        }
        Intent ui = new Intent(context, ImageRequestService.class);
        ui.putExtra(ImageRequestService.POST, true);
        ui.putExtra(ImageRequestService.LOCAL_PATH, image.getLocalPath());
        ui.putExtra(ImageRequestService.OFFLINE_REQUEST, offlineRequest);
        context.startService(ui);
    }

    public ArrayList<Image>getImagesForUnit(Context context, Unit unit){

            ArrayList<Image> images = DatabaseService.getInstance(context).selectImagesForUnit(unit);
            ArrayList<Image> existingImages = getOfflineImagesForUnit(context, unit);

            if (existingImages.size() > 0){
                for (Image ex : existingImages){
                    boolean addit = true;
                    for (Image img : images){
                        if (img!=null
                                && img.getLocalPath() != null
                                && ex!=null && ex.getLocalPath()!=null
                                && ex.getLocalPath().equalsIgnoreCase(img.getLocalPath())){
                            addit = false;
                            break;
                        }
                    }
                    if (addit && ex!=null){
                        images.add(ex);
                    }
                }
            }

            return ToolkitService.getInstance().sortImageArray(images);
    }

    public ArrayList<Image> getOfflineImagesForUnit(Context context, Unit unit){
        return ToolkitService.getInstance().sortImageArray(DatabaseService.getInstance(context).selectOfflineImagesForUnit(unit));
    }

    public Image getImageWithLocalPath(Context context, String localPath){
        return DatabaseService.getInstance(context).selectImageWithLocalPath(localPath);
    }

    public Image getImageWithImageId(Context context, int imageId){
        return DatabaseService.getInstance(context).selectImageWithImageId(imageId);
    }

    public ArrayList<Image> getOfflineImages(Context context){
        return DatabaseService.getInstance(context).selectOfflineImages();
    }

    public void updateImage(Context context, Image image){
        image.setUpdated(true);
        DatabaseService.getInstance(context).updateImage(image);
    }

    public void putImage(Context context, Image image, boolean offlineRequest){
        updateImage(context, image);

        Intent ui = new Intent(context, ImageRequestService.class);
        ui.putExtra(ImageRequestService.PUT, true);
        ui.putExtra(ImageRequestService.IMAGE_ID, image.getImageId());
        ui.putExtra(ImageRequestService.OFFLINE_REQUEST, offlineRequest);
        context.startService(ui);
    }

    public void deleteImage(Context context, Image image){
        image.setDelete(true);

        boolean deleteLocal = deleteLocalImage(context, image);

        if (!deleteLocal){
            Intent ui = new Intent(context, ImageRequestService.class);
            ui.putExtra(ImageRequestService.DELETE, true);
            ui.putExtra(ImageRequestService.IMAGE_ID, image.getImageId());
            context.startService(ui);
        }
    }

    public boolean deleteLocalImage(Context context, Image image){
        boolean deleted = false;
        if (image.getLocalPath() != null && image.getLocalPath().length() > 0) {
            DatabaseService.getInstance(context).deleteLocalImage(image);
            File file = new File(image.getLocalPath());
            if (file.exists()) {
                deleted = file.delete();
            }
        }
        return deleted;
    }

    public void syncOfflineImages(Context context, ArrayList<Image>offlineImages){
        for (Image image : offlineImages){
            if (image.isNew()){
                postImage(context, image, true);
            }
            else if (image.isUpdated()){
                putImage(context, image, true);
            }
            else if (image.isDelete()){
                deleteImage(context, image);
            }
        }
    }
}
