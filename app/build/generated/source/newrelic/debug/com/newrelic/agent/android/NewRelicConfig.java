package com.newrelic.agent.android;
final class NewRelicConfig {
	static final String VERSION = "5.24.1";
	static final String BUILD_ID = "bc61a86b-fd4f-4dd0-84b5-0adfbeb05042";
	static final Boolean OBFUSCATED = false;
	public static String getBuildId() {
		return BUILD_ID;
	}
}
